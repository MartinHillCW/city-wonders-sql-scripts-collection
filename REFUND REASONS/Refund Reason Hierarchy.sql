SELECT	rt.RefundType_Description + ' >> ' 
			+ rr.RefundReasons_Description 
			+ CASE WHEN rre.RefundReasonsExtended_Description IS NULL 
				THEN ''
				ELSE ' >> ' + rre.RefundReasonsExtended_Description 
			END [Refund Workflow]
FROM	d_Mapping_RefundTypes_RefundReasons mrtrr
			LEFT OUTER JOIN		d_Mapping_RefundReasons_RefundExtended mrrre	on mrtrr.RefundReasons_id = mrrre.RefundReasons_id
			INNER JOIN			d_RefundTypes rt								on rt.RefundType_id = mrtrr.RefundType_id
			INNER JOIN			d_RefundReasons rr								on rr.RefundReasons_id = mrtrr.RefundReasons_id
			LEFT OUTER JOIN		d_RefundReasonsExtended rre						on rre.RefundReasonsExtended_id = mrrre.RefundReasonsExtended_id

ORDER BY 1

SELECT	 rt.RefundType_Description							RefundType	
		,rr.RefundReasons_Description						RefundReason
		,ISNULL(rre.RefundReasonsExtended_Description,'')	[RefundReason Extended]
FROM	d_Mapping_RefundTypes_RefundReasons mrtrr
			LEFT OUTER JOIN		d_Mapping_RefundReasons_RefundExtended mrrre	on mrtrr.RefundReasons_id = mrrre.RefundReasons_id
			INNER JOIN			d_RefundTypes rt								on rt.RefundType_id = mrtrr.RefundType_id
			INNER JOIN			d_RefundReasons rr								on rr.RefundReasons_id = mrtrr.RefundReasons_id
			LEFT OUTER JOIN		d_RefundReasonsExtended rre						on rre.RefundReasonsExtended_id = mrrre.RefundReasonsExtended_id

ORDER BY 1,2,3

/*
SELECT	 rrpg.RefundReasons_PermissionGroups_Description		[Department]
		,dbo.ProperCase(REPLACE(u.user_name,'.',' '))			[User Name]
		,rr.RefundReasons_Description 							[Refund Reason Access]
FROM	d_Mapping_RefundReasons_PermissionGroups mrrpg
			INNER JOIN d_RefundReasons rr on rr.RefundReasons_id = mrrpg.RefundReasons_id
			INNER JOIN d_RefundReasons_PermissionGroups rrpg on rrpg.RefundReasons_PermissionGroups_id = mrrpg.RefundReasons_PermissionGroups_id			
			INNER JOIN d_Mapping_PermissionGroups_Members mpgm on mpgm.PermissionGroup_id = mrrpg.RefundReasons_PermissionGroups_id
			INNER JOIN d_user u ON u.user_id = mpgm.Member_id
		*/