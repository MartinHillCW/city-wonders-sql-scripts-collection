--------------------------------------------------------------------------------------------
SET NOCOUNT ON;
--------------------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_RefundReason]') AND parent_object_id = OBJECT_ID(N'[dbo].[d_Mapping_RefundTypes_RefundReasons]'))
ALTER TABLE [dbo].[d_Mapping_RefundTypes_RefundReasons] DROP CONSTRAINT [fk_RefundReason]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_RefundType]') AND parent_object_id = OBJECT_ID(N'[dbo].[d_Mapping_RefundTypes_RefundReasons]'))
ALTER TABLE [dbo].[d_Mapping_RefundTypes_RefundReasons] DROP CONSTRAINT [fk_RefundType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_RefundExtended]') AND parent_object_id = OBJECT_ID(N'[dbo].[d_Mapping_RefundReasons_RefundExtended]'))
ALTER TABLE [dbo].[d_Mapping_RefundReasons_RefundExtended] DROP CONSTRAINT [fk_RefundExtended]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_RefundReason_extended]') AND parent_object_id = OBJECT_ID(N'[dbo].[d_Mapping_RefundReasons_RefundExtended]'))
ALTER TABLE [dbo].[d_Mapping_RefundReasons_RefundExtended] DROP CONSTRAINT [fk_RefundReason_extended]
GO


--------------------------------------------------------------------------------------------
--CREATE TABLE d_RefundTypes
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[d_RefundTypes]') AND type in (N'U'))
DROP TABLE [dbo].[d_RefundTypes]
GO
CREATE TABLE d_RefundTypes (
	RefundType_id INT IDENTITY (1,1) NOT NULL,
	RefundType_Description VARCHAR(10) NOT NULL,
	PRIMARY KEY (RefundType_id)
)
GO

--------------------------------------------------------------------------------------------
--CREATE TABLE d_RefundReasons
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[d_RefundReasons]') AND type in (N'U'))
DROP TABLE [dbo].[d_RefundReasons]
GO
CREATE TABLE d_RefundReasons (
	RefundReasons_id INT IDENTITY (1,1) NOT NULL,	
	RefundReasons_Description VARCHAR(255) NOT NULL,
	RefundReasons_PermissionRequired  BIT NOT NULL,
	PRIMARY KEY (RefundReasons_id)	
)
GO

--------------------------------------------------------------------------------------------
--CREATE TABLE [d_RefundReasons_PermissionGroups]
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[d_RefundReasons_PermissionGroups]') AND type in (N'U'))
DROP TABLE [dbo].[d_RefundReasons_PermissionGroups]
GO
CREATE TABLE d_RefundReasons_PermissionGroups (	
	RefundReasons_PermissionGroups_id INT IDENTITY (1,1) NOT NULL,	
	RefundReasons_PermissionGroups_Description VARCHAR(255) NOT NULL
	PRIMARY KEY (RefundReasons_PermissionGroups_id)	
)
GO


--------------------------------------------------------------------------------------------
--CREATE TABLE [d_RefundReasons_PermissionGroups_Members]
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[d_RefundReasons_PermissionGroups_Members]') AND type in (N'U'))
DROP TABLE [dbo].[d_RefundReasons_PermissionGroups_Members]
GO
CREATE TABLE d_RefundReasons_PermissionGroups_Members (	
	d_RefundReasons_PermissionGroups_Members_id INT IDENTITY (1,1) NOT NULL,	
	RefundReasons_PermissionGroups_Member INT NOT NULL
	PRIMARY KEY (d_RefundReasons_PermissionGroups_Members_id)	
)
GO
--------------------------------------------------------------------------------------------
-- drop the constraint in the [d_RefundReasonsExtended] table
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_RefundClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[d_RefundReasonsExtended]'))
ALTER TABLE [dbo].[d_RefundReasonsExtended] DROP CONSTRAINT [fk_RefundClass]
GO

--CREATE TABLE d_RefundClassification
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[d_RefundClassification]') AND type in (N'U'))
DROP TABLE [dbo].[d_RefundClassification]
GO
CREATE TABLE d_RefundClassification (
	RefundClassification_id INT IDENTITY (1,1) NOT NULL ,	
	RefundClassification_Description VARCHAR(255) NOT NULL,
	PRIMARY KEY (RefundClassification_id)
)
GO

--------------------------------------------------------------------------------------------
--CREATE TABLE d_RefundReasonsExtended
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[d_RefundReasonsExtended]') AND type in (N'U'))
DROP TABLE [dbo].[d_RefundReasonsExtended]
GO
CREATE TABLE d_RefundReasonsExtended (
	RefundReasonsExtended_id INT IDENTITY (1,1) NOT NULL,
	RefundReasonsExtended_Description VARCHAR(255) NOT NULL,
	RefundClassification_id INT,
	RefundReasonsExtended_RequiresNote BIT NOT NULL,
	PRIMARY KEY (RefundReasonsExtended_id),
	CONSTRAINT fk_RefundClass FOREIGN KEY (RefundClassification_id) REFERENCES [d_RefundClassification](RefundClassification_id)
	
)
GO

--------------------------------------------------------------------------------------------
--CREATE TABLE d_RefundAmountType
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[d_RefundAmountType]') AND type in (N'U'))
DROP TABLE [dbo].[d_RefundAmountType]
GO
CREATE TABLE d_RefundAmountType (
	RefundAmountType_id INT IDENTITY (1,1),	
	RefundAmountType_Description VARCHAR(255),
	RefundAmountType_Rate INT
	
)
GO
INSERT INTO d_RefundAmountType SELECT 'Percentage 100', 100
INSERT INTO d_RefundAmountType SELECT 'Figure Full', 100
INSERT INTO d_RefundAmountType SELECT 'Percentage 30', 30
INSERT INTO d_RefundAmountType SELECT 'Percentage 95', 95
INSERT INTO d_RefundAmountType SELECT 'Percentage', NULL
INSERT INTO d_RefundAmountType SELECT 'Figure', NULL
GO

------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--CREATE TABLE [d_Mapping_RefundReasons_PermissionGroups]
/*
	MAPS A REFUND REASON TO A GROUP. THIS ALLOWS CERTAIN GROUPS TO ONLY SEE S
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[d_Mapping_RefundReasons_PermissionGroups]') AND type in (N'U'))
DROP TABLE [dbo].[d_Mapping_RefundReasons_PermissionGroups]
GO
CREATE TABLE d_Mapping_RefundReasons_PermissionGroups (
	Mapping_id			INT IDENTITY (1,1) NOT NULL,
	RefundReasons_id	INT NOT NULL,
	RefundReasons_PermissionGroups_id	INT NOT NULL,
	PRIMARY KEY (Mapping_id)
)
GO
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--CREATE TABLE [d_Mapping_PermissionGroups_Members]
/*
	MAPS A USER TO A GROUP
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[d_Mapping_PermissionGroups_Members]') AND type in (N'U'))
DROP TABLE [dbo].[d_Mapping_PermissionGroups_Members]
GO
CREATE TABLE d_Mapping_PermissionGroups_Members (
	Mapping_id			INT IDENTITY (1,1) NOT NULL,
	PermissionGroup_id	INT NOT NULL,
	Member_id	INT NOT NULL,
	PRIMARY KEY (Mapping_id)
)
GO
------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--CREATE TABLE d_RefundTypes
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[d_Mapping_RefundTypes_RefundReasons]') AND type in (N'U'))
DROP TABLE [dbo].[d_Mapping_RefundTypes_RefundReasons]
GO
CREATE TABLE d_Mapping_RefundTypes_RefundReasons (
	Mapping_id			INT IDENTITY (1,1) NOT NULL,
	RefundType_id		INT NOT NULL,
	RefundReasons_id	INT NOT NULL,
	PRIMARY KEY (Mapping_id),
	CONSTRAINT fk_RefundType	FOREIGN KEY (RefundType_id)		REFERENCES d_RefundTypes(RefundType_id),
	CONSTRAINT fk_RefundReason	FOREIGN KEY (RefundReasons_id)	REFERENCES d_RefundReasons(RefundReasons_id)
)
GO


-----------------------------------------------------------------------------
--CREATE TABLE d_RefundTypes
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[d_Mapping_RefundReasons_RefundExtended]') AND type in (N'U'))
DROP TABLE [dbo].[d_Mapping_RefundReasons_RefundExtended]
GO
CREATE TABLE d_Mapping_RefundReasons_RefundExtended (
	Mapping_id					INT IDENTITY (1,1) NOT NULL,
	RefundReasons_id			INT NOT NULL,
	RefundReasonsExtended_id	INT NOT NULL,
	PRIMARY KEY (Mapping_id),
	CONSTRAINT fk_RefundReason_extended	FOREIGN KEY (RefundReasons_id)			REFERENCES d_RefundReasons(RefundReasons_id),
	CONSTRAINT fk_RefundExtended		FOREIGN KEY (RefundReasonsExtended_id)	REFERENCES d_RefundReasonsExtended(RefundReasonsExtended_id)
)
GO

-------------------------------------------------------------------------------
----CREATE TABLE [d_Trans_RefundReasons]
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[d_Trans_RefundReasons]') AND type in (N'U'))
--DROP TABLE [dbo].[d_Trans_RefundReasons]
--GO
--CREATE TABLE d_Trans_RefundReasons (
--	RefundReasons_id INT IDENTITY (1,1) NOT NULL,
--	Trans_id INT,
--	RefundReasons_RefundType INT,
--	RefundReasons_RefundReason INT,
--	RefundReasons_RefundReasonExtended INT
--	PRIMARY KEY (RefundReasons_id),
--	CONSTRAINT fk_RefundReasons FOREIGN KEY (Trans_id) REFERENCES d_transactions(Trans_id)
--)
--GO



--------------------------------------------------------------------------------------
--REFUND TYPES
INSERT INTO d_RefundTypes SELECT 'Downgrade'									--1
INSERT INTO d_RefundTypes SELECT 'Partial'										--2
INSERT INTO d_RefundTypes SELECT 'Full'											--3
GO
-- REFUND REASONS
INSERT INTO d_RefundReasons SELECT 'Tour Disruption',0							--1
INSERT INTO d_RefundReasons SELECT 'Complaint',0								--2
INSERT INTO d_RefundReasons SELECT 'Cancellation',0								--3
INSERT INTO d_RefundReasons SELECT 'Duplicate Booking',0						--4
INSERT INTO d_RefundReasons SELECT 'Test Booking',0								--5
INSERT INTO d_RefundReasons SELECT 'Downgrade',0								--6
INSERT INTO d_RefundReasons SELECT 'Discount',0									--7
INSERT INTO d_RefundReasons SELECT 'Overbooking',0								--8
--
INSERT INTO d_RefundReasons SELECT 'Chargeback',1								--9
INSERT INTO d_RefundReasons SELECT 'Pricing Error',1							--10
--
INSERT INTO d_RefundReasons SELECT 'Free Museum',0								--11
--
INSERT INTO d_RefundReasons SELECT 'Closeout',0									--12
INSERT INTO d_RefundReasons SELECT 'Ticket Unavailability',0					--13
GO

-- REFUND REASONS : GROUP PERMISSIONS
INSERT INTO d_RefundReasons_PermissionGroups SELECT 'FINANCE'					--1
INSERT INTO d_RefundReasons_PermissionGroups SELECT 'IT'						--2
INSERT INTO d_RefundReasons_PermissionGroups SELECT 'SCC'						--3
INSERT INTO d_RefundReasons_PermissionGroups SELECT 'OPS'						--4
INSERT INTO d_RefundReasons_PermissionGroups SELECT 'B2BSALES'					--5
GO

-- REFUND CLASSIFICATION TYPES
INSERT INTO [d_RefundClassification] SELECT 'Sales'								--1
INSERT INTO [d_RefundClassification] SELECT 'Product'							--2
INSERT INTO [d_RefundClassification] SELECT 'Miscellaneous'						--3
GO
-- REFUND EXTENDED REASONS
INSERT INTO d_RefundReasonsExtended SELECT 'Venue Closure',					2,1	--1
INSERT INTO d_RefundReasonsExtended SELECT 'Strike',						2,0	--2
INSERT INTO d_RefundReasonsExtended SELECT 'Weather Conditions',			2,0	--3
INSERT INTO d_RefundReasonsExtended SELECT 'Mechanical Issues',				2,0	--4
INSERT INTO d_RefundReasonsExtended SELECT 'Guide Issue',					2,0	--5
INSERT INTO d_RefundReasonsExtended SELECT 'Tour Related',					2,0	--6
INSERT INTO d_RefundReasonsExtended SELECT 'Good Will/Easy Life',			3,1	--7
INSERT INTO d_RefundReasonsExtended SELECT 'Darkrome Related',				3,0	--8
INSERT INTO d_RefundReasonsExtended SELECT 'Pax Specific',					3,0	--9
INSERT INTO d_RefundReasonsExtended SELECT 'Distributor Cancellation',		1,0	--10
INSERT INTO d_RefundReasonsExtended SELECT 'Customer Cancellation (100%)',	1,0	--11
INSERT INTO d_RefundReasonsExtended SELECT 'Illness, Death, Booking Issue',	1,0 --12
INSERT INTO d_RefundReasonsExtended SELECT 'Website, Voucher Issues',		3,0	--13
INSERT INTO d_RefundReasonsExtended SELECT 'Tour Language Issues',			3,0	--14
INSERT INTO d_RefundReasonsExtended SELECT 'Pax Reduction',					1,0	--15
INSERT INTO d_RefundReasonsExtended SELECT 'Tour Downgrade',				1,0	--16
INSERT INTO d_RefundReasonsExtended SELECT 'Adult >> Kid',					1,0	--17
INSERT INTO d_RefundReasonsExtended SELECT 'Sick/Late',						1,0	--18
INSERT INTO d_RefundReasonsExtended SELECT '4 > Tours',						1,0	--19
INSERT INTO d_RefundReasonsExtended SELECT 'Guide Related',					3,0	--20
INSERT INTO d_RefundReasonsExtended SELECT 'Venue Closure � Jubliee',		2,0	--21

GO

-- MAPPINGS

INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 1,6			-- DOWNGRADE/DOWNGRADE

INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 2,1			-- PARTIAL/TOURDISRUPTION
INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 2,2			-- PARTIAL/COMPLAINT
--INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 2,3		-- PARTIAL/CANCELLATION
INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 2,7			-- PARTIAL/DISCOUNT
--
INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 2,9			-- PARTIAL/CHARGEBACK
INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 2,10			-- PARTIAL/PRICINGERROR
INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 2,11			-- PARTIAL/FREEMUSEUM
INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 2,12			-- PARTIAL/CLOSEOUT
INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 2,13			-- PARTIAL/TICKET UNAVAILABILITY

INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 3,1			-- FULL/TOURDISRUPTION
INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 3,2			-- FULL/COMPLAINT
INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 3,3			-- FULL/CANCELLATION
INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 3,4			-- FULL/DUPLICATEBOOKING
INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 3,5			-- FULL/TESTBOOKING
INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 3,8			-- FULL/OVERBOOKING
INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 3,12			-- FULL/CLOSEOUT
INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 3,13			-- FULL/TICKET UNAVAILABILITY

--
INSERT INTO d_Mapping_RefundTypes_RefundReasons SELECT 3,9			-- FULL/CHARGEBACK
GO


INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 1,1		-- TOURDISRUPTION/VENUECLOSURE
INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 1,2		-- TOURDISRUPTION/STRIKE
INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 1,3		-- TOURDISRUPTION/WEATHERCONDITIONS
INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 1,4		-- TOURDISRUPTION/MECHANICALISSUES
INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 1,5		-- TOURDISRUPTION/GUIDEISSUE
INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 1,21		-- TOURDISRUPTION/VENUECLOSUREJUBLIEE

INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 2,6		-- COMPLAINT/TOURRELATED
INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 2,7		-- COMPLAINT/GOODWILLEASYLIFE
INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 2,8		-- COMPLAINT/DARKROMERELATED
INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 2,9		-- COMPLAINT/PAXSPECIFIC
INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 2,20		-- COMPLAINT/GUIDERELATED
INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 2,13		-- COMPLAINT/GUIDERELATED
INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 2,14		-- COMPLAINT/GUIDERELATED

INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 3,10		-- CANCELLATION/DISTRIBUTORCANCELLATION
INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 3,11		-- CANCELLATION/CUSTOMERCANCELLATION100%
INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 3,12		-- CANCELLATION/ILLNESSDEATHBOOKINGISSUE


INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 6,15		-- DOWNGRADE/PAXREDUCTION
INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 6,16		-- DOWNGRADE/TOURDOWNGRADE
INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 6,17		-- DOWNGRADE/ADULTTOKID

INSERT INTO d_Mapping_RefundReasons_RefundExtended SELECT 7,19		-- DISCOUNT/4TOURS
GO

-- ADD USERS TO PERMISSION GROUPS
INSERT INTO d_Mapping_PermissionGroups_Members SELECT 2,522			-- IT/STEPHEN.MAHONY
INSERT INTO d_Mapping_PermissionGroups_Members SELECT 2,697			-- IT/TIM.WHELAN
INSERT INTO d_Mapping_PermissionGroups_Members SELECT 1,477			-- FINANCE/AILISH.REILLY
INSERT INTO d_Mapping_PermissionGroups_Members SELECT 1,1704		-- FINANCE/AILISH.REILLY
INSERT INTO d_Mapping_PermissionGroups_Members SELECT 1,986			-- FINANCE/SEAN.RYAN
INSERT INTO d_Mapping_PermissionGroups_Members SELECT 1,428			-- FINANCE/IAN.RICHARDSON
INSERT INTO d_Mapping_PermissionGroups_Members SELECT 1,957			-- FINANCE/RONAN.OLEARY
INSERT INTO d_Mapping_PermissionGroups_Members SELECT 5,407			-- B2BSALES/JULIA
INSERT INTO d_Mapping_PermissionGroups_Members SELECT 5,1819		-- B2BSALES/B2B INTERN
INSERT INTO d_Mapping_PermissionGroups_Members SELECT 5,1550		-- B2BSALES/KANDY
INSERT INTO d_Mapping_PermissionGroups_Members SELECT 5,1962		-- B2BSALES/BEATRIZ.VILLALOBOS
GO
-- GIVE GROUPS PERMISSIONS
INSERT INTO d_Mapping_RefundReasons_PermissionGroups SELECT 9,1		--CHARGEBACK/FINANCE
INSERT INTO d_Mapping_RefundReasons_PermissionGroups SELECT 10,1	--PRICINGERROR/FINANCE
INSERT INTO d_Mapping_RefundReasons_PermissionGroups SELECT 9,2		--CHARGEBACK/IT
INSERT INTO d_Mapping_RefundReasons_PermissionGroups SELECT 10,2	--PRICINGERROR/IT
INSERT INTO d_Mapping_RefundReasons_PermissionGroups SELECT 10,5	--PRICINGERROR/B2BSALES

GO

-- select * from d_user where user_name like '%kandy%'