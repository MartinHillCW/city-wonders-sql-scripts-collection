﻿/*---------------------------------------------------------------------------------------------------------
START

Before you run this 
- replace Minerve_PreProd and CityWonder_PreProd with the required databases 
- Create the lines that will create the user (see example last line!)
	Sample:
	Exec dbo.tmp_AddUserBatch 'Forename.Surename', 'Forename', 'Surname', 'ForeName.Surname@mymail.com', 'FS'

-- Run this in one go
---------------------------------------------------------------------------------------------------------*/

Use Minerva_preprod
Go

If Exists(Select name from sys.procedures where name = 'tmp_AddUser')
  Drop Procedure dbo.tmp_AddUser
Go


Create Procedure dbo.tmp_AddUser
		@UserName		nvarchar(150),
		@ForeName		nvarchar(150),
		@SurName		nvarchar(150),
		@EMail			nvarchar(150),
		@Password		nvarchar(128),
		@Salt			nvarchar(50)

As 
 Begin
		
		If Exists(Select [Username] from dbo.SysUser Where [Username] = @UserName) 
				Return 0


		INSERT [dbo].[SysUser] (
				[Username], 
				[UserType], 
				[Forename], 
				[Surname], 
				[Email],
				[Password],
				[Salt],
				[IsAuthenticated], 
				[IsEnabled], 
				[IsDeleted], 
				[HasATemporaryPassword], 
				[FailedLoginCount], 
				[IsB2CAdmin], 
				[IsB2BAdmin], 
				[DateCreated], 
				[DateModified], 
				[LastModifiedUserId]
				)
		VALUES (	
				@UserName,		-- [Username]
				0,				-- [UserType]
				@ForeName,		-- [Forename]
				@SurName,		-- [Surname]
				@EMail,			-- [Email]
				@Password,		-- [Password]
				@Salt,			-- [Salt]
				1,				-- [IsAuthenticated]
				1,				-- [IsEnabled]
				0,				-- [IsDeleted]
				0,				-- [HasATemporaryPassword]
				0,				-- [FailedLoginCount]
				0,				-- [IsB2CAdmin]
				0,				-- [IsB2BAdmin]
				GetDate(),		-- [DateCreated]
				GetDate(),		-- [DateModified]
				1				-- [LastModifiedUserId]
				)

		Declare @MinervaID		int

		Select @MinervaID = Id From [dbo].[SysUser] Where [Username] = @UserName

		INSERT [dbo].[LegacyUserDatas] (
				[IsTourLeader], 
				[IsGuide], 
				[IsCoordinator], 
				[IsExternal], 
				[City_Id], 
				[Trainer], 
				[VaticanPass], 
				[Licensed], 
				[OutOfProvence], 
				[Comune], 
				[Spanish], 
				[English], 
				[French], 
				[Portugese], 
				[German], 
				[Italian], 
				[IsLoyalty], 
				[ReceiveGuideEmail], 
				[HiredBy], 
				[HireDate], 
				[DateCreated], 
				[DateModified], 
				[LastModifiedUserId], 
				[GuidaVaticana], 
				[GuidaNazionale], 
				[GuidaEstera], 
				[GuidePrefecture], 
				[GuideConferencierEcole], 
				[TourLeaderUnlicenced], 
				[TourLeaderLicenced], 
				[MinervaUserId] )
		VALUES (
				0,		-- [IsTourLeader]
				0,		-- [IsGuide] 
				0, 		-- [IsCoordinator]
				0, 		-- [IsExternal]
				1, 		-- [City_Id]  
				0, 		-- [Trainer]
				0, 		-- [VaticanPass]
				0, 		-- [Licensed]
				0, 		-- [OutOfProvence]
				0, 		-- [Comune]
				0, 		-- [Spanish]
				0, 		-- [English]
				0, 		-- [French]
				0, 		-- [Portugese]
				0, 		-- [German]
				0, 		-- [Italian]
				0, 		-- [IsLoyalty]
				0, 		-- [ReceiveGuideEmail]
				1, 		-- [HiredBy]
				Cast(GetDate() as Date),			-- [HireDate]
				GetDate(),	-- [DateCreated]
				GetDate(),	-- [DateModified]
				1, 		-- [LastModifiedUserId]
				0,		-- [GuidaVaticana]
				0,		-- [GuidaNazionale]
				0,		-- [GuidaEstera]
				0,		-- [GuidePrefecture]
				0,		-- [GuideConferencierEcole]
				0,		-- [TourLeaderUnlicenced]
				0,		-- [TourLeaderLicenced]
				@MinervaID	-- [MinervaUserId]
				)

	Return @MinervaID * (-1)		-- make it negative to distinct between return state and ID
End
Go

If Exists(Select name from sys.procedures where name = 'tmp_AddUserRole')
  Drop Procedure dbo.tmp_AddUserRole
Go


Create Procedure dbo.tmp_AddUserRole
	@MinervaID			int,
	@Role				nvarchar(255)
as 
Begin

	Declare @RoleID		int
	Select @RoleID = id from dbo.PermissionGroups Where Name = @Role

	If (@RoleID is null) 
		return -1

	INSERT [dbo].[PermissionGroupSysUsers]([PermissionGroup_Id], [SysUser_Id]) VALUES (@RoleID, @MinervaID)

End
Go

If Exists(Select name from sys.procedures where name = 'tmp_AddUserBatch')
  Drop Procedure dbo.tmp_AddUserBatch
Go


Create Procedure dbo.tmp_AddUserBatch
		@UserName		nvarchar(150),
		@ForeName		nvarchar(150),
		@SurName		nvarchar(150),
		@EMail			nvarchar(150),
		@Initials		nvarchar(5),
		@Password		nvarchar(128),
		@Salt			nvarchar(50)
As 
Begin

	Declare @MinervaID int = 0

	-- preserve password and salt if user exists
	Select @MinervaID = Id From [dbo].[SysUser] Where [Username] = @UserName
	If @MinervaID > 0
	  begin
			Declare @OldPassword	nvarchar(128),
					@OldSalt		nvarchar(50)

			Select	@OldPassword = [Password],
					@OldSalt	 = [Salt]
			From [dbo].[SysUser] Where [Username] = @UserName

			If (RTrim(@OldPassword) <> '')
			   Begin
					Set @Password = @OldPassword
					Set @Salt = @OldSalt
			   End
	  end
	
	-- delete user user if exists
	Exec dbo.tmp_DeleteUser @UserName

	-- create new user 
	Exec @MinervaID = dbo.tmp_AddUser @UserName, @ForeName,	@SurName, @EMail, @Password, @Salt

	If (@MinervaID >= 0) return @MinervaID

	Set @MinervaID = @MinervaID * (-1)

	Exec dbo.tmp_AddUserRole @MinervaID, 'View Supplier Users'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Manage Supplier Users'
	Exec dbo.tmp_AddUserRole @MinervaID, 'View Guide Users'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Manage Guide Users'
	Exec dbo.tmp_AddUserRole @MinervaID, 'View Users'
	Exec dbo.tmp_AddUserRole @MinervaID, 'View Roles and Permissions'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Default Permissions'
	Exec dbo.tmp_AddUserRole @MinervaID, 'B2B Manager'
	Exec dbo.tmp_AddUserRole @MinervaID, 'B2B Account Manager'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Ops Supervisor'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Ops B2B Data Entry'
	Exec dbo.tmp_AddUserRole @MinervaID, 'SCC Supervisor'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Purchasing'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Forecasting'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Guide Scheduling'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Inventory'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Product'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Guide Management'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Coordinator'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Coordination Management'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Manage Product Options'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Product Management'


	Exec citywonders_preprod.dbo.tmp_AddLegacyUser @UserName, @ForeName, @SurName, @EMail, @Initials, @MinervaID, @Password

	-- Return @MinervaID * (-1)
End
Go

If Exists(Select name from sys.procedures where name = 'tmp_DeleteUser')
  Drop Procedure dbo.tmp_DeleteUser
Go

Create Procedure dbo.tmp_DeleteUser
		@UserName		nvarchar(150)
As 
 Begin
	 Declare @MinervaID	int,
			@LegacyID   int

	Select @MinervaID = Id From [dbo].[SysUser] Where [Username] = @UserName
	If @MinervaID > 0
	  begin
			Delete dbo.PermissionGroupSysUsers Where SysUser_Id = @MinervaID
			Delete dbo.LegacyUserDatas Where MinervaUserId = @MinervaID
			Delete dbo.SysUserTokens Where SysUserId = @MinervaID
			Delete dbo.SysUserWidgets  Where SysUserId = @MinervaID
			Delete dbo.SysUser Where id = @MinervaID
	  end


	Select @LegacyID = [user_id] From CityWonders_PreProd.dbo.d_User Where [User_Name] = @UserName
	If @LegacyID > 0 
	 begin
		Delete CityWonders_PreProd.dbo.UsersBridge Where [user_id] =  @LegacyID
		Delete CityWonders_PreProd.dbo.d_user_modulo WHERE [user_id] = @LegacyID
		Delete CityWonders_PreProd.dbo.d_user_language WHERE [USER_ID] = @LegacyID
		Delete CityWonders_PreProd.dbo.d_user WHERE [user_id] = @LegacyID
	 end

 End
Go


Use citywonders_preprod
go

If Exists(Select name from sys.procedures where name = 'tmp_AddLegacyUser')
  Drop Procedure dbo.tmp_AddLegacyUser
Go

Create Procedure dbo.tmp_AddLegacyUser
		@UserName		nvarchar(150),
		@ForeName		nvarchar(150),
		@SurName		nvarchar(150),
		@EMail			nvarchar(150),
		@Initials		nvarchar(5),
		@MinervaID		int,
		@Password		nvarchar(50)
As
Begin

	Declare @HireDate varchar(8) = Convert(varchar, GetDate(), 112)

	Exec dbo.SP_USERS_ADD 
		@user_name = @UserName,
		@user_pwd = @Password,
		@supplier_id = NULL,
		@user_email = @EMail,
		@user_phone = NULL,
		@user_flagGuide = 0,
		@user_flagAdministrative = 1,
		@user_flagInServizio = 1,
		@user_firstName = @ForeName,
		@user_lastName = @SurName,
		@user_emailVisibile = 0,
		@user_phoneVisibile = 0,
		@user_office_city_id = 1,
		@user_lavoraTanto = 0,
		@user_acceptBigGroups = 0,
		@user_initials = @Initials,
		@user_flagBasilicaPass = 0,
		@user_page = N'defaultUser.aspx',
		@IsSupplier = 0,
		@user_supplierType = NULL,
		@auduser = 1,
		@user_B2BAdmin = 0,
		@user_B2CAdmin = 0,
		@user_flagCoordinator = 0,
		@user_flagAccompagnatore = 0,
		@user_IsGuide = 0,
		@user_isGuidaVaticana = 0,
		@user_isGuidaNazionale = 0,
		@user_isGuidaEstera = 0,
		@user_isGuidePrefecture = 0,
		@user_isGuideConferencierEcole = 0,
		@user_isTourLeaderUnlicenced = 0,
		@user_isTourLeaderLicenced = 0,
		@user_italianLicenceRegions = NULL,
		@user_italianLicenceProvinces = NULL,
		@city_id = 1,
		@user_notes = NULL,
		@user_rating = NULL,
		@user_flagVaticanPass = 0,
		@user_coordinatorNotes = NULL,
		@user_flagTrainer = 0,
		@user_Address = NULL,
		@user_HireDate = @HireDate,
		@Receive_Guide_Email = 0,
		@HiredBy = 522,
		@ExternalGuide = 0,
		@ExternalGuideType = NULL,
		@OutOfProvince = 0,
		@user_flagComunePass = 0,
		@user_flagPackage = 0,
		@user_flagLicensed = 0,
		@AttendedGuideSchool = NULL,
		@RateLevel = NULL,
		@languageIdsList = N''

	Declare @UserID		int
	Select @UserID = [user_id] From d_User Where [User_Name] = @UserName

	INSERT INTO UsersBridge (UserId, [user_id]) VALUES (@MinervaID, @UserID)

	DELETE FROM d_user_modulo WHERE [user_id] = @UserID

	INSERT INTO d_user_modulo  (user_id, modulo_id,user_modulo_readOnly) 
	SELECT @UserID,1,1  UNION ALL SELECT @UserID,2,1  UNION ALL SELECT @UserID,3,1  UNION ALL SELECT @UserID,4,1  UNION ALL SELECT @UserID,5,1  UNION ALL SELECT @UserID,6,1  UNION ALL SELECT @UserID,7,1  UNION ALL SELECT 
	@UserID,8,1  UNION ALL SELECT @UserID,9,1  UNION ALL SELECT @UserID,10,1  UNION ALL SELECT @UserID,11,1  UNION ALL SELECT @UserID,12,1  UNION ALL SELECT @UserID,13,1  UNION ALL SELECT @UserID,14,1  UNION ALL SELECT @UserID,15,1  UNION ALL SELECT @UserID,16,1  UNION ALL SELECT @UserID,17,1  UNION ALL 
	SELECT @UserID,22,1  UNION ALL SELECT @UserID,23,1  UNION ALL SELECT @UserID,24,1  UNION ALL SELECT @UserID,25,1  UNION ALL SELECT @UserID,26,0  UNION ALL SELECT @UserID,27,0  UNION ALL SELECT @UserID,32,1  UNION ALL SELECT @UserID,33,1  UNION ALL SELECT @UserID,34,1  UNION ALL SELECT @UserID,35,1  UNION ALL SELECT @UserID,36,1  UNION ALL SELECT @UserID,38,1  UNION ALL SELECT @UserID,39,0  UNION ALL SELECT @UserID,40,1  UNION ALL SELECT @UserID,41,1  UNION ALL SELECT @UserID,42,1  UNION ALL SELECT @UserID,43,1  UNION ALL SELECT @UserID,44,1  UNION ALL SELECT @UserID,45,1  UNION ALL SELECT @UserID,46,1  UNION ALL SELECT @UserID,48,1  UNION ALL SELECT @UserID,49,1  UNION ALL SELECT @UserID,50,1  UNION ALL SELECT @UserID,51,1  UNION ALL SELECT @UserID,52,1  UNION ALL SELECT @UserID,55,1  UNION ALL SELECT @UserID,57,1  UNION ALL SELECT @UserID,58,1  UNION ALL SELECT @UserID,59,1  UNION ALL SELECT @UserID,61,1  UNION ALL SELECT @UserID,64,1  UNION ALL SELECT @UserID,65,1  UNION ALL SELECT @UserID,66,1  UNION ALL SELECT @UserID,67,1  UNION ALL SELECT @UserID,68,1  UNION ALL SELECT @UserID,69,1  UNION ALL SELECT @UserID,70,1  UNION ALL SELECT @UserID,73,1  UNION ALL SELECT @UserID,74,1  UNION ALL SELECT @UserID,75,1  UNION ALL SELECT @UserID,76,1  UNION ALL SELECT @UserID,77,1  UNION ALL SELECT @UserID,78,1  UNION ALL SELECT @UserID,79,1  UNION ALL SELECT @UserID,80,0  UNION ALL SELECT @UserID,82,1  UNION ALL SELECT @UserID,84,1  UNION ALL SELECT @UserID,85,1  UNION ALL SELECT @UserID,86,1  UNION ALL SELECT @UserID,87,1  UNION ALL SELECT @UserID,88,1  UNION ALL SELECT @UserID,89,1  UNION ALL SELECT @UserID,91,1  UNION ALL SELECT @UserID,92,1  UNION ALL SELECT @UserID,94,1  UNION ALL SELECT @UserID,95,1  UNION ALL SELECT @UserID,96,1  UNION ALL SELECT @UserID,97,1  UNION ALL SELECT @UserID,98,1  UNION ALL SELECT @UserID,99,1  UNION ALL SELECT @UserID,100,1  UNION ALL SELECT @UserID,101,1  UNION ALL SELECT @UserID,105,1  UNION ALL SELECT @UserID,106,0  UNION ALL SELECT @UserID,107,1  UNION ALL SELECT @UserID,108,1  UNION ALL SELECT @UserID,109,1  UNION ALL SELECT @UserID,110,1  UNION ALL SELECT @UserID,111,1  UNION ALL SELECT @UserID,113,1  UNION ALL SELECT @UserID,114,1  UNION ALL SELECT @UserID,116,1  UNION ALL SELECT @UserID,117,1  UNION ALL SELECT @UserID,118,1  UNION ALL SELECT @UserID,119,1  UNION ALL SELECT @UserID,120,1  UNION ALL SELECT @UserID,121,1  UNION ALL SELECT @UserID,122,1  UNION ALL SELECT @UserID,123,1  UNION ALL SELECT @UserID,124,1  UNION ALL SELECT @UserID,125,1  UNION ALL SELECT @UserID,126,1  UNION ALL SELECT @UserID,127,1  UNION ALL SELECT @UserID,128,1  UNION ALL SELECT @UserID,129,1  UNION ALL SELECT 
	@UserID,130,1  UNION ALL SELECT @UserID,131,1  UNION ALL SELECT @UserID,132,1  UNION ALL SELECT @UserID,133,1  UNION ALL SELECT @UserID,134,1  UNION ALL SELECT @UserID,136,1  UNION ALL SELECT @UserID,137,1  UNION ALL SELECT @UserID,139,1  UNION ALL SELECT @UserID,140,1  UNION ALL SELECT 
	@UserID,141,1  UNION ALL SELECT @UserID,142,1  UNION ALL SELECT @UserID,143,1  UNION ALL SELECT @UserID,145,1  UNION ALL SELECT @UserID,146,1  UNION ALL SELECT @UserID,147,1  UNION ALL SELECT @UserID,148,1  UNION ALL SELECT @UserID,149,1  UNION ALL SELECT @UserID,150,1  UNION ALL SELECT 
	@UserID,151,1  UNION ALL SELECT @UserID,152,1  UNION ALL SELECT @UserID,154,1  UNION ALL SELECT @UserID,155,1  UNION ALL SELECT @UserID,156,1  UNION ALL SELECT @UserID,157,1  UNION ALL SELECT @UserID,159,1  UNION ALL SELECT @UserID,160,1  UNION ALL SELECT @UserID,162,1  UNION ALL SELECT 
	@UserID,163,1  UNION ALL SELECT @UserID,164,1  UNION ALL SELECT @UserID,165,1  UNION ALL SELECT @UserID,166,1  UNION ALL SELECT @UserID,167,1  UNION ALL SELECT @UserID,168,1  UNION ALL SELECT @UserID,169,1  UNION ALL SELECT @UserID,170,1  UNION ALL SELECT @UserID,171,1  UNION ALL SELECT 
	@UserID,172,1  UNION ALL SELECT @UserID,173,1  UNION ALL SELECT @UserID,174,1  UNION ALL SELECT @UserID,175,1  UNION ALL SELECT @UserID,176,1  UNION ALL SELECT @UserID,177,1  UNION ALL SELECT @UserID,178,1  UNION ALL SELECT @UserID,179,1  UNION ALL SELECT @UserID,180,1  UNION ALL SELECT 
	@UserID,181,1  UNION ALL SELECT @UserID,182,1  UNION ALL SELECT @UserID,183,1  UNION ALL SELECT @UserID,184,1  UNION ALL SELECT @UserID,185,1  UNION ALL SELECT @UserID,186,1  UNION ALL SELECT @UserID,187,1  UNION ALL SELECT @UserID,188,1  UNION ALL SELECT @UserID,189,1  UNION ALL SELECT 
	@UserID,190,1  UNION ALL SELECT @UserID,191,1  UNION ALL SELECT @UserID,192,1  UNION ALL SELECT @UserID,193,1  UNION ALL SELECT @UserID,194,1  UNION ALL SELECT @UserID,195,1  UNION ALL SELECT @UserID,196,1  UNION ALL SELECT @UserID,197,1  UNION ALL SELECT @UserID,198,1  UNION ALL SELECT 
	@UserID,199,1  UNION ALL SELECT @UserID,200,1  UNION ALL SELECT @UserID,201,1  UNION ALL SELECT @UserID,202,1  UNION ALL SELECT @UserID,203,1  UNION ALL SELECT @UserID,204,1  UNION ALL SELECT @UserID,205,1  UNION ALL SELECT @UserID,207,1  UNION ALL SELECT @UserID,208,1  UNION ALL SELECT @UserID,209,1  UNION ALL SELECT @UserID,211,1  UNION ALL SELECT @UserID,212,1  UNION ALL SELECT @UserID,213,1  UNION ALL SELECT @UserID,214,1  UNION ALL SELECT @UserID,217,1  UNION ALL SELECT @UserID,218,1  UNION ALL SELECT @UserID,219,1  UNION ALL SELECT @UserID,220,1  UNION ALL SELECT @UserID,221,1  UNION ALL SELECT @UserID,222,1  UNION ALL SELECT @UserID,224,1  UNION ALL SELECT @UserID,225,1  UNION ALL SELECT @UserID,226,1  UNION ALL SELECT @UserID,227,1  UNION ALL SELECT @UserID,228,1  UNION ALL SELECT @UserID,230,1  UNION ALL SELECT @UserID,232,1  UNION ALL SELECT @UserID,233,1  UNION ALL SELECT @UserID,234,1  UNION ALL SELECT @UserID,235,1  UNION ALL SELECT @UserID,236,1  UNION ALL SELECT @UserID,237,1  UNION ALL SELECT @UserID,238,1  UNION ALL SELECT @UserID,239,1  UNION ALL SELECT @UserID,240,1  UNION ALL SELECT @UserID,241,1  UNION ALL SELECT @UserID,242,1  UNION ALL SELECT @UserID,243,1  UNION ALL SELECT @UserID,245,1  UNION ALL SELECT @UserID,252,1  UNION ALL SELECT @UserID,254,1  UNION ALL SELECT @UserID,255,1  UNION ALL SELECT @UserID,256,1  UNION ALL SELECT @UserID,257,1  UNION ALL SELECT @UserID,267,1  UNION ALL SELECT @UserID,269,1  UNION ALL SELECT @UserID,280,1  UNION ALL SELECT @UserID,281,1  UNION ALL SELECT @UserID,294,1  UNION ALL SELECT @UserID,295,1  UNION ALL SELECT @UserID,63,1  UNION ALL SELECT @UserID,62,1  UNION ALL SELECT @UserID,28,1  UNION ALL SELECT @UserID,37,1  UNION ALL SELECT @UserID,83,1  UNION ALL SELECT @UserID,47,1  UNION ALL SELECT @UserID,135,1  UNION ALL SELECT @UserID,102,1  UNION ALL SELECT @UserID,138,1  UNION ALL SELECT @UserID,229,1  UNION ALL SELECT @UserID,231,1  UNION ALL SELECT @UserID,251,1  UNION ALL SELECT @UserID,253,1  UNION ALL SELECT @UserID,268,1  UNION ALL SELECT @UserID,250,1

end
Go


/*
Konrad Balys		kbalys@future-processing.com
Mateusz Łyżwiński	mlyzwinski@future-processing.com
Daniel Jaros		djaros@future-processing.com
Bartłomiej Glac		bglac@future-processing.com
Daniel Feist		dfeist@future-processing.com
Rafał Ostrowski		rostrowski@future-processing.com
Piotr Opaczyński	popaczynski@future-processing.com

Use Minerva_preprod
Go

Declare	@UserName		nvarchar(150),
		@ForeName		nvarchar(150),
		@SurName		nvarchar(150),
		@EMail			nvarchar(150),
		@Initials		nvarchar(5),
		@MinervaID		int

Select	@UserName = 'Konrad.Balys',
		@ForeName = 'Konrad',
		@SurName = 'Balys',
		@EMail = 'kbalys@future-processing.com',
		@Initials = 'KBa'

Exec @MinervaID = dbo.tmp_AddUserBatch @UserName, @ForeName, @SurName, @EMail
Exec citywonders_preprod.dbo.tmp_AddLegacyUser @UserName, @ForeName, @SurName, @EMail, @Initials, @MinervaID

*/



Use Minerva_preprod
Go

Exec dbo.tmp_AddUserBatch 'Konrad.Balys', 'Konrad', 'Balys', 'kbalys@future-processing.com', 'FP_KB', 'VBKRdjIfkm42aWKmZxNitVxepmvj63u/hYoCsDL8Wkw=', 'Qqr0Rr9FQgDBma6V'
Exec dbo.tmp_AddUserBatch 'Mateusz.Łyżwiński', 'Mateusz', 'Łyżwiński', 'mlyzwinski@future-processing.com', 'FP_MT', '0UcK8dLwwz6FBLYMLrQA9o6FNDU4gudq1s3ngnbtw0w=', 'GfuFVFIgpJiXPXrF'
Exec dbo.tmp_AddUserBatch 'Daniel.Jaros', 'Daniel', 'Jaros', 'djaros@future-processing.com', 'FP_DJ', 'sEg2icjaGQ4cP41UPGqpyjK5BtwpYcN8kUK/RKiNMCw=', 'zmc3yj2dIcZkH8Rr'
Exec dbo.tmp_AddUserBatch 'Bartłomiej.Glac', 'Bartłomiej', 'Glac', 'bglac@future-processing.com', 'FP_BG', '8qa6yb4+jXiCYv3I6stmApgK+UX80PYAYPNIrU6q0mQ=', 'bxV+5XOilKSsS2My'
Exec dbo.tmp_AddUserBatch 'Daniel.Feist', 'Daniel', 'Feist', 'dfeist@future-processing.com', 'FP_DF', 'clPzbX9RuJ+wYXVld8zEuYc4aVd6tVCSdDQFb0y5wRs=',      'uZzJZS7b3Gt7tQpq'
Exec dbo.tmp_AddUserBatch 'Rafał.Ostrowski', 'Rafał', 'Ostrowski', 'rostrowski@future-processing.com', 'FP_RO', 'ie4rErl44zEDmZlbzyvhwrc+AE6uJUFNj7Qlmk7ACNc=', 'yD47YiMBrsIaxzMU'
Exec dbo.tmp_AddUserBatch 'Piotr.Opaczyński', 'Piotr', 'Opaczyński', 'popaczynski@future-processing.com', 'FP_PO', 'SaeoUYWLitgFldHHaEjT8xIkCzXAhJuZ6NmGKH4JUsI=', 'INzxT1npCrYpnb/B'

--Daniel.Feist			- Daniel.Feist123
--Rafał.Ostrowski		- Ostrowski321!
--Piotr.Opaczyński		- Piotr789*!

/* -- Delete User Account

Use Minerva_preprod
Go

Exec dbo.tmp_DeleteUser 'Konrad.Balys'
Exec dbo.tmp_DeleteUser 'Mateusz.Łyżwiński'
Exec dbo.tmp_DeleteUser 'Daniel.Jaros'
Exec dbo.tmp_DeleteUser 'Bartłomiej.Glac'
Exec dbo.tmp_DeleteUser 'Daniel.Feist'
Exec dbo.tmp_DeleteUser 'Rafał.Ostrowski'
Exec dbo.tmp_DeleteUser 'Piotr.Opaczyński'
 		

*/



/* TEST
Select * from [SysUser] Where Username like '%Glac%'
-- Minerva ID 
Select * From [LegacyUserDatas] where MinervaUserId = 3720
Select * from [PermissionGroupSysUsers] Where SysUser_Id = 3720
Select * from citywonders_preprod.dbo.d_user where [user_name] = 'Daniel Feist'
-- Legacy User ID
Select * from citywonders_preprod.dbo.UsersBridge where [user_id] = 3765
Select * from citywonders_preprod.dbo.d_user_modulo WHERE [user_id] = 3765
*/




