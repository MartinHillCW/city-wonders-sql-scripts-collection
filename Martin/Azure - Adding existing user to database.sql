If Exists(Select * from sys.database_principals where name = 'App.Minerva') 
   Drop User [App.Minerva]
Go

If Exists(Select * from sys.database_principals where name = 'App.CityWonders') 
   Drop User [App.CityWonders]
Go

CREATE USER [App.Minerva]
	FOR LOGIN [App.Minerva]
	WITH DEFAULT_SCHEMA = dbo
GO

EXEC sp_addrolemember N'db_owner', N'App.Minerva'
GO

CREATE USER [App.CityWonders]
	FOR LOGIN [App.CityWonders]
	WITH DEFAULT_SCHEMA = dbo
GO

EXEC sp_addrolemember N'db_owner', N'App.CityWonders'
GO

