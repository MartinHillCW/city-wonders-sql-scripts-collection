-- create availability counter - no index
Create Table tmp.Availability_Counter (
		OID						int	IDENTITY		NOT Null,
		AC_GUID					uniqueidentifier 	NULL,
		CounterTime				datetime2			NULL Default SysDateTime(),
		DistributorID			int					NULL,
		FromDate				date				NULL,
		ToDate					date				NULL,
		Products				varchar(500)		NULL,
		Constraint PK_tmp_Availability_Counter Primary Key Clustered (OID)
)

-- Drop Table tmp.Availability_Counter
-- Truncate Table tmp.Availability_Counter

/* Sample Insert

-- at the start of the procedure
Declare @AC_GUID uniqueidentifier =  NEWID()
Insert tmp.Availability_Counter ( AC_GUID, DistributorID, FromDate, ToDate, Products )
		Values (@AC_GUID, @Distributor, @FromDate, @ToDate, @ProductArray)

-- at the end of the procedure
Insert tmp.Availability_Counter ( AC_GUID, DistributorID, FromDate, ToDate, Products )
		Values (@AC_GUID, null, null, null, null)

*/
		