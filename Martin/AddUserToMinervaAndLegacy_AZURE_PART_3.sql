USE [MinervaIntegration]
GO

BEGIN TRAN

DECLARE @date DATETIME = GETDATE();

-- New Permissions for NBO
SELECT *
INTO #NboPermissions
FROM (
	VALUES ('View Experiences List')
		,('View Experience Details')
		,('View Prices')
		,('Manage Prices')
		,('Export Price History')
		,('Get Lowest Rrp Prices')
		,('Get Rrp Prices')
		,('Get Total Rrp Price')
	) AS Source([Name])

-- Insert NBO Permissions if necessary
MERGE dbo.[Permissions] AS Target
USING (
	SELECT *
	FROM #NboPermissions
	) AS source
	ON target.[Name] = source.[Name]
WHEN NOT MATCHED
	THEN
		INSERT (
			[Name]
			,Action
			,DateCreated
			,DateModified
			,IsDeleted
			,LastModifiedUserId
			)
		VALUES (
			Source.[Name]
			,NULL
			,@date
			,@date
			,0
			,1
			);

			-- Insert Permission Groups (Roles) if necessary
MERGE dbo.PermissionGroups AS Target
USING (
	VALUES ('NBO')
	) AS Source([Name])
	ON target.[Name] = source.[Name]
WHEN NOT MATCHED
	THEN
		INSERT (
			[Name]
			,DateCreated
			,DateModified
			,IsDeleted
			,LastModifiedUserId
			)
		VALUES (
			Source.[Name]
			,@date
			,@date
			,0
			,1
			);

-- Add Nbo Permissions to Nbo Permission Group
MERGE dbo.PermissionPermissionGroups AS Target
USING (
	SELECT pg.Id AS PermissionGroup_Id
		,p.Id AS Permission_Id
	FROM dbo.PermissionGroups pg
	CROSS JOIN dbo.[Permissions] p
	WHERE pg.[Name] = 'Nbo'
		AND p.[Name] IN (
			SELECT [Name]
			FROM #NboPermissions
			)
	) AS Source
	ON target.PermissionGroup_Id = source.PermissionGroup_Id
WHEN NOT MATCHED
	THEN
		INSERT (
			PermissionGroup_Id
			,Permission_Id
			)
		VALUES (
			Source.PermissionGroup_Id
			,Source.Permission_Id
			);

-- Add NBO Permission Group to all users
MERGE dbo.PermissionGroupSysUsers AS Target
USING (
	SELECT pg.Id AS PermissionGroup_Id
		,su.Id AS SysUser_Id
	FROM dbo.PermissionGroups pg
	CROSS JOIN dbo.SysUser su
	WHERE pg.[Name] = 'Nbo'
	) AS Source
	ON target.PermissionGroup_Id = source.PermissionGroup_Id
		AND target.SysUser_Id = source.SysUser_Id
WHEN NOT MATCHED
	THEN
		INSERT (
			PermissionGroup_Id
			,SysUser_Id
			)
		VALUES (
			Source.PermissionGroup_Id
			,Source.SysUser_Id
			);


DROP TABLE #NboPermissions

COMMIT TRAN