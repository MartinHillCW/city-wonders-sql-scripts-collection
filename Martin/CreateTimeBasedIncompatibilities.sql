
-- all current and future tours per city that have a valid end time 
Select 
	cou.COU_name				Country,
	c.city_desc					City,
	p.product_id				ProductID,
	pp.price_id					TourOptionID,
	p.product_titleNEW			Product,
	pp.price_desc				TourOption,
	Convert(datetime, pp.price_startTime)		StartTime,
	Convert(datetime, pp.price_endTime)			EndTime,
	pp.price_unavailable_type	TimeSlot,
	pp.price_validoDa			StartDate,
	pp.price_validoA			EndDate	
Into #Tours	
From dbo.d_product_price pp 
		Inner Join dbo.d_product p on pp.product_id = p.product_id
		Inner Join dbo.d_city c on c.city_id = p.city_id
		Inner Join dbo.D_COUNTRY cou on cou.id = c.CountryId
Where 
	pp.price_validoA >= GetDate() And
	IsDate(pp.price_endTime) = 1

Select 	T.TourOptionID,
	T1.TourOptionID			TourOptionID_Incomp
Into #Tours_Incomp
From #Tours T
		Inner Join #Tours T1 on T.City = T1.City And									-- same city
								T.TourOptionID <> T1.TourOptionID And					-- other tour option
								T1.StartTime >= T.StartTime And							-- second tour has to start on or after
								T1.StartTime < DateAdd(minute, 90, T.EndTime) And		-- second tour is within the time limit of the first
								T.StartDate <= T1.EndDate And							-- tour option are intercepting (date period wise)
								T.EndDate >= T1.StartDate

Create Table #Incomp
	(	TourOptionID1		int,
		TourOptionID2		int)

Insert #InComp
	Select Distinct TourOptionID, TourOptionID_Incomp from #Tours_Incomp
--Insert #InComp
--	Select Distinct TourOptionID_Incomp, TourOptionID from #Tours_Incomp

Delete I
From #Incomp I
			Inner Join d_product_price_incompatible pp on I.TourOptionID1 = pp.price_id And
														  I.TourOptionID2 = pp.price_id_incompatible


Delete I
From #Incomp I
			Inner Join #Tours T1 on T1.TourOptionID = I.TourOptionID1
			Inner Join #Tours T2 on T2.TourOptionID = I.TourOptionID2
Where T1.TimeSlot = T2.TimeSlot	Or
	  (T1.TimeSlot = 'full' And T2.TimeSlot in ('am','pm')) Or
	  (T2.TimeSlot = 'full' And T1.TimeSlot in ('am','pm')) 


Select 
	T1.Country,
	T1.City,
	T1.ProductID,
	T1.TourOptionID,
	T1.Product,
	T1.TourOption,
	T1.TimeSlot,
	Convert(varchar(5), T1.StartTime, 114)		StartTime,
	Convert(varchar(5), T1.EndTime, 114)		EndTime,
	'  '										NextTour,
	Convert(varchar(5), T2.StartTime, 114)		StartTime,
	Convert(varchar(5), T2.EndTime, 114)		EndTime,
	T2.ProductID,
	T2.TourOptionID,
	T2.Product,
	T2.TourOption,
	T2.TimeSlot
From #Incomp I
			Inner Join #Tours T1 on T1.TourOptionID = I.TourOptionID1
			Inner Join #Tours T2 on T2.TourOptionID = I.TourOptionID2
Order By T1.Country, T1.City, T1.Product, T1.TourOption

Drop Table #Tours	
Drop Table #Tours_Incomp
Drop Table #Incomp

