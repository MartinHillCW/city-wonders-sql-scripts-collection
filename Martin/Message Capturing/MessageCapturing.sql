use citywonders_preprod
/*


-- CREATE THE TABLE AND THE PROCEDURE

Drop Table [appsys].[Messages]
CREATE TABLE [appsys].[Messages](
	MessageID			int IDENTITY(1,1)	NOT NULL,
	MessageCode			varchar(100)		NOT NULL,
	SourceID			uniqueidentifier 	NOT NULL,			
	MessageDateTime		datetime2			NOT NULL,
	MessageText			nvarchar(2000)		NOT NULL
) ON [PRIMARY]
GO

Alter Procedure appsys.AddMessage 
	@SourceID			uniqueidentifier,	
	@SysDateTime		DATETIME2 = NULL,
	@MessageCode		varchar(100),		
	@MessageText		nvarchar(2000)
As
Begin
	Insert appsys.Messages ( MessageCode, SourceID, MessageDateTime, MessageText )
		Values ( @MessageCode, @SourceID, ISNULL(@SysDateTime,SysDateTime()), @MessageText)
End
Go

*/


/*
		SAMPLES SAMPLES SAMPLES SAMPLES SAMPLES SAMPLES SAMPLES SAMPLES 
*/

If Object_ID('_SP_MAIN') IS NOT NULL
	Drop Procedure _SP_MAIN
If Object_ID('_SP_CHILD') IS NOT NULL
	Drop Procedure _SP_CHILD
Go

Create Procedure _SP_CHILD
	@ChangeSetID			uniqueidentifier
As
Begin

	Declare @Message nvarchar(2000)
	
	-- SAMPLES
	Exec appsys.AddMessage @ChangeSetID, 'INFO', 'Child Process is executing'

	-- SAMPLE with some random data to report
	Declare @CountOfRecords int
	Select @CountOfRecords = Count(*) from d_booking where booking_date >= '20190101'
	Set @Message = 'Bookings since 1st Januar 2019 : ' + Convert(varchar(10), @CountOfRecords)

	Exec appsys.AddMessage @ChangeSetID, '', @Message

	-- Various samples more data style / processed by code afterwards

	-- encode the nature of the information in the Message Code
	Exec appsys.AddMessage @ChangeSetID, 'DOUBLE RECORD', '1'
	Exec appsys.AddMessage @ChangeSetID, 'DOUBLE RECORD', '2'

	-- encode the nature of the information in the message
	Exec appsys.AddMessage @ChangeSetID, '', 'MISSING ID:3'


	Exec appsys.AddMessage @ChangeSetID, 'NEW', '3,4,5,6'

	Exec appsys.AddMessage @ChangeSetID, 'DISPLAY', 'Please correct the issues with double records.'

	-- Example for error capturing in catch sections
	Exec appsys.AddMessage @ChangeSetID, 'Error', @@ERROR

End
Go

Create Procedure _SP_MAIN
As
Begin

	-- the main procedure creates a UID value - all messages are stored with this value
	Declare @ChangeSetID	uniqueidentifier = NEWID()
	
	-- do stuff

	-- the first call to message table should include a process name so the child processes don't need to be given the name as parameter
	-- however - the child processes CAN add own process names
	Exec appsys.AddMessage @ChangeSetID, 'Option Splitting Add (this is the message code)', 'Start of process'

	-- Execute Child procedure sample / the UID has to be given as parameter so the child process can add additional messages of required
	Exec _SP_CHILD @ChangeSetID

	-- the main procedure returns the change set ID
	Select @ChangeSetID	ChangeSetID
	Return 0

End
Go

-- example (in C# it will be the DATASET returned)

Create Table #ID ( SourceID uniqueidentifier )
Insert #ID Exec _SP_MAIN
Select M.* from appsys.Messages M Inner Join #ID On M.SourceID = #ID.SourceID
Drop Table #ID


	
