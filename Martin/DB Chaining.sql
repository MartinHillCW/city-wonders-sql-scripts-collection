Use DataWarehouse
Go

EXEC sp_changedbowner 'sa';
GO 

Use PlayAround 
Go

EXEC sp_changedbowner 'sa';
GO 


ALTER DATABASE PlayAround SET DB_CHAINING ON;  
ALTER DATABASE DataWarehouse SET DB_CHAINING ON; 

ALTER DATABASE PlayAround SET DB_CHAINING OFF;  
ALTER DATABASE DataWarehouse SET DB_CHAINING OFF; 


