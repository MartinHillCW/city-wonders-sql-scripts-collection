﻿-- FOR IP: 89.25.216.1

-- RUN ON MASTER In AZURE
CREATE LOGIN [kbalys.future-processing] WITH PASSWORD = 'M)Te45eLq9'	-- Konrad Balys
CREATE LOGIN [bglac.future-processing]	WITH PASSWORD = '(V!4ihwaUc'	-- Bartłomiej Glac

-- RUN ON EACH DATABASE (IMPORTANT: ON THE MASTER DATABASE ONLY CREATE THE USER!)

CREATE USER [kbalys.future-processing] FROM LOGIN [kbalys.future-processing];
EXEC sp_addrolemember 'db_owner', 'kbalys.future-processing'
-- GRANT VIEW ANY DEFINITION TO kbalys.future-processing
-- EXEC sp_droprolemember 'db_datareader', 'kbalys.future-processing'
-- EXEC sp_droprolemember 'db_owner', 'kbalys.future-processing'

CREATE USER [bglac.future-processing] FROM LOGIN [bglac.future-processing]
EXEC sp_addrolemember 'db_owner', 'bglac.future-processing'
-- EXEC sp_droprolemember 'db_datareader', 'bglac.future-processing'
-- EXEC sp_droprolemember 'db_owner', 'bglac.future-processing'

-- check permission
SELECT r.name role_principal_name, m.name AS member_principal_name
	FROM sys.database_role_members rm 
			JOIN sys.database_principals r  ON rm.role_principal_id = r.principal_id
			JOIN sys.database_principals m   ON rm.member_principal_id = m.principal_id
Where m.name like '%future%'


