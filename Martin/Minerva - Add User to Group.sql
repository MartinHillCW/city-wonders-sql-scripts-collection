--------------------------------------------------------------------
--- Minerva / Legacy: Add a user to a group 
---
--- RUN ON THE MINERVA DATABASE THAT IS USED
--------------------------------------------------------------------

Declare @User		varchar(150) = 'emmet.gibney',
		@UserID		int,
		@Group		varchar(255) = 'Developer',
		@GroupID	int
		

/* List of User
Select * from dbo.SysUser Where UserName like 'emmet%'
*/

/* List of Permission Groups
Select * from dbo.PermissionGroups 
*/


Select @UserID = id from dbo.SysUser Where Username = @User
Select @GroupID = id from dbo.PermissionGroups Where [Name] = @Group

If (@UserID is null) Or (@GroupID is null) 
	return

Insert dbo.PermissionGroupSysUsers ( PermissionGroup_Id, SysUser_Id )
		Values ( @GroupID, @UserID )




