﻿/*---------------------------------------------------------------------------------------------------------
PART 1 - RUN ON MINERVA (TEST)

Produces lines to be executed as part of PART 2 (no inter-database connectivity in Azure)
---------------------------------------------------------------------------------------------------------*/

If Exists(Select name from sys.procedures where name = 'tmp_DeleteUser')
  Drop Procedure dbo.tmp_DeleteUser
Go

Create Procedure dbo.tmp_DeleteUser
		@UserName		nvarchar(150)
As 
 Begin
	 Declare @MinervaID	int,
			@LegacyID   int

	Select @MinervaID = Id From [dbo].[SysUser] Where [Username] = @UserName
	If @MinervaID > 0
	  begin
			Delete dbo.PermissionGroupSysUsers Where SysUser_Id = @MinervaID
			Delete dbo.LegacyUserDatas Where MinervaUserId = @MinervaID
			Delete dbo.SysUserTokens Where SysUserId = @MinervaID
			Delete dbo.SysUserWidgets  Where SysUserId = @MinervaID
			Delete dbo.SysUser Where id = @MinervaID
	  end
 End
Go

If Exists(Select name from sys.procedures where name = 'tmp_AddUser')
  Drop Procedure dbo.tmp_AddUser
Go


Create Procedure dbo.tmp_AddUser
		@UserName		nvarchar(150),
		@ForeName		nvarchar(150),
		@SurName		nvarchar(150),
		@EMail			nvarchar(150),
		@Password		nvarchar(128),
		@Salt			nvarchar(50)

As 
 Begin
		
		If Exists(Select [Username] from dbo.SysUser Where [Username] = @UserName) 
				Return 0


		INSERT [dbo].[SysUser] (
				[Username], 
				[UserType], 
				[Forename], 
				[Surname], 
				[Email],
				[Password],
				[Salt],
				[IsAuthenticated], 
				[IsEnabled], 
				[IsDeleted], 
				[HasATemporaryPassword], 
				[FailedLoginCount], 
				[IsB2CAdmin], 
				[IsB2BAdmin], 
				[DateCreated], 
				[DateModified], 
				[LastModifiedUserId]
				)
		VALUES (	
				@UserName,		-- [Username]
				0,				-- [UserType]
				@ForeName,		-- [Forename]
				@SurName,		-- [Surname]
				@EMail,			-- [Email]
				@Password,		-- [Password]
				@Salt,			-- [Salt]
				1,				-- [IsAuthenticated]
				1,				-- [IsEnabled]
				0,				-- [IsDeleted]
				0,				-- [HasATemporaryPassword]
				0,				-- [FailedLoginCount]
				0,				-- [IsB2CAdmin]
				0,				-- [IsB2BAdmin]
				GetDate(),		-- [DateCreated]
				GetDate(),		-- [DateModified]
				1				-- [LastModifiedUserId]
				)

		Declare @MinervaID		int

		Select @MinervaID = Id From [dbo].[SysUser] Where [Username] = @UserName

		INSERT [dbo].[LegacyUserDatas] (
				[IsTourLeader], 
				[IsGuide], 
				[IsCoordinator], 
				[IsExternal], 
				[City_Id], 
				[Trainer], 
				[VaticanPass], 
				[Licensed], 
				[OutOfProvence], 
				[Comune], 
				[Spanish], 
				[English], 
				[French], 
				[Portugese], 
				[German], 
				[Italian], 
				[IsLoyalty], 
				[ReceiveGuideEmail], 
				[HiredBy], 
				[HireDate], 
				[DateCreated], 
				[DateModified], 
				[LastModifiedUserId], 
				[GuidaVaticana], 
				[GuidaNazionale], 
				[GuidaEstera], 
				[GuidePrefecture], 
				[GuideConferencierEcole], 
				[TourLeaderUnlicenced], 
				[TourLeaderLicenced], 
				[MinervaUserId] )
		VALUES (
				0,		-- [IsTourLeader]
				0,		-- [IsGuide] 
				0, 		-- [IsCoordinator]
				0, 		-- [IsExternal]
				1, 		-- [City_Id]  
				0, 		-- [Trainer]
				0, 		-- [VaticanPass]
				0, 		-- [Licensed]
				0, 		-- [OutOfProvence]
				0, 		-- [Comune]
				0, 		-- [Spanish]
				0, 		-- [English]
				0, 		-- [French]
				0, 		-- [Portugese]
				0, 		-- [German]
				0, 		-- [Italian]
				0, 		-- [IsLoyalty]
				0, 		-- [ReceiveGuideEmail]
				1, 		-- [HiredBy]
				Cast(GetDate() as Date),			-- [HireDate]
				GetDate(),	-- [DateCreated]
				GetDate(),	-- [DateModified]
				1, 		-- [LastModifiedUserId]
				0,		-- [GuidaVaticana]
				0,		-- [GuidaNazionale]
				0,		-- [GuidaEstera]
				0,		-- [GuidePrefecture]
				0,		-- [GuideConferencierEcole]
				0,		-- [TourLeaderUnlicenced]
				0,		-- [TourLeaderLicenced]
				@MinervaID	-- [MinervaUserId]
				)

	Return @MinervaID * (-1)		-- make it negative to distinct between return state and ID
End
Go

If Exists(Select name from sys.procedures where name = 'tmp_AddUserRole')
  Drop Procedure dbo.tmp_AddUserRole
Go


Create Procedure dbo.tmp_AddUserRole
	@MinervaID			int,
	@Role				nvarchar(255)
as 
Begin

	Declare @RoleID		int
	Select @RoleID = id from dbo.PermissionGroups Where Name = @Role

	If (@RoleID is null) 
		return -1

	INSERT [dbo].[PermissionGroupSysUsers]([PermissionGroup_Id], [SysUser_Id]) VALUES (@RoleID, @MinervaID)

End
Go

If Exists(Select name from sys.procedures where name = 'tmp_AddUserBatch')
  Drop Procedure dbo.tmp_AddUserBatch
Go


Create Procedure dbo.tmp_AddUserBatch
		@UserName		nvarchar(150),
		@ForeName		nvarchar(150),
		@SurName		nvarchar(150),
		@EMail			nvarchar(150),
		@Initials		nvarchar(5),
		@Password		nvarchar(128),
		@Salt			nvarchar(50)
As 
Begin

	Declare @MinervaID int = 0

	-- preserve password and salt if user exists
	Select @MinervaID = Id From [dbo].[SysUser] Where [Username] = @UserName
	If @MinervaID > 0
	  begin
			Declare @OldPassword	nvarchar(128),
					@OldSalt		nvarchar(50)

			Select	@OldPassword = [Password],
					@OldSalt	 = [Salt]
			From [dbo].[SysUser] Where [Username] = @UserName

			If (RTrim(@OldPassword) <> '')
			   Begin
					Set @Password = @OldPassword
					Set @Salt = @OldSalt
			   End
	  end
	
	-- delete user user if exists
	Exec dbo.tmp_DeleteUser @UserName

	-- create new user 
	Exec @MinervaID = dbo.tmp_AddUser @UserName, @ForeName,	@SurName, @EMail, @Password, @Salt

	If (@MinervaID >= 0) return @MinervaID

	Set @MinervaID = @MinervaID * (-1)

	Exec dbo.tmp_AddUserRole @MinervaID, 'View Supplier Users'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Manage Supplier Users'
	Exec dbo.tmp_AddUserRole @MinervaID, 'View Guide Users'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Manage Guide Users'
	Exec dbo.tmp_AddUserRole @MinervaID, 'View Users'
	Exec dbo.tmp_AddUserRole @MinervaID, 'View Roles and Permissions'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Default Permissions'
	Exec dbo.tmp_AddUserRole @MinervaID, 'B2B Manager'
	Exec dbo.tmp_AddUserRole @MinervaID, 'B2B Account Manager'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Ops Supervisor'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Ops B2B Data Entry'
	Exec dbo.tmp_AddUserRole @MinervaID, 'SCC Supervisor'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Purchasing'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Forecasting'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Guide Scheduling'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Inventory'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Product'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Guide Management'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Coordinator'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Coordination Management'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Manage Product Options'
	Exec dbo.tmp_AddUserRole @MinervaID, 'Product Management'
	
	-- to run within Part II (Add Legacy User
	Select 'Exec dbo.tmp_AddLegacyUser N''' + @UserName + ''', N''' + @ForeName + ''', N''' + @SurName + ''', N''' + @EMail + ''', N''' + @Initials + ''', ' + Convert(varchar(10), @MinervaID) + ', N''' + @Password + ''''

End
Go

Exec dbo.tmp_AddUserBatch N'Konrad.Balys', N'Konrad', N'Balys', N'kbalys@future-processing.com', N'FP_KB', N'VBKRdjIfkm42aWKmZxNitVxepmvj63u/hYoCsDL8Wkw=', N'Qqr0Rr9FQgDBma6V'
Exec dbo.tmp_AddUserBatch N'Mateusz.Łyżwiński', N'Mateusz', N'Łyżwiński', N'mlyzwinski@future-processing.com', N'FP_MT', N'0UcK8dLwwz6FBLYMLrQA9o6FNDU4gudq1s3ngnbtw0w=', N'GfuFVFIgpJiXPXrF'
Exec dbo.tmp_AddUserBatch N'Daniel.Jaros', N'Daniel', N'Jaros', N'djaros@future-processing.com', N'FP_DJ', N'sEg2icjaGQ4cP41UPGqpyjK5BtwpYcN8kUK/RKiNMCw=', N'zmc3yj2dIcZkH8Rr'
Exec dbo.tmp_AddUserBatch N'Bartłomiej.Glac', N'Bartłomiej', N'Glac', N'bglac@future-processing.com', N'FP_BG', N'8qa6yb4+jXiCYv3I6stmApgK+UX80PYAYPNIrU6q0mQ=', N'bxV+5XOilKSsS2My'
Exec dbo.tmp_AddUserBatch N'Daniel.Feist', N'Daniel', N'Feist', N'dfeist@future-processing.com', N'FP_DF', N'clPzbX9RuJ+wYXVld8zEuYc4aVd6tVCSdDQFb0y5wRs=',      N'uZzJZS7b3Gt7tQpq'

-- will be replaces
--Piotr.Opaczyński
--Daniel.Feist			- Daniel.Feist123
--Rafał.Ostrowski		- Ostrowski321!
--Piotr.Opaczyński		- Piotr789*!

Exec dbo.tmp_AddUserBatch N'Łukasz.Ciura', N'Łukasz', N'Ciura', N'lciura@future-processing.com'						, N'FP_PO', N'UAqlMv8B238lIZf+kH5tf+SF/j/Chsup5+FyqLsl3Cc=', N'cTiOijfUQepm5gDH'
Exec dbo.tmp_AddUserBatch N'Łukasz.Sośniak', N'Łukasz', N'Sośniak', N'lsosniak@future-processing.com'				, N'FP_PO', N'OUxM3Sb5fuVIOSW9UkviWd4cL0cuYQXwpTJkQ1Zqxpw=', N'TBv6hfkFJYI7J7E6'
Exec dbo.tmp_AddUserBatch N'Wojciech.Niemkowski', N'Wojciech', N'Niemkowski', N'wniemkowski@future-processing.com'	, N'FP_PO', N'GxhMRMsVNohA4mQdVp2E7AOCutYFFwGnP7XiJT7abMs=', N'hw8CNmUSJzGQ3OKC'


Exec dbo.tmp_AddUserBatch N'Karolina.Cieślar', N'Karolina', N'Cieślar', N'kcieslar@future-processing.com '			, N'FP_KC', N'UT27RWMWFW9vg0ezS4gVkVAz7tpv2PUvZqcIq+XGOoQ=', N'gDoWf7M7LBKICKWt'


--Łukasz Ciura			lciura@future-processing.com				PWD: Trinity321
--Łukasz Sośniak		lsosniak@future-processing.com				PWD: Trinity321
--Wojciech Niemkowski	wniemkowski@future-processing.com			PWD: Trinity321

-----------------------------------------------------------------------------------------------------
-- Other
-----------------------------------------------------------------------------------------------------


/* -- Delete User Account

	Exec dbo.tmp_DeleteUser 'Konrad.Balys'
	Exec dbo.tmp_DeleteUser 'Mateusz.Łyżwiński'
	Exec dbo.tmp_DeleteUser 'Daniel.Jaros'
	Exec dbo.tmp_DeleteUser 'Bartłomiej.Glac'
	Exec dbo.tmp_DeleteUser 'Daniel.Feist'
	Exec dbo.tmp_DeleteUser 'Rafał.Ostrowski'
	Exec dbo.tmp_DeleteUser 'Piotr.Opaczyński'

	Exec dbo.tmp_DeleteUser 'Joe.Doe4'

*/

/* TEST

	Select * from [SysUser] Where Username like '%Feist%'
	-- Minerva ID 
	Select * From [LegacyUserDatas] where MinervaUserId = 3720
	Select * from [PermissionGroupSysUsers] Where SysUser_Id = 3720
	Select * from citywonders_preprod.dbo.d_user where [user_name] = 'Daniel Feist'
	-- Legacy User ID
	Select * from citywonders_preprod.dbo.UsersBridge where [user_id] = 3765
	Select * from citywonders_preprod.dbo.d_user_modulo WHERE [user_id] = 3765

*/


-------------------------------------------------------------------------------
--- RUN THIS ON CITYWONDERS AND MINERVA TO ADD Konrad and Bartłomiej as DBO
-------------------------------------------------------------------------------

CREATE USER [kbalys.future-processing]
	FOR LOGIN [kbalys.future-processing]
	WITH DEFAULT_SCHEMA = dbo
GO

CREATE USER [bglac.future-processing]
	FOR LOGIN [bglac.future-processing]
	WITH DEFAULT_SCHEMA = dbo
GO


EXEC sp_addrolemember N'db_owner', N'bglac.future-processing'
GO

EXEC sp_addrolemember N'db_owner', N'kbalys.future-processing'
GO



Update dbo.sysuser Set IsEnabled = 1 Where username = 'lbo.test'
Update dbo.sysuser Set IsEnabled = 0 Where username = 'lbo.prod'



/* COPY THE RESULTS HERE TO COLLECT THEM BEFORE APENDING TO THE SECOND SCRIPT

Exec dbo.tmp_AddLegacyUser N'Karolina.Cieślar', N'Karolina', N'Cieślar', N'kcieslar@future-processing.com ', N'FP_KC', 4189, N'UT27RWMWFW9vg0ezS4gVkVAz7tpv2PUvZqcIq+XGOoQ='

*/