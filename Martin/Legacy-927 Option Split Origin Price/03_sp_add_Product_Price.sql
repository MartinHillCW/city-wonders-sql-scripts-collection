USE [citywonders_preprod]
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('[dbo].[sp_add_Product_Price]'))
   exec('CREATE PROCEDURE [dbo].[sp_add_Product_Price] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER Procedure [dbo].[sp_add_Product_Price]
/*	---------------------------------------------------------------------------------
Procedure			sp_add_Product_Price
Description		    Creates a new product price record with or without dependent table
			
Change		By				Change
2019-01-03	Martin Hill		Created

Adding - no dependent table  (button on the main list of options)
Copy/Split - dependent table update (button on option / splitting) 
------------------------------------------------------------------------------------*/
	@original_price_id			as int = null,				-- orignal price ID 
																
	@product_id					as int,						-- needed? Yes
	@currency_id				as int, 
	@group_id					as int,			
	@price_desc					as nvarchar(255), 
	@price_validoDa				as smalldatetime, 
	@price_validoA				as smalldatetime,	
	@price_adultEtaDa			as int, 
	@price_adultEtaA			as int, 	
	@price_childEtaDa			as int, 
	@price_childEtaA			as int, 	
	@price_seniorEtaDa			as int, 
	@price_seniorEtaA			as int, 	
	@price_infantEtaDa			as int, 
	@price_infantEtaA			as int, 	
	@price_freeSaleDays			as int, 
	@price_onRequestHours		as int, 
	@price_voucherValidity		as int, 
	@price_flagMostraHomePage	as bit, 
	@price_ordine				as int, 
	@price_flagPrivate			as bit, 
	@price_meetingTime			as nvarchar(8), 
	@price_startTime			as nvarchar(8), 
	@price_duration				as nvarchar(50), 
	@price_endTime				as nvarchar(8), 
	@price_meetingPoint			as ntext, 
	@price_conclusionPoint		as ntext, 
	@price_addInfoVoucher		as ntext, 
	@price_groupSize			as int, 
	@price_unavailable_type		as nvarchar(50), 
	@price_maxPax				as int, 
	@price_flagOnline			as bit, 
	@price_advancedSaleHours	as int, 
	@price_meetingPoint_sms		as nvarchar(255), 
	@price_maxTicketsTour		as int, 	
	@price_groupSizeExtraRole	as int, 
	@AdditionalInfoNewVoucherTemplate as nvarchar(max), 
	@RRP						as decimal(16,2), 
	@RRPADULT					as decimal(16,2), 
	@RRPCHILD					as decimal(16,2), 
	@RRPINFANT					as decimal(16,2), 
	@price_IncludeInfantsInPax	as bit,

	@FlagB2BDate				bit = 0,
	@FlagB2CDate				bit = 0,
	@FlagIncompatabilities		bit = 0,

	@AuditUser					int

As
Begin

	SET NOCOUNT ON;
	If (0=1) SET FMTONLY OFF;  -- enables meta data querying from code - Entity Framework temp table issues

	Declare		@DateOfChange	datetime = GetDate(),
				@Result			int = 0,
				@NewPriceID		int,
				@ChangeSetID	uniqueidentifier = NEWID()
	
	BEGIN TRY	
			Declare @Message nvarchar(2000), @SysDateTime datetime2,@ERRORMESSAGE VARCHAR(1000)	
			SET @SysDateTime = SYSDATETIME()
			-----------------------------------------------------------------------------------
			---------- ADD RECORD FIRST
			-----------------------------------------------------------------------------------			
			Insert dbo.d_product_price ( 
							product_id, 
							currency_id, 
							group_id, 
							price_desc, 
							price_validoDa, 
							price_validoA,							
							price_adultEtaDa, 
							price_adultEtaA, 							
							price_childEtaDa, 
							price_childEtaA, 							
							price_seniorEtaDa, 
							price_seniorEtaA, 							
							price_infantEtaDa, 
							price_infantEtaA, 
							price_freeSaleDays, 
							price_onRequestHours, 
							price_voucherValidity, 
							price_flagMostraHomePage, 
							price_ordine, 
							price_flagPrivate, 
							price_meetingTime, 
							price_startTime, 
							price_duration, 
							price_endTime, 
							price_meetingPoint, 
							price_conclusionPoint, 
							price_addInfoVoucher, 
							price_groupSize, 
							price_unavailable_type, 
							price_maxPax, 
							price_flagOnline, 
							price_advancedSaleHours, 
							price_meetingPoint_sms, 
							price_maxTicketsTour, 
							price_groupSizeExtraRole, 
							AdditionalInfoNewVoucherTemplate, 
							RRP, 
							RRPADULT, 
							RRPCHILD, 
							RRPINFANT, 
							price_IncludeInfantsInPax	)
			Values (
							@product_id, 
							@currency_id, 
							@group_id, 
							@price_desc, 
							@price_validoDa, 
							@price_validoA,							
							@price_adultEtaDa, 
							@price_adultEtaA, 							
							@price_childEtaDa, 
							@price_childEtaA, 							
							@price_seniorEtaDa, 
							@price_seniorEtaA, 							
							@price_infantEtaDa, 
							@price_infantEtaA, 
							@price_freeSaleDays, 
							@price_onRequestHours, 
							@price_voucherValidity, 
							@price_flagMostraHomePage, 
							@price_ordine, 
							@price_flagPrivate, 
							@price_meetingTime, 
							@price_startTime, 
							@price_duration, 
							@price_endTime, 
							@price_meetingPoint, 
							@price_conclusionPoint, 
							@price_addInfoVoucher, 
							@price_groupSize, 
							@price_unavailable_type, 
							@price_maxPax, 
							@price_flagOnline, 
							@price_advancedSaleHours, 
							@price_meetingPoint_sms, 
							@price_maxTicketsTour, 
							@price_groupSizeExtraRole, 
							@AdditionalInfoNewVoucherTemplate, 
							@RRP, 
							@RRPADULT, 
							@RRPCHILD, 
							@RRPINFANT, 
							@price_IncludeInfantsInPax
					)

			Set @NewPriceID = SCOPE_IDENTITY()
			Set @Message = 'Added New Option : '  + Convert(varchar(10), @NewPriceID)
			Exec appsys.AddMessage_OptionSplitting @ChangeSetID, @NewPriceID, @Message, 'd_product_price' ,null,null, @SysDateTime
			-----------------------------------------------------------------------------------
			---------- AUDIT START
			-----------------------------------------------------------------------------------			
			Insert audit.d_product_price 
				Select	*, 
						'ADD',
						@DateOfChange,
						@AuditUser
				From dbo.d_product_price pp
				Where pp.price_ID = @NewPriceID
			Set @Message = 'Audit record Added for : '  + Convert(varchar(10), @NewPriceID)
			Exec appsys.AddMessage_OptionSplitting @ChangeSetID, @NewPriceID, @Message, 'd_product_price' ,null,null, @SysDateTime
			-----------------------------------------------------------------------------------
			---------- AUDIT END
			-----------------------------------------------------------------------------------
									
			IF (@original_price_id IS NOT NULL) -- COPYING AN EXISTING OPTION
			BEGIN																	
				-----------------------------------------------------------------------------------
				---------- UPDATE DEPENDENT DATA START 
				---------- FIRST STEP IN ORDER TO FIND MATCHING RECORDS
				-----------------------------------------------------------------------------------
				BEGIN TRY					
					EXEC sp_add_Product_Price_sub_Origin_Price
							@NewPriceID, 
							@price_validoDa, 
							@price_validoA,									
							@price_adultEtaDa, 
							@price_adultEtaA, 									
							@price_childEtaDa, 
							@price_childEtaA, 									
							@price_seniorEtaDa, 
							@price_seniorEtaA, 									
							@price_infantEtaDa, 
							@price_infantEtaA, 
							@price_freeSaleDays, 
							@price_onRequestHours, 
							@price_flagPrivate, 
							@FlagB2BDate,
							@FlagB2CDate,									
							@AuditUser,
							@DateOfChange,
							@original_price_id,
							@ChangeSetID,
							@SysDateTime
																	
				END TRY
				BEGIN CATCH																														
					SELECT	@ERRORMESSAGE = ERROR_MESSAGE()
					EXEC	appsys.AddMessage_OptionSplitting @ChangeSetID, @NewPriceID, @ERRORMESSAGE, 'd_provenienza_price' ,null,'Error', @SysDateTime	
				END CATCH

				If (@FlagIncompatabilities = 1)
				Begin
					Exec @Result = sp_add_Product_Price_sub_Incompatibility
										@NewPriceID, 
										@AuditUser,
										@DateOfChange,
										@original_price_id,
										@ChangeSetID,
										@SysDateTime
				End

				If @Result = 1 
					Begin						
						Exec appsys.AddMessage_OptionSplitting @ChangeSetID, @NewPriceID, 'Error adding Incompatibility Data', 'd_product_price_incompatible' ,null,'Error', @SysDateTime	
						SET @Result = 0
					End			
				-----------------------------------------------------------------------------------
				---------- UPDATE DEPENDENT DATA END
				-----------------------------------------------------------------------------------
			END
								
			-----------------------------------------------------------------------------------
			---------- REAL TIME AVAILABILITY START
			-----------------------------------------------------------------------------------
			/*
			Declare @KEY	varchar(255) = NewID()
			Insert dbo.TourInstance_UpdateKeys ( CallID, KeyID )
			Select @Key, @price_id
			Exec dbo.sp_GetBookingAvailabilityUpdate @Key, Null, Null
			*/	
			-----------------------------------------------------------------------------------
			---------- REAL TIME AVAILABILITY END
			-----------------------------------------------------------------------------------
			
	END TRY
	BEGIN CATCH					
		SELECT @ERRORMESSAGE = ERROR_MESSAGE()
		Exec appsys.AddMessage_OptionSplitting @ChangeSetID, @NewPriceID, @ERRORMESSAGE, 'd_product_price' ,null,'Error', @SysDateTime	
		Return 1
	END CATCH
		
	Select @NewPriceID PriceID

	Return 0

End


