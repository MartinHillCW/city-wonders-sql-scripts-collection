use citywonders_preprod 
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('[dbo].[sp_upd_Product_Price_sub_Origin_Price]'))
   exec('CREATE PROCEDURE [dbo].[sp_upd_Product_Price_sub_Origin_Price] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER Procedure [dbo].[sp_upd_Product_Price_sub_Origin_Price]
/*	---------------------------------------------------------------------------------
Procedure			sp_upd_Product_Price_sub_Origin_Price
Description			Updates the d_product_price table and dependent tables if 
					necessary.

Change		By				Change
2018-12-10	Martin Hill		Created
------------------------------------------------------------------------------------*/
	@price_id					as int, 
	@price_validoDa				as smalldatetime, 
	@price_validoA				as smalldatetime,	
	@price_adultEtaDa			as int, 
	@price_adultEtaA			as int, 
	@price_childEtaDa			as int, 
	@price_childEtaA			as int, 
	@price_seniorEtaDa			as int, 
	@price_seniorEtaA			as int, 	
	@price_infantEtaDa			as int, 
	@price_infantEtaA			as int, 	
	@price_freeSaleDays			as int, 
	@price_onRequestHours		as int, 
	@price_flagPrivate			as bit, 
	@FlagB2BDate				bit = 0,
	@FlagB2CDate				bit = 0,	
	@AuditUser					int,
	@DateOfChange				datetime,
	@ChangeSetID				uniqueidentifier,
	@SysDateTime				datetime2

As
Begin

	SET NOCOUNT ON;
	If (0=1) SET FMTONLY OFF;  -- enables meta data querying from code - Entity Framework temp table issues

	-- BEGIN TRANSACTION HANDLED BY PARENT PROCEDURE
	BEGIN TRY

		Declare @Message nvarchar(2000)
		Set @Message = 'Updating Origin Pricing : ' + Convert(varchar(10), @price_id)
		Exec appsys.AddMessage_OptionSplitting @ChangeSetID, @price_id, @Message, 'd_provenienza_price' ,null,null,@SysDateTime		

		IF ((@FlagB2BDate = 1) Or (@FlagB2CDate = 1))			-- "origin price" DATE
			begin				
				-- get all IDs to be changed in target table (this way instead of direct change because the audit part is easier)
				-- find all records that exists with the current price_id
				
				-- Identify the Origin Prices to update			
				Insert appsys.Messages_OptionSplitting ( SourceID, OptionId, [Message], TablesName, RelatedEntityID ,[Action], [TimeStamp])
				Select	@ChangeSetID,@price_id, NULL, 'd_provenienza_price', pprice.provenienza_price_id, 'Identified', @SysDateTime
				From	d_provenienza_price pprice
				Inner Join dbo.d_provenienza p on p.provenienza_id = pprice.provenienza_id
				Where	( 
								((@FlagB2BDate = 1) And  (p.provenienza_gruppo_id = 2)) Or		-- B2B
								((@FlagB2CDate = 1) And  (p.provenienza_gruppo_id In (1,3)) ) 	-- B2C
						) And
						pprice.price_id = @price_id 
						--and pprice.provenienza_price_validoA >= GetDate()						-- select only current or future ***** Adding all options to message table.
				
				-- Now we identify records that we will update. 
				Select pprice.provenienza_price_id 
				Into #ID01
				From dbo.d_provenienza_price pprice
							Inner Join dbo.d_provenienza p on p.provenienza_id = pprice.provenienza_id
				Where	( 
								((@FlagB2BDate = 1) And  (p.provenienza_gruppo_id = 2)) Or		-- B2B
								((@FlagB2CDate = 1) And  (p.provenienza_gruppo_id In (1,3)) ) 	-- B2C
						) And
						pprice.price_id = @price_id And 
						pprice.provenienza_price_validoA >= GetDate()	And			-- select only current or future
						( Select Count(*) from dbo.d_provenienza_price pcount Where pcount.price_id = pprice.price_id And
																					pcount.provenienza_id = pprice.provenienza_id And				
																					pcount.provenienza_price_validoA >= GetDate()) = 1

				IF NOT EXISTS (SELECT * FROM #ID01 WHERE provenienza_price_id > 0) BEGIN
					SET @message = 'No d_provenienza_price records to update '
					Exec appsys.AddMessage_OptionSplitting @ChangeSetID, @price_id, @Message, 'd_provenienza_price' ,null,'NONE',@SysDateTime		
				END


				
				-- Mark the records as updated. We will check later to see if any were skipped
				UPDATE	m
				SET		m.[Action] = 'Updated'
				FROM	#ID01 id1
				inner join appsys.Messages_OptionSplitting m on m.RelatedEntityID = id1.provenienza_price_id  and m.SourceID = @ChangeSetID
				

				-- Insert INIT audit records before changes are made if necessary
				Insert audit.d_provenienza_price
					Select T.*, 'INIT', DateAdd(minute, -1, @DateOfChange), @AuditUser
						From dbo.d_provenienza_price T
								Inner Join #ID01 ID on ID.provenienza_price_id = T.provenienza_price_id
								Left Join audit.d_provenienza_price pp on	pp.price_id = T.price_id and
																			pp.provenienza_id = T.provenienza_id 
						Where pp.provenienza_price_id is null -- NO AUDIT RECORD

				Update pprice Set
						provenienza_price_validoDa			= @price_validoDa,
						provenienza_price_validoA			= @price_validoA,							
						provenienza_price_adultEtaDa		= @price_adultEtaDa,
						provenienza_price_adultEtaA			= @price_adultEtaA,
						provenienza_price_childEtaDa		= @price_childEtaDa,
						provenienza_price_childEtaA			= @price_childEtaA,
						provenienza_price_infantEtaDa		= @price_infantEtaDa,
						provenienza_price_infantEtaA		= @price_infantEtaA,
						provenienza_price_freeSaleDays		= @price_freeSaleDays,
						provenienza_price_onRequestHours	= @price_onRequestHours,
						provenienza_price_flagPrivate		= @price_flagPrivate
				From dbo.d_provenienza_price pprice
						Inner Join #ID01 ID on ID.provenienza_price_id = pprice.provenienza_price_id

				Insert audit.d_provenienza_price
					Select T.*, 'UPDATE', @DateOfChange, @AuditUser
					From dbo.d_provenienza_price T
						Inner Join #ID01 ID on ID.provenienza_price_id = T.provenienza_price_id
				
				Drop Table #ID01
			end

	END TRY
	BEGIN CATCH		
		DECLARE @ERRORMESSAGE VARCHAR(1000)		
		SELECT @ERRORMESSAGE = ERROR_MESSAGE()
		Exec appsys.AddMessage_OptionSplitting @ChangeSetID, @price_id, @ERRORMESSAGE, 'd_provenienza_price' ,null,'Error', @SysDateTime		
		RAISERROR(@ERRORMESSAGE, 15, 1)
	End Catch
	
	Return 0

End
