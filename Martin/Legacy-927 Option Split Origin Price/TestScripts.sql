
-----------------------


-- Select price_id, provenienza_id, Count(*) from d_provenienza_price  Group By price_id, provenienza_id Having Count(*) > 2

-- price ID 1480
-- provenienza ID 107

--Select * From d_provenienza_price Where price_id = 1480
--Select * from d_product_price where price_id = 1480
--Select Distinct provenienza_id From d_provenienza_price Where price_id = 1480

Declare @NewPriceID int

Create Table #NewPrice ( price_id int)

Insert #NewPrice
Exec [sp_add_Product_Price]

	@original_price_id				= 1480,						-- orignal price ID 
	
	@product_id						= 327,						-- needed?
	@currency_id					= 1,
	@group_id						= 339,
	@price_desc						= 'Group - Italian- 09:45 AM',
	@price_validoDa					= '20190101', 
	@price_validoA					= '20190110', 
	@price_base						= 62, 
	@price_baseNetto				= 62,
	@price_numeroPersoneBase		= 1,
	@price_adult					= 62,
	@price_adultNetto				= 62,
	@price_adultEtaDa				= 19, 
	@price_adultEtaA				= 99, 
	@price_child					= 57, 
	@price_childNetto				= 57, 
	@price_childEtaDa				= 6, 
	@price_childEtaA				= 18, 
	@price_senior					= null, 
	@price_seniorNetto				= null, 
	@price_seniorEtaDa				= null, 
	@price_seniorEtaA				= null, 
	@price_infant					= 0, 
	@price_infantNetto				= 0, 
	@price_infantEtaDa				= 0, 
	@price_infantEtaA				= 5, 
	
	@price_freeSaleDays				= null, 
	@price_onRequestHours			= null, 
	@price_voucherValidity			= null, 
	@price_flagMostraHomePage		= 0,
	@price_ordine					= null, 
	@price_flagPrivate				= 0,
	@price_meetingTime				= '09:30 AM', 
	@price_startTime				= '09:45 AM', 
	@price_duration					= '3 Ore', 
	@price_endTime					= '12:45 PM', 
	@price_meetingPoint				= 'a meeting point', 
	@price_conclusionPoint			= 'a conclusion point', 
	@price_addInfoVoucher			= '', 
	@price_groupSize				= 21, 
	@price_unavailable_type			= 'am', 
	@price_maxPax					= null, 
	@price_flagOnline				= null,
	@price_advancedSaleHours		= null, 
	@price_meetingPoint_sms			= null, 
	@price_maxTicketsTour			= null, 
	--@HRI_id = , 
	--@vettore_service_id = , 
	@price_groupSizeExtraRole		= null, 
	@AdditionalInfoNewVoucherTemplate = null, 
	@RRP							= 62, 
	@RRPADULT						= 62, 
	@RRPCHILD						= 57, 
	@RRPINFANT						= 0, 
	@price_IncludeInfantsInPax		= 0,

	@FlagB2BDate					= 1,
	@FlagB2CDate					= 1,
	@FlagB2CPrice					= 1,

	@AuditUser						= 3034

Select Top 1 @NewPriceID = price_id from #NewPrice
Drop Table #NewPrice


Select * from d_provenienza_price Where price_id = @NewPriceID
Select * from d_product_price where price_id = @NewPriceID

Select * from audit.d_provenienza_price  Where price_id = @NewPriceID
Select * from audit.d_product_price where price_id = @NewPriceID


/*

Delete d_provenienza_price Where price_id = 3715
Delete d_product_price where price_id = 3715

Delete audit.d_provenienza_price Where price_id = 3715
Delete audit.d_product_price where price_id = 3715

*/