USE [citywonders_preprod]
GO
/****** Object:  StoredProcedure [dbo].[sp_add_Product_Price_sub_Origin_Price]    Script Date: 04/02/2019 15:01:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('[dbo].[sp_add_Product_Price_sub_Incompatibility]'))
   exec('CREATE PROCEDURE [dbo].[sp_add_Product_Price_sub_Incompatibility] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER Procedure [dbo].[sp_add_Product_Price_sub_Incompatibility]
/*	---------------------------------------------------------------------------------
Procedure			sp_upd_Product_Price_sub_Incompatibility
Description			Inserts copies of incompatibility from original price ID to price ID

Change		By				Change
2019-02-06	Martin Hill		Created
------------------------------------------------------------------------------------*/
	@price_id					as int,					-- the new price ID
	@AuditUser					int,
	@DateOfChange				datetime,
	@Original_PriceID			int,						-- original price ID
	@ChangeSetID				uniqueidentifier,
	@SysDateTime				datetime2
As
Begin

	SET NOCOUNT ON;
	If (0=1) SET FMTONLY OFF;  -- enables meta data querying from code - Entity Framework temp table issues


	-- BEGIN TRANSACTION HANDLED BY PARENT PROCEDURE

	BEGIN TRY
			Declare @Message nvarchar(2000)
			SET	@Message = 'Adding new incompatibilities'
			Exec appsys.AddMessage_OptionSplitting @ChangeSetID, @price_id, @Original_PriceID, 'd_product_price_incompatible' ,@Original_PriceID,null, @SysDateTime

			Insert dbo.d_product_price_incompatible (
					price_id, 
					price_id_incompatible, 
					DateCreated
					)
			Select
					@price_id, 
					price_id_incompatible,
					GetDate()
			From dbo.d_product_price_incompatible p
			Where p.price_id = @Original_PriceID

			Insert dbo.d_product_price_incompatible (
					price_id, 
					price_id_incompatible, 
					DateCreated
					)
			Select	price_id,
					@price_id, 
					GetDate()
			From dbo.d_product_price_incompatible p
			Where p.price_id_incompatible = @Original_PriceID



			Insert audit.d_product_price_incompatible
				Select T.*, 'ADD', @DateOfChange, @AuditUser
				From dbo.d_product_price_incompatible T
				Where T.price_id = @price_id
				UNION ALL
				Select T.*, 'ADD', @DateOfChange, @AuditUser
				From dbo.d_product_price_incompatible T
				Where T.price_id_incompatible = @price_id
				
			

	END TRY
	BEGIN CATCH
		Rollback Transaction
		Return 1
	End Catch
	
	Return 0

End

/*
Exec [dbo].[sp_add_Product_Price_sub_Incompatibility]
	@price_id					= 1480,					-- the new price ID
	@AuditUser					= 3034,
	@DateOfChange				= '20190206',
	@Original_PriceID			= 3716

Select * from audit.d_product_price_incompatible	
Select * from d_product_price_incompatible	 where price_id = 1480
Select * from d_product_price_incompatible	 where price_id_incompatible = 1480
*/
