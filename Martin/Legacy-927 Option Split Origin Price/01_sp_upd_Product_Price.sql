
use citywonders_preprod 
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('[dbo].[sp_upd_Product_Price]'))
   exec('CREATE PROCEDURE [dbo].[sp_upd_Product_Price] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER Procedure [dbo].[sp_upd_Product_Price]
/*	---------------------------------------------------------------------------------
Procedure			sp_upd_Product_Price
Description			Updates the d_product_price table and dependent tables if 
					necessary.

Change		By				Change
2018-12-10	Martin Hill		Created

Adding - no dependent table  (button on the main list of options)
Price ID is null / Origin Price ID is not Null (source)

Update - dependent table update (button on option)
Price ID is not null / Origin Price ID is null

Copy/Split - dependent table update (button on option / splitting) 
Price ID is null / Origin Price ID it not Null
------------------------------------------------------------------------------------*/
	@price_id					as int, 
	@product_id					as int,						-- needed?
	@currency_id				as int		= NULL, 
	@group_id					as int		= NULL,			
	@price_desc					as nvarchar(255), 
	@price_validoDa				as smalldatetime, 
	@price_validoA				as smalldatetime,	
	@price_adultEtaDa			as int, 
	@price_adultEtaA			as int, 	
	@price_childEtaDa			as int, 
	@price_childEtaA			as int, 	
	@price_seniorEtaDa			as int, 
	@price_seniorEtaA			as int, 	
	@price_infantEtaDa			as int, 
	@price_infantEtaA			as int, 	
	@price_freeSaleDays			as int, 
	@price_onRequestHours		as int, 
	@price_voucherValidity		as int, 
	@price_flagMostraHomePage	as bit, 
	@price_ordine				as int, 
	@price_flagPrivate			as bit, 
	@price_meetingTime			as nvarchar(8), 
	@price_startTime			as nvarchar(8), 
	@price_duration				as nvarchar(50), 
	@price_endTime				as nvarchar(8), 
	@price_meetingPoint			as ntext, 
	@price_conclusionPoint		as ntext, 
	@price_addInfoVoucher		as ntext, 
	@price_groupSize			as int, 
	@price_unavailable_type		as nvarchar(50), 
	@price_maxPax				as int, 
	@price_flagOnline			as bit, 
	@price_advancedSaleHours	as int, 
	@price_meetingPoint_sms		as nvarchar(255), 
	@price_maxTicketsTour		as int, 
	@price_groupSizeExtraRole	as int, 
	@AdditionalInfoNewVoucherTemplate as nvarchar(max), 
	@RRP						as decimal(16,2), 
	@RRPADULT					as decimal(16,2), 
	@RRPCHILD					as decimal(16,2), 
	@RRPINFANT					as decimal(16,2), 
	@price_IncludeInfantsInPax	as bit,

	@FlagB2BDate				bit = 0,
	@FlagB2CDate				bit = 0,	

	@AuditUser					int

As
Begin

	SET NOCOUNT ON;
	If (0=1) SET FMTONLY OFF;  -- enables meta data querying from code - Entity Framework temp table issues

	Declare		@DateOfChange	datetime = GetDate(),
				@Result			int
	
	BEGIN TRY
			
		Declare @ChangeSetID	uniqueidentifier = NEWID()	
		Declare @Message nvarchar(2000), 
				@SysDateTime datetime2

		Set @Message = 'Editing Option : ' + Convert(varchar(10), @price_id)
		SET @SysDateTime = SYSDATETIME()

		Exec appsys.AddMessage_OptionSplitting @ChangeSetID, @price_id, @Message, 'd_product_price' ,null,null, @SysDateTime

		-----------------------------------------------------------------------------------
		---------- UPDATE DEPENDENT DATA START 
		---------- FIRST STEP IN ORDER TO FIND MATCHING RECORDS
		-----------------------------------------------------------------------------------
		Exec @Result = sp_upd_Product_Price_sub_Origin_Price
						@price_id, 
						@price_validoDa, 
						@price_validoA,
						@price_adultEtaDa, 
						@price_adultEtaA, 										
						@price_childEtaDa, 
						@price_childEtaA, 										
						@price_seniorEtaDa, 
						@price_seniorEtaA, 										
						@price_infantEtaDa, 
						@price_infantEtaA, 
						@price_freeSaleDays, 
						@price_onRequestHours, 
						@price_flagPrivate, 
						@FlagB2BDate,
						@FlagB2CDate,										
						@AuditUser,
						@DateOfChange,
						@ChangeSetID,
						@SysDateTime


			-----------------------------------------------------------------------------------
			---------- UPDATE DEPENDENT DATA END
			-----------------------------------------------------------------------------------
			Insert audit.d_product_price 
				Select	pp.*, 
						'INIT',
						@DateOfChange,
						@AuditUser
				From dbo.d_product_price pp
							Left Join audit.d_product_price a on pp.price_id = a.price_id
				Where pp.price_ID = @price_id And
					  a.price_id is null
						

			-- UPDATE THE RECORD
			Update pp
				Set 
					product_id					= @product_id, 
					currency_id					= @currency_id, 
					group_id					= @group_id, 
					price_desc					= @price_desc, 
					price_validoDa				= @price_validoDa, 
					price_validoA				= @price_validoA,					
					price_adultEtaDa			= @price_adultEtaDa, 
					price_adultEtaA				= @price_adultEtaA, 					 
					price_childEtaDa			= @price_childEtaDa, 
					price_childEtaA				= @price_childEtaA, 					
					price_seniorEtaDa			= @price_seniorEtaDa, 
					price_seniorEtaA			= @price_seniorEtaA, 					
					price_infantEtaDa			= @price_infantEtaDa, 
					price_infantEtaA			= @price_infantEtaA, 
					price_freeSaleDays			= @price_freeSaleDays, 
					price_onRequestHours		= @price_onRequestHours, 
					price_voucherValidity		= @price_voucherValidity, 
					price_flagMostraHomePage	= @price_flagMostraHomePage, 
					price_ordine				= @price_ordine, 
					price_flagPrivate			= @price_flagPrivate, 
					price_meetingTime			= @price_meetingTime, 
					price_startTime				= @price_startTime, 
					price_duration				= @price_duration, 
					price_endTime				= @price_endTime, 
					price_meetingPoint			= @price_meetingPoint, 
					price_conclusionPoint		= @price_conclusionPoint, 
					price_addInfoVoucher		= @price_addInfoVoucher, 
					price_groupSize				= @price_groupSize, 
					price_unavailable_type		= @price_unavailable_type, 
					price_maxPax				= @price_maxPax, 
					price_flagOnline			= @price_flagOnline, 
					price_advancedSaleHours		= @price_advancedSaleHours, 
					price_meetingPoint_sms		= @price_meetingPoint_sms, 
					price_maxTicketsTour		= @price_maxTicketsTour, 
					price_groupSizeExtraRole	= @price_groupSizeExtraRole, 
					AdditionalInfoNewVoucherTemplate = @AdditionalInfoNewVoucherTemplate, 
					RRP							= @RRP, 
					RRPADULT					= @RRPADULT, 
					RRPCHILD					= @RRPCHILD, 
					RRPINFANT					= @RRPINFANT, 
					price_IncludeInfantsInPax	= @price_IncludeInfantsInPax
			From dbo.d_product_price pp
			Where pp.price_ID = @price_id

			-----------------------------------------------------------------------------------
			---------- AUDIT START
			-----------------------------------------------------------------------------------
			Insert audit.d_product_price 
				Select	*, 
						'UPDATE',
						@DateOfChange,
						@AuditUser
				From dbo.d_product_price pp
				Where pp.price_ID = @price_id
			-----------------------------------------------------------------------------------
			---------- AUDIT END
			-----------------------------------------------------------------------------------

			-----------------------------------------------------------------------------------
			---------- REAL TIME AVAILABILITY START
			-----------------------------------------------------------------------------------
			/*
			Declare @KEY	varchar(255) = NewID()
			Insert dbo.TourInstance_UpdateKeys ( CallID, KeyID )
			Select @Key, @price_id
			Exec dbo.sp_GetBookingAvailabilityUpdate @Key, Null, Null
			*/	
			-----------------------------------------------------------------------------------
			---------- REAL TIME AVAILABILITY END
			-----------------------------------------------------------------------------------

			
	END TRY
	BEGIN CATCH		
		DECLARE @ERRORMESSAGE VARCHAR(1000)		
		SELECT @ERRORMESSAGE = ERROR_MESSAGE()
		Exec appsys.AddMessage_OptionSplitting @ChangeSetID, @price_id, @ERRORMESSAGE, 'd_product_price' ,null,'Error', @SysDateTime
		RAISERROR(@ERRORMESSAGE, 15, 1)
	END CATCH
	
	--COMMIT TRANSACTION
	Return 0

	/*	-- helper / sample to create audit table

		Drop Table audit.d_product_price
		Select	CAST([price_id] as INT) price_id,	-- so not to make it IDENTITY
				[product_id], [currency_id], [group_id], [price_desc], [price_validoDa], [price_validoA], [price_base], [price_baseNetto], [price_numeroPersoneBase], [price_adult], [price_adultNetto], [price_adultEtaDa], [price_adultEtaA], [price_child], [price_childNetto], [price_childEtaDa], [price_childEtaA], [price_senior], [price_seniorNetto], [price_seniorEtaDa], [price_seniorEtaA], [price_infant], [price_infantNetto], [price_infantEtaDa], [price_infantEtaA], [price_freeSaleDays], [price_onRequestHours], [price_voucherValidity], [price_flagMostraHomePage], [price_ordine], [price_flagPrivate], [price_meetingTime], [price_startTime], [price_duration], [price_endTime], [price_meetingPoint], [price_conclusionPoint], [price_addInfoVoucher], [price_groupSize], [price_unavailable_type], [price_maxPax], [price_flagOnline], [price_advancedSaleHours], [price_meetingPoint_sms], [price_maxTicketsTour], [HRI_id], [vettore_service_id], [price_groupSizeExtraRole], [AdditionalInfoNewVoucherTemplate], [RRP], [RRPADULT], [RRPCHILD], [RRPINFANT], [price_IncludeInfantsInPax],

				CAST('' as varchar(10))		AuditAction,	-- UPDATE, NEW, DELETE, INIT
				CAST(GETDATE() as datetime)	AuditDate,
				CAST(0 as int)				AuditUser		-- d_user USER_ID
		Into audit.d_product_price From [dbo].[d_product_price] Where 1 = 0

		Drop Table audit.d_provenienza_price
		Select	CAST([provenienza_price_id] as int)  provenienza_price_id, 
				[provenienza_id], [price_id], [provenienza_price_validoDa], [provenienza_price_validoA], [provenienza_price_base], [provenienza_price_numeroPersoneBase], [provenienza_price_adult], [provenienza_price_adultEtaDa], [provenienza_price_adultEtaA], [provenienza_price_child], [provenienza_price_childEtaDa], [provenienza_price_childEtaA], [provenienza_price_senior], [provenienza_price_seniorEtaDa], [provenienza_price_seniorEtaA], [provenienza_price_infant], [provenienza_price_infantEtaDa], [provenienza_price_infantEtaA], [provenienza_price_freeSaleDays], [provenienza_price_onRequestHours], [provenienza_price_flagPrivate], [provenienza_price_note], [AdultConversionRate], [ChildConversionRate], [DateCreated], [CreatedBy], [DateAltered], [AlteredBy],
				CAST('' as varchar(10))		AuditAction,	-- UPDATE, NEW, DELETE
				CAST(GETDATE() as datetime)	AuditDate,
				CAST(0 as int)				AuditUser		-- d_user USER_ID
		Into audit.d_provenienza_price From [dbo].[d_provenienza_price] Where 1 = 0

		Drop Table audit.d_product_price_incompatible
		Select	CAST(product_price_incompatible_id as int)  product_price_incompatible_id, 
				[price_id], [price_id_incompatible], [DateCreated],			
				CAST('' as varchar(10))		AuditAction,	-- UPDATE, NEW, DELETE
				CAST(GETDATE() as datetime)	AuditDate,
				CAST(0 as int)				AuditUser		-- d_user USER_ID
		Into audit.d_product_price_incompatible from dbo.d_product_price_incompatible Where 1 = 0


	*/

End
