USE [citywonders_preprod]
GO

/****** Object:  StoredProcedure [dbo].[sp_add_Product_Price_sub_Origin_Price]    Script Date: 04/02/2019 15:01:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('[dbo].[sp_add_Product_Price_sub_Origin_Price]'))
   exec('CREATE PROCEDURE [dbo].[sp_add_Product_Price_sub_Origin_Price] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER Procedure [dbo].[sp_add_Product_Price_sub_Origin_Price]
/*	---------------------------------------------------------------------------------
Procedure			sp_upd_Product_Price_sub_Origin_Price
Description			Updates the d_product_price table and dependent tables if 
					necessary.

Change		By				Change
2018-12-10	Martin Hill		Created
------------------------------------------------------------------------------------*/
	@price_id					as int,					-- the new price ID
	@price_validoDa				as smalldatetime, 
	@price_validoA				as smalldatetime,
	@price_adultEtaDa			as int, 
	@price_adultEtaA			as int, 	
	@price_childEtaDa			as int, 
	@price_childEtaA			as int, 	
	@price_seniorEtaDa			as int, 
	@price_seniorEtaA			as int, 	
	@price_infantEtaDa			as int, 
	@price_infantEtaA			as int, 	
	@price_freeSaleDays			as int, 
	@price_onRequestHours		as int, 
	@price_flagPrivate			as bit, 

	-- might make more sense to add a parameter - copy or not
	@FlagB2BDate				bit = 0,
	@FlagB2CDate				bit = 0,	

	@AuditUser					int,
	@DateOfChange				datetime,
	@Original_PriceID			int,							-- original price ID
	@ChangeSetID				uniqueidentifier,
	@SysDateTime				datetime2

As
Begin

	SET NOCOUNT ON;
	If (0=1) SET FMTONLY OFF;  -- enables meta data querying from code - Entity Framework temp table issues

	-- BEGIN TRANSACTION HANDLED BY PARENT PROCEDURE

	Declare @Message nvarchar(2000),@ERRORMESSAGE VARCHAR(1000)	

	Set @Message = 'Adding Origin Pricing : ' + Convert(varchar(10), @price_id)
	Exec appsys.AddMessage_OptionSplitting @ChangeSetID, @price_id, @Message, 'd_provenienza_price' ,null,null,@SysDateTime
	
	BEGIN TRY								
		-- get all provienza IDs to be added / get only the LAST record per provenienza ID
		Select	pprice.provenienza_id, 
				Max(pprice.provenienza_price_validoA)  provenienza_price_validoA
		Into #ID01
		From dbo.d_provenienza_price pprice
					Inner Join dbo.d_provenienza p on p.provenienza_id = pprice.provenienza_id
		Where	( 
					((@FlagB2BDate = 1) And  (p.provenienza_gruppo_id = 2)) Or		-- B2B
					((@FlagB2CDate = 1) And  (p.provenienza_gruppo_id In (1,3)) ) 	-- B2C
				) And
				pprice.price_id = @Original_PriceID 
		Group By pprice.provenienza_id

		Insert appsys.Messages_OptionSplitting ( SourceID, OptionId, [Message], TablesName, RelatedEntityID ,[Action], [TimeStamp])
		Select	@ChangeSetID,@price_id, CONVERT(VARCHAR,provenienza_id) + ' : ' + CONVERT(VARCHAR,provenienza_price_validoA,103), 'd_provenienza_price', provenienza_id, 'Identified', @SysDateTime
		From	#ID01			

		-- this logic is in the change procedure - only change records where there is only ONE origin price (for date changes) 
		-- probably not needed here (Stephen?)
		--					And ( Select Count(1) from dbo.d_provenienza_price pcount Where pcount.price_id = pprice.price_id And
		--																					pcount.provenienza_id = pprice.provenienza_id ) = 1

		Insert dbo.d_provenienza_price (
					provenienza_id, 
					price_id, 
					provenienza_price_validoDa, 
					provenienza_price_validoA, 
					provenienza_price_base, 
					provenienza_price_numeroPersoneBase, 
					provenienza_price_adult, 
					provenienza_price_adultEtaDa, 
					provenienza_price_adultEtaA, 
					provenienza_price_child, 
					provenienza_price_childEtaDa, 
					provenienza_price_childEtaA, 
					provenienza_price_senior, 
					provenienza_price_seniorEtaDa, 
					provenienza_price_seniorEtaA, 
					provenienza_price_infant, 
					provenienza_price_infantEtaDa, 
					provenienza_price_infantEtaA, 
					provenienza_price_freeSaleDays, 
					provenienza_price_onRequestHours, 
					provenienza_price_flagPrivate, 
					provenienza_price_note, 
					AdultConversionRate, 
					ChildConversionRate, 
					DateCreated, 
					CreatedBy, 
					DateAltered, 
					AlteredBy
				)
		Select
					pprice.provenienza_id, 
					@price_id, 
					@price_validoDa, 
					@price_validoA, 
					pprice.provenienza_price_base, 
					pprice.provenienza_price_numeroPersoneBase, 
					pprice.provenienza_price_adult, 
					@price_adultEtaDa, 
					@price_adultEtaA, 
					pprice.provenienza_price_child, 
					@price_childEtaDa, 
					@price_childEtaA, 
					pprice.provenienza_price_senior, 
					@price_seniorEtaDa, 
					@price_seniorEtaA, 
					pprice.provenienza_price_infant, 
					@price_infantEtaDa, 
					@price_infantEtaA, 
					@price_freeSaleDays, 
					@price_onRequestHours, 
					pprice.provenienza_price_flagPrivate,			-- copy
					pprice.provenienza_price_note,					-- copy
					pprice.AdultConversionRate,						-- copy
					pprice.ChildConversionRate,						-- copy
					GetDate(), 
					@AuditUser,
					null, 
					null
		From dbo.d_provenienza_price pprice
					Inner Join #ID01 On		pprice.provenienza_id = #ID01.provenienza_id AND						
											pprice.provenienza_price_validoA = #ID01.provenienza_price_validoA 
		Where pprice.price_id = @Original_PriceID 


		UPDATE	m
		SET		m.[Action] = 'Added'
		FROM	#ID01 id1
		inner join appsys.Messages_OptionSplitting m on m.RelatedEntityID = id1.provenienza_id  and m.SourceID = @ChangeSetID


		Insert audit.d_provenienza_price
			Select T.*, 'ADD', @DateOfChange, @AuditUser
			From dbo.d_provenienza_price T
			Where T.price_id = @price_id
				
		Drop Table #ID01
		

	END TRY
	BEGIN CATCH
		SELECT @ERRORMESSAGE = ERROR_MESSAGE()
		EXEC appsys.AddMessage_OptionSplitting @ChangeSetID, @price_id, @ERRORMESSAGE, 'd_product_price' ,null,'Error', @SysDateTime			
		RETURN 1
	END CATCH
	
	RETURN 0

End
