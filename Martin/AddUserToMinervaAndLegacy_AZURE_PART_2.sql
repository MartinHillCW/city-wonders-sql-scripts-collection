﻿/*---------------------------------------------------------------------------------------------------------
PART 2 - RUN ON CITYWONDERS

Insert the lines created in PART 1 at the end
---------------------------------------------------------------------------------------------------------*/

If Exists(Select name from sys.procedures where name = 'tmp_DeleteUser')
  Drop Procedure dbo.tmp_DeleteUser
Go

Create Procedure dbo.tmp_DeleteUser
		@UserName		nvarchar(150)
As 
 Begin
	Declare @LegacyID   int

	Select @LegacyID = [user_id] From dbo.d_User Where [User_Name] = @UserName
	If @LegacyID > 0 
	 begin
		Delete dbo.UsersBridge Where [user_id] =  @LegacyID
		Delete dbo.d_user_modulo WHERE [user_id] = @LegacyID
		Delete dbo.d_user_language WHERE [USER_ID] = @LegacyID
		Delete dbo.d_user WHERE [user_id] = @LegacyID
	 end

 End
Go

If Exists(Select name from sys.procedures where name = 'tmp_AddLegacyUser')
  Drop Procedure dbo.tmp_AddLegacyUser
Go

Create Procedure dbo.tmp_AddLegacyUser
		@UserName		nvarchar(150),
		@ForeName		nvarchar(150),
		@SurName		nvarchar(150),
		@EMail			nvarchar(150),
		@Initials		nvarchar(5),
		@MinervaID		int,
		@Password		nvarchar(50)
As
Begin

	Declare @HireDate varchar(8) = Convert(varchar, GetDate(), 112)

	Exec dbo.SP_USERS_ADD 
		@user_name = @UserName,
		@user_pwd = @Password,
		@supplier_id = NULL,
		@user_email = @EMail,
		@user_phone = NULL,
		@user_flagGuide = 0,
		@user_flagAdministrative = 1,
		@user_flagInServizio = 1,
		@user_firstName = @ForeName,
		@user_lastName = @SurName,
		@user_emailVisibile = 0,
		@user_phoneVisibile = 0,
		@user_office_city_id = 1,
		@user_lavoraTanto = 0,
		@user_acceptBigGroups = 0,
		@user_initials = @Initials,
		@user_flagBasilicaPass = 0,
		@user_page = N'defaultUser.aspx',
		@IsSupplier = 0,
		@user_supplierType = NULL,
		@auduser = 1,
		@user_B2BAdmin = 0,
		@user_B2CAdmin = 0,
		@user_flagCoordinator = 0,
		@user_flagAccompagnatore = 0,
		@user_IsGuide = 0,
		@user_isGuidaVaticana = 0,
		@user_isGuidaNazionale = 0,
		@user_isGuidaEstera = 0,
		@user_isGuidePrefecture = 0,
		@user_isGuideConferencierEcole = 0,
		@user_isTourLeaderUnlicenced = 0,
		@user_isTourLeaderLicenced = 0,
		@user_italianLicenceRegions = NULL,
		@user_italianLicenceProvinces = NULL,
		@city_id = 1,
		@user_notes = NULL,
		@user_rating = NULL,
		@user_flagVaticanPass = 0,
		@user_coordinatorNotes = NULL,
		@user_flagTrainer = 0,
		@user_Address = NULL,
		@user_HireDate = @HireDate,
		@Receive_Guide_Email = 0,
		@HiredBy = 522,
		@ExternalGuide = 0,
		@ExternalGuideType = NULL,
		@OutOfProvince = 0,
		@user_flagComunePass = 0,
		@user_flagPackage = 0,
		@user_flagLicensed = 0,
		@AttendedGuideSchool = NULL,
		@RateLevel = NULL,
		@languageIdsList = N'',
		@user_bankAccountIban = null,
		@user_bankAccountBic = null,
		@user_bankAccountHolderName = null,
		@user_bankAccountHolderAddress  = null,
		@user_bankAccountFiscalNumber = null,
		@user_bankAccountReference = null,
		@user_bankAccountNoIban = 0,
		@user_bankAccountIbanDateModified = null,
		@user_bankAccountIbanLastModifiedUserId  = null


	Declare @UserID		int
	Select @UserID = [user_id] From d_User Where [User_Name] = @UserName

	INSERT INTO UsersBridge (UserId, [user_id]) VALUES (@MinervaID, @UserID)

	DELETE FROM d_user_modulo WHERE [user_id] = @UserID

	INSERT INTO d_user_modulo  (user_id, modulo_id,user_modulo_readOnly) 
	SELECT @UserID,1,1  UNION ALL SELECT @UserID,2,1  UNION ALL SELECT @UserID,3,1  UNION ALL SELECT @UserID,4,1  UNION ALL SELECT @UserID,5,1  UNION ALL SELECT @UserID,6,1  UNION ALL SELECT @UserID,7,1  UNION ALL SELECT 
	@UserID,8,1  UNION ALL SELECT @UserID,9,1  UNION ALL SELECT @UserID,10,1  UNION ALL SELECT @UserID,11,1  UNION ALL SELECT @UserID,12,1  UNION ALL SELECT @UserID,13,1  UNION ALL SELECT @UserID,14,1  UNION ALL SELECT @UserID,15,1  UNION ALL SELECT @UserID,16,1  UNION ALL SELECT @UserID,17,1  UNION ALL 
	SELECT @UserID,22,1  UNION ALL SELECT @UserID,23,1  UNION ALL SELECT @UserID,24,1  UNION ALL SELECT @UserID,25,1  UNION ALL SELECT @UserID,26,0  UNION ALL SELECT @UserID,27,0  UNION ALL SELECT @UserID,32,1  UNION ALL SELECT @UserID,33,1  UNION ALL SELECT @UserID,34,1  UNION ALL SELECT @UserID,35,1  UNION ALL SELECT @UserID,36,1  UNION ALL SELECT @UserID,38,1  UNION ALL SELECT @UserID,39,0  UNION ALL SELECT @UserID,40,1  UNION ALL SELECT @UserID,41,1  UNION ALL SELECT @UserID,42,1  UNION ALL SELECT @UserID,43,1  UNION ALL SELECT @UserID,44,1  UNION ALL SELECT @UserID,45,1  UNION ALL SELECT @UserID,46,1  UNION ALL SELECT @UserID,48,1  UNION ALL SELECT @UserID,49,1  UNION ALL SELECT @UserID,50,1  UNION ALL SELECT @UserID,51,1  UNION ALL SELECT @UserID,52,1  UNION ALL SELECT @UserID,55,1  UNION ALL SELECT @UserID,57,1  UNION ALL SELECT @UserID,58,1  UNION ALL SELECT @UserID,59,1  UNION ALL SELECT @UserID,61,1  UNION ALL SELECT @UserID,64,1  UNION ALL SELECT @UserID,65,1  UNION ALL SELECT @UserID,66,1  UNION ALL SELECT @UserID,67,1  UNION ALL SELECT @UserID,68,1  UNION ALL SELECT @UserID,69,1  UNION ALL SELECT @UserID,70,1  UNION ALL SELECT @UserID,73,1  UNION ALL SELECT @UserID,74,1  UNION ALL SELECT @UserID,75,1  UNION ALL SELECT @UserID,76,1  UNION ALL SELECT @UserID,77,1  UNION ALL SELECT @UserID,78,1  UNION ALL SELECT @UserID,79,1  UNION ALL SELECT @UserID,80,0  UNION ALL SELECT @UserID,82,1  UNION ALL SELECT @UserID,84,1  UNION ALL SELECT @UserID,85,1  UNION ALL SELECT @UserID,86,1  UNION ALL SELECT @UserID,87,1  UNION ALL SELECT @UserID,88,1  UNION ALL SELECT @UserID,89,1  UNION ALL SELECT @UserID,91,1  UNION ALL SELECT @UserID,92,1  UNION ALL SELECT @UserID,94,1  UNION ALL SELECT @UserID,95,1  UNION ALL SELECT @UserID,96,1  UNION ALL SELECT @UserID,97,1  UNION ALL SELECT @UserID,98,1  UNION ALL SELECT @UserID,99,1  UNION ALL SELECT @UserID,100,1  UNION ALL SELECT @UserID,101,1  UNION ALL SELECT @UserID,105,1  UNION ALL SELECT @UserID,106,0  UNION ALL SELECT @UserID,107,1  UNION ALL SELECT @UserID,108,1  UNION ALL SELECT @UserID,109,1  UNION ALL SELECT @UserID,110,1  UNION ALL SELECT @UserID,111,1  UNION ALL SELECT @UserID,113,1  UNION ALL SELECT @UserID,114,1  UNION ALL SELECT @UserID,116,1  UNION ALL SELECT @UserID,117,1  UNION ALL SELECT @UserID,118,1  UNION ALL SELECT @UserID,119,1  UNION ALL SELECT @UserID,120,1  UNION ALL SELECT @UserID,121,1  UNION ALL SELECT @UserID,122,1  UNION ALL SELECT @UserID,123,1  UNION ALL SELECT @UserID,124,1  UNION ALL SELECT @UserID,125,1  UNION ALL SELECT @UserID,126,1  UNION ALL SELECT @UserID,127,1  UNION ALL SELECT @UserID,128,1  UNION ALL SELECT @UserID,129,1  UNION ALL SELECT 
	@UserID,130,1  UNION ALL SELECT @UserID,131,1  UNION ALL SELECT @UserID,132,1  UNION ALL SELECT @UserID,133,1  UNION ALL SELECT @UserID,134,1  UNION ALL SELECT @UserID,136,1  UNION ALL SELECT @UserID,137,1  UNION ALL SELECT @UserID,139,1  UNION ALL SELECT @UserID,140,1  UNION ALL SELECT 
	@UserID,141,1  UNION ALL SELECT @UserID,142,1  UNION ALL SELECT @UserID,143,1  UNION ALL SELECT @UserID,145,1  UNION ALL SELECT @UserID,146,1  UNION ALL SELECT @UserID,147,1  UNION ALL SELECT @UserID,148,1  UNION ALL SELECT @UserID,149,1  UNION ALL SELECT @UserID,150,1  UNION ALL SELECT 
	@UserID,151,1  UNION ALL SELECT @UserID,152,1  UNION ALL SELECT @UserID,154,1  UNION ALL SELECT @UserID,155,1  UNION ALL SELECT @UserID,156,1  UNION ALL SELECT @UserID,157,1  UNION ALL SELECT @UserID,159,1  UNION ALL SELECT @UserID,160,1  UNION ALL SELECT @UserID,162,1  UNION ALL SELECT 
	@UserID,163,1  UNION ALL SELECT @UserID,164,1  UNION ALL SELECT @UserID,165,1  UNION ALL SELECT @UserID,166,1  UNION ALL SELECT @UserID,167,1  UNION ALL SELECT @UserID,168,1  UNION ALL SELECT @UserID,169,1  UNION ALL SELECT @UserID,170,1  UNION ALL SELECT @UserID,171,1  UNION ALL SELECT 
	@UserID,172,1  UNION ALL SELECT @UserID,173,1  UNION ALL SELECT @UserID,174,1  UNION ALL SELECT @UserID,175,1  UNION ALL SELECT @UserID,176,1  UNION ALL SELECT @UserID,177,1  UNION ALL SELECT @UserID,178,1  UNION ALL SELECT @UserID,179,1  UNION ALL SELECT @UserID,180,1  UNION ALL SELECT 
	@UserID,181,1  UNION ALL SELECT @UserID,182,1  UNION ALL SELECT @UserID,183,1  UNION ALL SELECT @UserID,184,1  UNION ALL SELECT @UserID,185,1  UNION ALL SELECT @UserID,186,1  UNION ALL SELECT @UserID,187,1  UNION ALL SELECT @UserID,188,1  UNION ALL SELECT @UserID,189,1  UNION ALL SELECT 
	@UserID,190,1  UNION ALL SELECT @UserID,191,1  UNION ALL SELECT @UserID,192,1  UNION ALL SELECT @UserID,193,1  UNION ALL SELECT @UserID,194,1  UNION ALL SELECT @UserID,195,1  UNION ALL SELECT @UserID,196,1  UNION ALL SELECT @UserID,197,1  UNION ALL SELECT @UserID,198,1  UNION ALL SELECT 
	@UserID,199,1  UNION ALL SELECT @UserID,200,1  UNION ALL SELECT @UserID,201,1  UNION ALL SELECT @UserID,202,1  UNION ALL SELECT @UserID,203,1  UNION ALL SELECT @UserID,204,1  UNION ALL SELECT @UserID,205,1  UNION ALL SELECT @UserID,207,1  UNION ALL SELECT @UserID,208,1  UNION ALL SELECT @UserID,209,1  UNION ALL SELECT @UserID,211,1  UNION ALL SELECT @UserID,212,1  UNION ALL SELECT @UserID,213,1  UNION ALL SELECT @UserID,214,1  UNION ALL SELECT @UserID,217,1  UNION ALL SELECT @UserID,218,1  UNION ALL SELECT @UserID,219,1  UNION ALL SELECT @UserID,220,1  UNION ALL SELECT @UserID,221,1  UNION ALL SELECT @UserID,222,1  UNION ALL SELECT @UserID,224,1  UNION ALL SELECT @UserID,225,1  UNION ALL SELECT @UserID,226,1  UNION ALL SELECT @UserID,227,1  UNION ALL SELECT @UserID,228,1  UNION ALL SELECT @UserID,230,1  UNION ALL SELECT @UserID,232,1  UNION ALL SELECT @UserID,233,1  UNION ALL SELECT @UserID,234,1  UNION ALL SELECT @UserID,235,1  UNION ALL SELECT @UserID,236,1  UNION ALL SELECT @UserID,237,1  UNION ALL SELECT @UserID,238,1  UNION ALL SELECT @UserID,239,1  UNION ALL SELECT @UserID,240,1  UNION ALL SELECT @UserID,241,1  UNION ALL SELECT @UserID,242,1  UNION ALL SELECT @UserID,243,1  UNION ALL SELECT @UserID,245,1  UNION ALL SELECT @UserID,252,1  UNION ALL SELECT @UserID,254,1  UNION ALL SELECT @UserID,255,1  UNION ALL SELECT @UserID,256,1  UNION ALL SELECT @UserID,257,1  UNION ALL SELECT @UserID,267,1  UNION ALL SELECT @UserID,269,1  UNION ALL SELECT @UserID,280,1  UNION ALL SELECT @UserID,281,1  UNION ALL SELECT @UserID,294,1  UNION ALL SELECT @UserID,295,1  UNION ALL SELECT @UserID,63,1  UNION ALL SELECT @UserID,62,1  UNION ALL SELECT @UserID,28,1  UNION ALL SELECT @UserID,37,1  UNION ALL SELECT @UserID,83,1  UNION ALL SELECT @UserID,47,1  UNION ALL SELECT @UserID,135,1  UNION ALL SELECT @UserID,102,1  UNION ALL SELECT @UserID,138,1  UNION ALL SELECT @UserID,229,1  UNION ALL SELECT @UserID,231,1  UNION ALL SELECT @UserID,251,1  UNION ALL SELECT @UserID,253,1  UNION ALL SELECT @UserID,268,1  UNION ALL SELECT @UserID,250,1

end
Go

-------------------------------------------------------------------------------
--- INSERT CREATED LINE FROM PART 1 HERE
-------------------------------------------------------------------------------

Exec dbo.tmp_AddLegacyUser N'Konrad.Balys', N'Konrad', N'Balys', N'kbalys@future-processing.com', N'FP_KB', 4180, N'VBKRdjIfkm42aWKmZxNitVxepmvj63u/hYoCsDL8Wkw='
Exec dbo.tmp_AddLegacyUser N'Mateusz.Łyżwiński', N'Mateusz', N'Łyżwiński', N'mlyzwinski@future-processing.com', N'FP_MT', 4181, N'0UcK8dLwwz6FBLYMLrQA9o6FNDU4gudq1s3ngnbtw0w='
Exec dbo.tmp_AddLegacyUser N'Daniel.Jaros', N'Daniel', N'Jaros', N'djaros@future-processing.com', N'FP_DJ', 4182, N'sEg2icjaGQ4cP41UPGqpyjK5BtwpYcN8kUK/RKiNMCw='
Exec dbo.tmp_AddLegacyUser N'Bartłomiej.Glac', N'Bartłomiej', N'Glac', N'bglac@future-processing.com', N'FP_BG', 4183, N'8qa6yb4+jXiCYv3I6stmApgK+UX80PYAYPNIrU6q0mQ='
Exec dbo.tmp_AddLegacyUser N'Daniel.Feist', N'Daniel', N'Feist', N'dfeist@future-processing.com', N'FP_DF', 4184, N'clPzbX9RuJ+wYXVld8zEuYc4aVd6tVCSdDQFb0y5wRs='
Exec dbo.tmp_AddLegacyUser N'Piotr.Opaczyński', N'Piotr', N'Opaczyński', N'popaczynski@future-processing.com', N'FP_PO', 4185, N'SaeoUYWLitgFldHHaEjT8xIkCzXAhJuZ6NmGKH4JUsI='
Exec dbo.tmp_AddLegacyUser N'Łukasz.Ciura', N'Łukasz', N'Ciura', N'lciura@future-processing.com', N'FP_PO', 4186, N'UAqlMv8B238lIZf+kH5tf+SF/j/Chsup5+FyqLsl3Cc='
Exec dbo.tmp_AddLegacyUser N'Łukasz.Sośniak', N'Łukasz', N'Sośniak', N'lsosniak@future-processing.com', N'FP_PO', 4187, N'OUxM3Sb5fuVIOSW9UkviWd4cL0cuYQXwpTJkQ1Zqxpw='
Exec dbo.tmp_AddLegacyUser N'Wojciech.Niemkowski', N'Wojciech', N'Niemkowski', N'wniemkowski@future-processing.com', N'FP_PO', 4188, N'GxhMRMsVNohA4mQdVp2E7AOCutYFFwGnP7XiJT7abMs='

-------------------------------------------------------------------------------
--- OTHER
-------------------------------------------------------------------------------

/* -- Delete User Account

Exec dbo.tmp_DeleteUser 'Konrad.Balys'
Exec dbo.tmp_DeleteUser 'Mateusz.Łyżwiński'
Exec dbo.tmp_DeleteUser 'Daniel.Jaros'
Exec dbo.tmp_DeleteUser 'Bartłomiej.Glac'
Exec dbo.tmp_DeleteUser 'Daniel.Feist'
Exec dbo.tmp_DeleteUser 'Rafał.Ostrowski'
Exec dbo.tmp_DeleteUser 'Piotr.Opaczyński'

*/



/* TEST
Select * from dbo.d_user where [user_name] = 'Daniel Feist'
-- Legacy User ID
Select * from dbo.UsersBridge where [user_id] = 3765
Select * from dbo.d_user_modulo WHERE [user_id] = 3765
*/





-------------------------------------------------------------------------------
--- RUN THIS ON CITYWONDERS AND MINERVA TO ADD Konrad and Bartłomiej as DBO
-------------------------------------------------------------------------------

CREATE USER [kbalys.future-processing]
	FOR LOGIN [kbalys.future-processing]
	WITH DEFAULT_SCHEMA = dbo
GO

CREATE USER [bglac.future-processing]
	FOR LOGIN [bglac.future-processing]
	WITH DEFAULT_SCHEMA = dbo
GO


EXEC sp_addrolemember N'db_owner', N'bglac.future-processing'
GO

EXEC sp_addrolemember N'db_owner', N'kbalys.future-processing'
GO

Update dbo.d_user Set user_flagInServizio = 1 Where user_name = 'lbo.test'
Update dbo.d_user Set user_flagInServizio = 0 Where user_name = 'lbo.prod'



Konrad.Balys
Mateusz.Łyżwiński
Daniel.Jaros
Bartłomiej.Glac
Daniel.Feist
Piotr.Opaczyński
Łukasz.Ciura
Łukasz.Sośniak
Wojciech.Niemkowski
