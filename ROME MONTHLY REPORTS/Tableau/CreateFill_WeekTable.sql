		SET DATEFORMAT DMY;
		SET DATEFIRST 1; -- MONDAY

		/*
		Drop Table CityWonders_GuideManagement_Weeks
		Go

		Create Table CityWonders_GuideManagement_Weeks  (
						OID				int IDENTITY(1,1)	NOT NULL,
						CityId			int					NOT NULL,
						WeekStart		date				NOT NULL,
						WeekEnd			date				NOT NULL,
						WeekNumber		int					NOT NULL
					)
		Alter Table CityWonders_GuideManagement_Weeks 
				Add Constraint PK_CityWonders_GuideManagement_Weeks PRIMARY KEY (OID)

		*/

		Create Table #WeeksByYearData  (
			[CityId] int,
			[Year] int,
			[WeekStart] DATETIME,
			[NoOfWeeks] int
		)

		-- amend here to add weeks
		INSERT INTO #WeeksByYearData (CityId,[Year],[WeekStart],[NoOfWeeks]) 
		VALUES  -- (1,2015,'23/03/2015',32)
				--,(8,2015,'23/03/2015',32)
				--,(1,2016,'21/03/2016',32)
				--,(8,2016,'21/03/2016',32)
				--,(1,2017,'01/05/2017',26)
				--,(8,2017,'01/05/2017',26)
				--,(1,2018,'30/04/2018',26)
				--,(8,2018,'30/04/2018',24)
				--,(1,2019,'15/04/2019',29)
				--,(8,2019,'15/04/2019',28)
			
		;WITH WeekNumbers ( Number ) AS (
			SELECT 0 UNION ALL
			SELECT 1 + Number FROM WeekNumbers WHERE Number < 31
		)
		SELECT
			A.CityId
		   ,A.Year
		   ,DATEADD(WK,N.Number, a.WeekStart)	[WeekStart]
		   ,DATEADD(day,6, DATEADD(WK,N.Number, a.WeekStart)) [WeekEnd]
		   ,N.Number+1 WeekNumber
		INTO #WeekNumbersByYear   
		FROM
		   #WeeksByYearData A
		   INNER JOIN WeekNumbers N ON N.Number <= A.NoOfWeeks
		ORDER BY A.CityId,DATEADD(WK,N.Number, a.WeekStart)

		Insert dbo.CityWonders_GuideManagement_Weeks ( CityId, WeekStart, WeekEnd, WeekNumber )
			Select CityID, WeekStart, WeekEnd, WeekNumber From #WeekNumbersByYear

		Drop Table #WeekNumbersByYear
		Drop Table #WeeksByYearData