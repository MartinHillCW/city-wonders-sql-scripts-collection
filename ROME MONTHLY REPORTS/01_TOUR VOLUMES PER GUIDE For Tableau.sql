Alter Procedure report.upd_GM_MonthlyTourVolume
/* ---------------------------------------------------------------------------------
Procedure			report.rpt_GetBookingCancelationStats
Description			Get stats per tour instance per origin

Change		By				Change
2019-06-04	Martin Hill		Created
------------------------------------------------------------------------------------*/
as
begin

		SET TRANSACTION ISOLATION LEVEL SNAPSHOT  
		SET NOCOUNT ON

		SET DATEFORMAT DMY;
		SET DATEFIRST 1; -- MONDAY

		Exec sys_DropObject 'GM_MonthlyTourVolume','report'

		Create Table #WeeksByYearData  (
			[CityId] int,
			[Year] int,
			[WeekStart] DATETIME,
			[NoOfWeeks] int
		)

		INSERT INTO #WeeksByYearData (CityId,[Year],[WeekStart],[NoOfWeeks]) 
		VALUES   (1,2015,'23/03/2015',32)
				,(8,2015,'23/03/2015',32)
				,(1,2016,'21/03/2016',32)
				,(8,2016,'21/03/2016',32)
				,(1,2017,'01/05/2017',26)
				,(8,2017,'01/05/2017',26)
				,(1,2018,'30/04/2018',26)
				,(8,2018,'30/04/2018',24)
				,(1,2019,'15/04/2019',29)
				,(8,2019,'15/04/2019',28)

		;WITH WeekNumbers ( Number ) AS (
			SELECT 0 UNION ALL
			SELECT 1 + Number FROM WeekNumbers WHERE Number < 31
		)
		SELECT
			A.CityId
		   ,A.Year
		   ,DATEADD(WK,N.Number, a.WeekStart)	[WeekStart]
		   ,DATEADD(day,6, DATEADD(WK,N.Number, a.WeekStart)) [WeekEnd]
		   ,N.Number+1 WeekNumber
		INTO #WeekNumbersByYear   
		FROM
		   #WeeksByYearData A
		   INNER JOIN WeekNumbers N ON N.Number <= A.NoOfWeeks
		ORDER BY A.CityId,DATEADD(WK,N.Number, a.WeekStart)

		--SELECT * FROM #WeekNumbersByYear

		-- ASSIGNMANET
		SELECT	 ISNULL(u.user_firstName,'') + ' ' + ISNULL(u.user_lastName,'')						AS [USER NAME]			
				,ur.user_role_desc																	AS [ROLE]
				,CAST(REPLACE(STR(DATEPART(DAY,ag.assignement_date),2),' ','0') AS VARCHAR(2))		AS [DAY]
				,CAST(REPLACE(STR(DATEPART(MONTH,ag.assignement_date),2),' ','0') AS VARCHAR(2))	AS [MONTH]
				,YEAR(ag.assignement_date)															AS [YEAR]	
				,wn.WeekNumber																		AS [WEEK NUMBER]		
				,ag.assignement_date																AS [ASSIGNEMENT DATE]
				,ISNULL(P.product_title, N'') + N' ' + ISNULL(pp.price_desc, N'') + N' - ' + ISNULL(P.product_code, N'') AS [PRODUCT NAME]		
				,REPLACE(REPLACE(REPLACE(g.group_desc, CHAR(9), ''), CHAR(10), ''), CHAR(13), '')	AS [GROUP DESC]	
				,p.product_titleNEW																	AS [PRODUCT STD NAME]
				,c.city_desc																		AS [CITY DESC]
				,CASE WHEN p.product_flagVatican = 1 
					THEN 'Vatican' 
					ELSE c.city_desc 
				 END																				AS [PRODUCT STD CITY]
				,CASE WHEN u.user_flagLicensed = 1 
					THEN 'YES'
					ELSE 'NO'
	 			 END																				AS [LICENSED]
	 			,CASE WHEN u.user_flagVaticanPass = 1 THEN 'YES' ELSE 'NO' END						AS [VATICAN PASS]
				,CASE WHEN u.user_flagBasilicaPass =1 THEN 'YES' ELSE 'NO' END						AS [BASILICA PASS]
				,CASE WHEN u.user_flagComunePass =1 THEN 'YES' ELSE 'NO' END						AS [COMUNE PASS]
				,(SELECT SUM(user_rate_pack_tourPromessi) AS Expr1 FROM citywonders.dbo.d_user_rate_pack WHERE user_id = ag.user_id AND ag.assignement_date BETWEEN user_rate_pack_validFrom AND user_rate_pack_validTo) AS TourPromessi
				,ag.assignement_scheduleDate														AS [SCHEDULED DATE]
				,CASE WHEN ag.assignement_isExtraRole	 = 1 
					THEN 'YES'
					ELSE 'NO'
	 			 END																				AS [IS EXTRA ROLE]
	 			 ,u.user_IsExternalGuide															AS [IS EXTERNAL GUIDE]
	 			 ,egt.[desc]																		AS [EXTERNAL GUIDE TYPE]
		-- 	 	 ,ROW_NUMBER() OVER(ORDER BY YEAR(ag.assignement_date) ASC)							AS [Row]
		INTO	#DATA	 	 																				
		FROM	citywonders.dbo.d_assignement_guide ag
					INNER JOIN citywonders.dbo.d_product_price pp on pp.price_id = ag.price_id
					INNER Join citywonders.dbo.d_product P On P.product_id = pp.product_id
					INNER JOIN citywonders.dbo.d_group G on G.group_id = pp.group_id
					INNER JOIN citywonders.dbo.d_city C on C.city_id = p.city_id
					INNER JOIN citywonders.dbo.d_user u ON u.user_id = ag.user_id
					INNER JOIN citywonders.dbo.d_user_role ur ON ur.user_role_id = ag.user_role_id
					LEFT  JOIN #WeekNumbersByYear wn on (wn.CityId = p.city_id) and ag.assignement_date BETWEEN wn.WeekStart AND wn.WeekEnd
					LEFT  JOIN citywonders.dbo.d_guides_ExternalType egt ON egt.id = u.user_ExternalGuideType
		WHERE	(year(ag.assignement_date) >= 2017)
		--AND		u.city_id = 8
		ORDER BY 
			5,4,3,1

		SELECT * INTO report.GM_MonthlyTourVolume FROM #DATA

		DROP TABLE #DATA	
		DROP TABLE #WeekNumbersByYear	
		DROP TABLE #WeeksByYearData

End
Go

--Select Top 1000 * From report.GM_MonthlyTourVolume Where [Scheduled Date] = '20170401'