USE CITYWONDERS
SET LANGUAGE ENGLISH;
SELECT	 uc.user_id_segnalato							[User Id]
		,u.user_name									[Guide Name]
		,p.product_title								[Product]
		,pp.price_desc									[Option]
		,YEAR(uc.booking_date)							[Year]
		,MONTH(uc.booking_date)							[Month Id]
		,DATENAME(MONTH,uc.booking_date)				[Month]
		,cur.currency_desc								[Currency]
		,c1.city_desc									[Guide City]		
		,CONVERT(INT,ROUND(SUM(uc.commissione) * 10,0))	[SALES]
FROM	v_user_commission uc
			inner join d_user u on u.user_id = user_id_segnalato 
			inner join d_product p on p.product_id = uc.product_id
			inner join d_city c on c.city_id = p.city_id
			inner join d_city c1 on c1.city_id = u.city_id
			inner join d_currency cur on cur.currency_id = c.CurrencyId
			inner join d_product_price pp on pp.price_id = uc.price_id
WHERE	1=1
AND		YEAR(booking_date) >= 2014
--AND		user_id = 1930
GROUP BY
	 uc.user_id_segnalato
	,u.user_name
	,p.product_title
	,pp.price_desc
	,YEAR(uc.booking_date)
	,MONTH(uc.booking_date)
	,DATENAME(MONTH,uc.booking_date)
	,cur.currency_desc
	,c1.city_desc

ORDER BY 2 ASC, 3, 4 ASC	