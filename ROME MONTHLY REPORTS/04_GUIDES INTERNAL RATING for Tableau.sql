Alter Procedure report.upd_GM_MonthlyInternalGuideRating
/* ---------------------------------------------------------------------------------
Procedure			report.upd_GM_MonthlyInternalGuideRating
Description			Get stats relating to internal feedback

Change		By				Change
2019-06-04	Martin Hill		Created
------------------------------------------------------------------------------------*/
as
begin

		SET TRANSACTION ISOLATION LEVEL SNAPSHOT  
		SET NOCOUNT ON

		SET DATEFORMAT DMY;
		SET DATEFIRST 1; -- MONDAY

		Exec sys_DropObject 'GM_MonthlyInternalGuideRating','report'

		SELECT	 c.city_desc												[CITY]
				,p.product_title											[PRODUCT]
				,u.user_name												[USER NAME]
				,et.[desc]													[USER_EXTERNALGUIDETYPE]
				,YEAR(u.user_HireDate)										[HIRED]
				,f.feedback_date											[FEEDBACK DATE]
				,f.feedback_dateActivity									[TOUR DATE]
				,CONVERT(VARCHAR(3),CASE WHEN U.user_flagLicensed = 1 
						THEN 'YES' ELSE 'NO' 
				 END)														[IS LICENSED?]
				,CAST(YEAR(f.feedback_date) AS INT)							[YEARVAL]
				,CAST(MONTH(F.feedback_date) AS INT)						[MONTHVAL]

				,CAST(f.feedback_starRating_guide AS INT)					[GUIDE RATING]
				,CAST(f.feedback_starRating_activity AS INT)				[TOUR RATING]
				,REPLACE(REPLACE(CONVERT(VARCHAR(4000),f.feedback_text), CHAR(13), ''), CHAR(10), '')						[FEEDBACK]				
				,REPLACE(REPLACE(CONVERT(VARCHAR(4000),f.feedback_howFind_notes), CHAR(13), ''), CHAR(10), '')				[NOTES]
		Into #DATA
		FROM	citywonders.dbo.d_feedback f ( nolock)
					INNER JOIN		citywonders.dbo.d_product p					ON p.product_id = f.product_id
					LEFT OUTER join citywonders.dbo.d_user u					ON u.user_id = f.user_id
					INNER JOIN		citywonders.dbo.d_city c					ON c.city_id = p.city_id
					LEFT JOIN		citywonders.dbo.d_guides_ExternalType et	ON u.user_ExternalGuideType = et.id
		Where CAST(YEAR(f.feedback_date) AS INT) >= 2016
		--AND		c.city_id = 3
		--AND		CAST(YEAR(f.feedback_date) AS INT) NOT IN (2017)
		order by 8,9 asc

		SELECT * INTO report.GM_MonthlyInternalGuideRating FROM #DATA

		DROP TABLE #DATA	

End
Go

Exec report.upd_GM_MonthlyInternalGuideRating