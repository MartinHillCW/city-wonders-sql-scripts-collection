select	product_id,
		product_title,
		CASE 
			WHEN product_title LIKE '%Tickets%' THEN 'N' 
			WHEN product_id IN (314,430,388,457) THEN 'N'
			ELSE 'Y'
		END [Recieves Feedback],
		CASE 
			WHEN product_title LIKE '%Tickets%' THEN 'Tickets' 
			WHEN product_id IN (314,430,388,457) THEN 'Requested'
			ELSE ''
		END [Reason for Exclusion]
from	d_product
order by product_title