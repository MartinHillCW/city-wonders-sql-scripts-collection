use citywonders
select	 f.Deleted [Deleted]
		,f.discounted [Discounted]
		,discounted.user_name DiscountedBy
		,f.DiscountedReasonId
		,f.DateDiscounted		
		,f.DiscountedReasonComments			
		,deleted.user_name DeletedBy
		,f.DateDeleted		
		,f.UndiscountedDate
		,f.UndiscountedBy		
		,f.*
from	d_feedback f
	LEFT join d_user discounted on discounted.user_id = f.discountedby
	LEFT join d_user deleted on discounted.user_id = f.DeletedBy
where	Deleted = 1
or		discounted = 1