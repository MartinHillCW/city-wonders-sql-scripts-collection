-- output
-- to be copied in the Excel file
-- filler columns can be emptied in the file
USE CITYWONDERS
Select
		pp.product_id														ProductID,
		b.booking_id														BookingID,
		b.booking_dateTour													TourDate,
		CAST(0 as int)														FeedbackReceived,
		ISNULL([booking_contactEmail],'')									EMAIL,
		ISNULL([booking_flagEmailFeedback],0)								FeedbackSendInd,
		[booking_dataInvioEmailFeedback]									FeedbackSendDate,
		ISNULL(booking_nAdults, 0) 
				+ ISNULL(booking_nChild, 0)									TotalPax, 
	
		ISNULL(booking_nAdults_show, ISNULL(booking_nAdults, 0)) + 
				+ ISNULL(booking_nChild_show, ISNULL(booking_nChild, 0))	TotalShow	
Into #Booking
From dbo.d_booking b with (nolock)
				Inner Join dbo.d_product_price pp on b.price_id = pp.price_id
				WHERE	b.booking_flagPagato = 1 AND
						b.booking_flagDeleted = 0  AND
						( 
							( Year(b.booking_dateTour) In ( 2018,2019) ) OR
							( Year(b.[booking_dataInvioEmailFeedback]) IN (2018,2019)  )
						)

-- Drop Table #Feedback
Update bo
		Set FeedbackReceived = 1
From dbo.d_feedback b with (nolock)
				Inner Join #Booking bo on bo.BookingID = b.booking_id
WHERE	ISNULL(b.Deleted,0) = 0 



-- Drop Table #Result
Create Table #Result (
		KPI_Month				int,
		ProductID				int,
		BookingsA				int,
		BookingsB				int,
		BookingsChange			decimal(10,2),
		CustomerA				int,

		CustomerB				int,
		CustomerChange			decimal(10,2),
		CustomerShowA			int,
		CustomerShowB			int,
		CustomerShowChange		decimal(10,2),
		CustomerEMailA			int,
		CustomerEMailB			int,
		CustomerEMailChange		decimal(10,2),
		CustomerFeedbackSendA	int,
		CustomerFeedbackSendB	int,
		CustomerFeedbackSendChange	decimal(10,2),
		FeedbackRequestSendA		int,
		FeedbackRequestSendB		int,
		FeedbackRequestSendChange	decimal(10,2),
		FeedbackA				int,
		FeedbackB				int,
		FeedbackChange			decimal(10,2)
)

Truncate Table #RESULT

Insert #Result ( KPI_Month, ProductID ) Select Distinct 1, ProductID From #Booking Where Month(TourDate) = 1
Insert #Result ( KPI_Month, ProductID ) Select Distinct 2, ProductID From #Booking Where Month(TourDate) = 2
Insert #Result ( KPI_Month, ProductID ) Select Distinct 3, ProductID From #Booking Where Month(TourDate) = 3
Insert #Result ( KPI_Month, ProductID ) Select Distinct 4, ProductID From #Booking Where Month(TourDate) = 4
Insert #Result ( KPI_Month, ProductID ) Select Distinct 5, ProductID From #Booking Where Month(TourDate) = 5
Insert #Result ( KPI_Month, ProductID ) Select Distinct 6, ProductID From #Booking Where Month(TourDate) = 6
Insert #Result ( KPI_Month, ProductID ) Select Distinct 7, ProductID From #Booking Where Month(TourDate) = 7
Insert #Result ( KPI_Month, ProductID ) Select Distinct 8, ProductID From #Booking Where Month(TourDate) = 8
Insert #Result ( KPI_Month, ProductID ) Select Distinct 9, ProductID From #Booking Where Month(TourDate) = 9
Insert #Result ( KPI_Month, ProductID ) Select Distinct 10, ProductID From #Booking Where Month(TourDate) = 10
Insert #Result ( KPI_Month, ProductID ) Select Distinct 11, ProductID From #Booking Where Month(TourDate) = 11
Insert #Result ( KPI_Month, ProductID ) Select Distinct 12, ProductID From #Booking Where Month(TourDate) = 12

Declare @YearA		int		= 2018,
		@YearB		int		= 2019

Update #Result Set
		BookingsA		= (Select Count(BookingID) from #Booking Where Year(TourDate) = @YearA and Month(TourDate) = #Result.KPI_Month and ProductID = #Result.ProductID),
		CustomerA		= (Select SUM(TotalPax) from #Booking Where Year(TourDate) = @YearA and Month(TourDate) = #Result.KPI_Month and ProductID = #Result.ProductID),
		CustomerShowA	= (Select SUM(TotalShow) from #Booking Where Year(TourDate) = @YearA and Month(TourDate) = #Result.KPI_Month and ProductID = #Result.ProductID),
		CustomerEMailA	= (Select Count(BookingID) from #Booking Where Year(TourDate) = @YearA and Month(TourDate) = #Result.KPI_Month and EMail <> ''  and ProductID = #Result.ProductID),
		CustomerFeedbackSendA = (Select Count(BookingID) from #Booking Where Year(TourDate) = @YearA and Month(TourDate) = #Result.KPI_Month and FeedbackSendInd = 1 and ProductID = #Result.ProductID),
		FeedbackA			= (Select Count(BookingID) from #Booking Where Year(TourDate) = @YearA and Month(TourDate) = #Result.KPI_Month And ProductID = #Result.ProductID And FeedbackReceived = 1)

Update #Result Set
		BookingsB		= (Select Count(BookingID) from #Booking Where Year(TourDate) = @YearB and Month(TourDate) = #Result.KPI_Month and ProductID = #Result.ProductID),
		CustomerB		= (Select SUM(TotalPax) from #Booking Where Year(TourDate) = @YearB and Month(TourDate) = #Result.KPI_Month and ProductID = #Result.ProductID),
		CustomerShowB	= (Select SUM(TotalShow) from #Booking Where Year(TourDate) = @YearB and Month(TourDate) = #Result.KPI_Month and ProductID = #Result.ProductID),
		CustomerEMailB	= (Select Count(BookingID) from #Booking Where Year(TourDate) = @YearB and Month(TourDate) = #Result.KPI_Month and EMail <> '' and ProductID = #Result.ProductID),
		CustomerFeedbackSendB = (Select Count(BookingID) from #Booking Where Year(TourDate) = @YearB and Month(TourDate) = #Result.KPI_Month and FeedbackSendInd = 1 and ProductID = #Result.ProductID),
		FeedbackB			= (Select Count(BookingID) from #Booking Where Year(TourDate) = @YearB and Month(TourDate) = #Result.KPI_Month And ProductID = #Result.ProductID And FeedbackReceived = 1)


Select 
--		P.product_title	Product,
		KPI_Month,
		SUM(BookingsA)				BookingsA,
		SUM(BookingsB)				BookingsB,
		SUM(CustomerFeedbackSendA)	CustomerFeedbackSendA,
		SUM(CustomerFeedbackSendB)	CustomerFeedbackSendB,
		SUM(FeedbackA)				FeedbackA,
		SUM(FeedbackB)				FeedbackB
Into #PerMonth		
From #RESULT R
Group By KPI_Month
Order By KPI_Month

-- Totals
Select
		KPI_Month,
		'' [f1],
		BookingsA,
		BookingsB,
		Case
			When BookingsA = 0 And BookingsB = 0 then 0
			When BookingsA = 0 then 1
			Else BookingsB * 1.0 / BookingsA - 1
		End	BookingsChange,
		''			f2,
		CustomerFeedbackSendA,
		CustomerFeedbackSendB,
		Case
			When CustomerFeedbackSendA = 0 And CustomerFeedbackSendB = 0 then 0
			When CustomerFeedbackSendA = 0 then 1
			Else CustomerFeedbackSendB * 1.0 / CustomerFeedbackSendA - 1
		End	CustomerFeedbackSendChange,
		'' f3,
		FeedbackA,
		FeedbackB,
		Case
			When FeedbackB = 0 And FeedbackB = 0 then 0
			When FeedbackA = 0 then 1
			Else FeedbackB * 1.0 / FeedbackA - 1
		End	FeedbackChange,
		'' f4,
		Case
			When FeedbackA = 0 And CustomerFeedbackSendA = 0 then 0
			When CustomerFeedbackSendA = 0 then 1
			Else FeedbackA * 1.0 / CustomerFeedbackSendA 
		End	FeedbackResponseA,
		Case
			When FeedbackB = 0 And CustomerFeedbackSendB = 0 then 0
			When CustomerFeedbackSendB = 0 then 1
			Else FeedbackB * 1.0 / CustomerFeedbackSendB 
		End	FeedbackResponseB
From #PerMonth


Select	R.KPI_Month,
		Case 
			When P.product_flagVatican = 1 Then 'Vatican'
			Else C.city_desc
		End City,
		SUM(BookingsA)					BookingsA,
		SUM(BookingsB)					BookingsB,
		SUM(CustomerFeedbackSendA)		CustomerFeedbackSendA,
		SUM(CustomerFeedbackSendB)		CustomerFeedbackSendB,
		SUM(FeedbackA)					FeedbackA,
		SUM(FeedbackB)					FeedbackB
Into #RESULT_CITY
From #RESULT R
		Inner Join d_product P on R.ProductID = P.product_id
		Inner Join d_city C On C.city_id = P.city_id
Group By 
		R.KPI_Month,
		Case 
			When P.product_flagVatican = 1 Then 'Vatican'
			Else C.city_desc
		End


Select	R.KPI_Month,
		P.product_title					Product,
		SUM(BookingsA)					BookingsA,
		SUM(BookingsB)					BookingsB,
		SUM(CustomerFeedbackSendA)		CustomerFeedbackSendA,
		SUM(CustomerFeedbackSendB)		CustomerFeedbackSendB,
		SUM(FeedbackA)					FeedbackA,
		SUM(FeedbackB)					FeedbackB
Into #RESULT_PRODUCT
From #RESULT R
		Inner Join d_product P on R.ProductID = P.product_id
Group By 
		R.KPI_Month,
		P.product_title

-- Per City - Month
Select 
		City,
		KPI_Month,
		'' f1,

		BookingsA,
		BookingsB,
		Case
			When BookingsA = 0 And BookingsB = 0 then 0
			When BookingsA = 0 then 1
			Else BookingsB * 1.0 / BookingsA - 1
		End	BookingsChange,
		'' f2,
		CustomerFeedbackSendA,
		CustomerFeedbackSendB,
		Case
			When CustomerFeedbackSendA = 0 And CustomerFeedbackSendB = 0 then 0
			When CustomerFeedbackSendA = 0 then 1
			Else CustomerFeedbackSendB * 1.0 / CustomerFeedbackSendA - 1
		End	CustomerFeedbackSendChange,
		'' f3,
		FeedbackA,
		FeedbackB,
		Case
			When FeedbackA = 0 And FeedbackB = 0 then 0
			When FeedbackA = 0 then 1
			Else FeedbackB * 1.0 / FeedbackA - 1
		End	FeedbackChange,
		'' f4,

		Case
			When FeedbackA = 0 And CustomerFeedbackSendA = 0 then 0
			When CustomerFeedbackSendA = 0 then 1
			Else FeedbackA * 1.0 / CustomerFeedbackSendA 
		End	FeedbackResponce2016,
		Case
			When FeedbackB = 0 And CustomerFeedbackSendB = 0 then 0
			When CustomerFeedbackSendB = 0 then 1
			Else FeedbackB * 1.0 / CustomerFeedbackSendB 
		End	FeedbackResponce2017
From #RESULT_CITY R
Order By 
		City,
		KPI_Month


-- Per Product - Month
Select 
		Product,
		KPI_Month,
		'' f1,

		BookingsA,
		BookingsB,
		Case
			When BookingsA = 0 And BookingsB = 0 then 0
			When BookingsA = 0 then 1
			Else BookingsB * 1.0 / BookingsA - 1
		End	BookingsChange,
		'' f2,
		CustomerFeedbackSendA,
		CustomerFeedbackSendB,
		Case
			When CustomerFeedbackSendA = 0 And CustomerFeedbackSendB = 0 then 0
			When CustomerFeedbackSendA = 0 then 1
			Else CustomerFeedbackSendB * 1.0 / CustomerFeedbackSendA - 1
		End	CustomerFeedbackSendChange,
		'' f3,
		FeedbackA,
		FeedbackB,
		Case
			When FeedbackA = 0 And FeedbackB = 0 then 0
			When FeedbackA = 0 then 1
			Else FeedbackB * 1.0 / FeedbackA - 1
		End	FeedbackChange,
		'' f4,

		Case
			When FeedbackA = 0 And CustomerFeedbackSendA = 0 then 0
			When CustomerFeedbackSendA = 0 then 1
			Else FeedbackA * 1.0 / CustomerFeedbackSendA 
		End	FeedbackResponce2016,
		Case
			When FeedbackB = 0 And CustomerFeedbackSendB = 0 then 0
			When CustomerFeedbackSendB = 0 then 1
			Else FeedbackB * 1.0 / CustomerFeedbackSendB 
		End	FeedbackResponce2017
From #RESULT_PRODUCT R
Order By 
		Product,
		KPI_Month

Drop Table #RESULT_PRODUCT
Drop Table #RESULT_CITY
Drop Table #PerMonth
Drop Table #Result
Drop Table #Booking
