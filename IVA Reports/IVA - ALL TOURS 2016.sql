DECLARE @PRODUCTID INT = NULL, @OPTIONID INT = NULL, @OFFSET INT = -120

--SET @PRODUCTID = 34
--SET @OPTIONID = 992
--SET @@OFFSET = 992

SELECT	 booking_id		
		,billing_id
		,booking_date		[Booking Date]
		,booking_dateTour	[Tour Date]		
		,product_name		[Product Name]		
		,tot_pax			[Total Pax]
		,tot_show			[Total Show]
		,booking_nAdults	[Adults]
		,booking_nChild		[Child]
		,booking_nInfant	[Infant]
		,product_id
		,price_id
		,client_name		[Client Name]		
FROM	v_booking 
WHERE	booking_dateTour >= DATEADD(DAY,@OFFSET,GETDATE())
AND		(product_id = @PRODUCTID OR @PRODUCTID IS NULL)
AND		(price_id = @OPTIONID OR @OPTIONID IS NULL)

ORDER BY booking_dateTour ASC