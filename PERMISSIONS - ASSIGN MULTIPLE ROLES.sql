
DECLARE @USERLIST VARCHAR(100)
DECLARE @USER INT

--	SELECT	* FROM	d_user where user_name like '%vanessa%'

SELECT	@USERLIST = '8,1225,233'
SELECT	@USER = 797




SELECT	*
INTO	#tmpModulo
FROM	d_user_modulo
WHERE	user_id IN (SELECT items from fn_Split(@USERLIST,','))


SELECT	modulo_id,
		MIN(CONVERT(INT,user_modulo_readOnly)) user_modulo_readOnly
INTO	#tmpPermissions		
FROM	#tmpModulo 
GROUP BY 
		modulo_id

DELETE FROM d_user_modulo 
WHERE	user_id = @USER
AND		modulo_id IN (SELECT modulo_id FROM #tmpPermissions)

INSERT INTO d_user_modulo
SELECT	@USER, 
		tm.modulo_id,
		tm.user_modulo_readonly
FROM	#tmpPermissions tm 		

--SELECT DISTINCT modulo_id FROM #tmpModulo
DROP TABLE #tmpPermissions
DROP TABLE #tmpModulo