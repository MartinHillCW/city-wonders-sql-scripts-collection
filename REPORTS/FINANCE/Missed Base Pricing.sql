SELECT	p.product_title
		,p.product_id
		,pp.price_id,pp.price_base
		,pp.price_validoDa
		,pp.price_validoA

FROM	[dbo].[d_product_price] pp
inner join dbo.d_product p on p.product_id = pp.product_id
left join [dbo].[d_FinanceGrossUp_BasePrices] bp on bp.option_id = pp.price_id
where	bp.option_id is null
and		pp.price_validoA >= GETDATE()
and		pp.price_base <> 0
order by pp.product_id

