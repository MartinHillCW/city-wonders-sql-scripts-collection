SET DATEFORMAT DMY;

DECLARE @MESSAGEID INT
DECLARE @FROMDATE DATETIME, @TODATE DATETIME

SET	@FROMDATE = '01/01/2014'; SET @TODATE = GETDATE()
SET @MESSAGEID = NULL	

/*
set dateformat dmy;
SELECT agentid,* 
	FROM	[moxie].[dbo].[mailmessage]	
	WHERE  1=1
	--AND		MsgCC is NOT null
	AND		daterecv BETWEEN '01/01/2014' AND '02/01/2014'	
	AND		(msgid = @MESSAGEID OR @MESSAGEID IS NULL)
	
--468701
*/

--CREATE TABLE [dbo].[pre_address](
--	[address_type_id] [tinyint]		NOT NULL,
--	[email_address] [varchar](255)	NOT NULL,
--	[msgid] int						NOT NULL,
--	[email_name] [varchar](255)		NULL
--)

--GO

TRUNCATE TABLE pre_address

INSERT INTO pre_address (address_type_id,msgid,email_address,email_name)
SELECT	 1 AddressType
		,msgid
		,x.item		
		,x.item	
FROM	[moxie].[dbo].[mailmessage]	mm
		cross apply (select RTRIM(LTRIM(items)) item from dbo.fn_Split(mm.MsgFrom,CASE WHEN CHARINDEX(';',mm.MsgFrom) > 0  THEN ';' ELSE ',' END) ) x
WHERE	(msgid = @MESSAGEID OR @MESSAGEID IS NULL)
AND		daterecv BETWEEN @FROMDATE AND @TODATE
UNION
SELECT	 2 AddressType
		,msgid
		,x.item		
		,x.item	
FROM	[moxie].[dbo].[mailmessage] mm
		cross apply (select RTRIM(LTRIM(items)) item from dbo.fn_Split(mm.MsgTo,CASE WHEN CHARINDEX(';',mm.MsgTo) > 0  THEN ';' ELSE ',' END) ) x
WHERE	(msgid = @MESSAGEID OR @MESSAGEID IS NULL)
AND		daterecv BETWEEN @FROMDATE AND @TODATE
UNION
SELECT	 3 AddressType
		,msgid
		,x.item		
		,x.item	
FROM	[moxie].[dbo].[mailmessage] mm
		cross apply (select RTRIM(LTRIM(items)) item from dbo.fn_Split(mm.MsgCC,CASE WHEN CHARINDEX(';',mm.MsgCC) > 0  THEN ';' ELSE ',' END) ) x
WHERE	(msgid = @MESSAGEID OR @MESSAGEID IS NULL)
AND		daterecv BETWEEN @FROMDATE AND @TODATE

ORDER BY msgid

SELECT	*
FROM	pre_address
where	msgid = 705967