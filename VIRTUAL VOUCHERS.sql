
select	 p.product_title
		,c.*
		,g.group_desc
		,v.venue_desc
from	[dbo].[ProductTicketAvailabilityConfig] c
inner join d_product p on p.product_id = c.ProductId
inner join d_product_price pp on pp.product_id = p.product_id
inner join d_group g on g.group_id = pp.group_id
inner join d_fascia f on f.group_id = g.group_id
left join d_venue_fascia vf on vf.fascia_id = f.fascia_id
Left join d_venue v on v.venue_id = vf.venue_id
group by  p.product_title
		,c.[id], c.[ProductId], c.[UnlimitedTickets], c.[UseVirtualVoucher], c.[VirtualVoucherNumber], c.[UsePercentage], c.[PercentageValue],g.group_desc,v.venue_desc
order by p.product_title


/*
select * from d_venue where venue_id = 19
select * from d_venue_fascia where venue_id = 19
select * from d_fascia where fascia_id IN (select fascia_id from d_venue_fascia where venue_id = 19)
*/