
BEGIN TRAN
	
	DECLARE	@BOOKINGID INT, @BILLINGIDMASTER INT, @BOOKINGIDMASTER INT
	
	SET	@BOOKINGID			= 1753818
	SET	@BILLINGIDMASTER	= 1454826	 
	SET	@BOOKINGIDMASTER	= 1753413
	
	--	COMMIT TRAN				-- REBOOKING/MISSED
	
	--	select * from D_TRANSACTIONS_TYPE
INSERT INTO d_transactions (booking_id, billing_id, trans_amount, trans_transcode_id, trans_transdate, trans_comment, 
			trans_user_id,trans_currency_rate, billing_id_master, trans_date_last_change, fattura_id, trans_status,booking_id_master,salescurrency_id
		)						
	SELECT 
		 BOOKING_ID							--booking_id, 
		,BILLING_ID							--billing_id, 
		,booking_finalPrice					--trans_amount, 
		,2									--trans_transcode_id, 
		,booking_date						--trans_transdate, 
		,'AMENDED'							--trans_comment, 
		,user_id_ins						--trans_user_id, 
		,booking_cambio						--trans_currency_rate, 
		,@BILLINGIDMASTER					--billing_id_master, 
		,getdate()							--trans_date_last_change,
		,NULL								--fattura_id, 
		,1									--trans_status,
		,@BOOKINGIDMASTER					--booking_id_master
		,1									-- salescrurrencyid
	FROM d_booking 
	WHERE booking_id = @BOOKINGID
	
	SELECT	*
	FROM	d_transactions 
	WHERE	booking_id = @BOOKINGID
	
--		COMMIT TRAN
--		ROLLBACK TRAN