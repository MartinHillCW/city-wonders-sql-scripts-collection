select count(*)
from
 [dbo].[d_booking]
 where 
	-- booking_contactNumber <> '-'
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 30
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'

	and booking_billingCountry not IN
	( 
		'Argentina',
		'Australia',
		'Austria',
		'AU',
		'Brazil',
		'CA',
		'Canada',
		'FRANCE',
		'Germany',
		'GB',
		'Netherlands',
		'Mexico',
		'Poland',
		'Singapore',
		'South Africa',
		'Spain',
		'Switzerland',
		'US',
		'United Kingdom',
		'United States'
	)
select TOP 20 
booking_firstNameLead,
booking_lastNameLead,
booking_contactEmail,
booking_contactNumber,
booking_billingAddress,
booking_billingCountry,
CustomerFullName,
LEN(booking_billingAddress),
*
 FROM 
 [dbo].[d_booking]
 where 
	-- booking_contactNumber <> '-'
	booking_billingAddress IS NOT NULL
--	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 10
	and booking_billingAddress <> 'N/A -- N/A'

/*	and booking_id IN 
	(
	1189468, -- anon
1586987,-- anon
1903816,
1986196, -- anon
2197923, -- anon
2285356, -- anon
2326673,
2499942  -- anon
	)
	*/

	/*
UPDATE [dbo].[d_booking]
Set booking_billingAddress = 'Fake Mailing Address; anytown; someplace in the world 123345'
where booking_id = 905563
*/

	/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '1. Stanley Madisons 742 Parkpad Avenue, Schenectady New York 12304  USA 2. Maureen Madisons 742 Parkpad Avenue, Schenectady New York 12304 USA 3. Rick Abigail 123 Sunrise Avenue, Sayville New York 11782 USA 4. Jasca Abigail 123 Sunrise Avenue, Sayville Ne',
--	CustomerFullName = 'Madisons Mr Stanley',
	booking_firstNameLead = 'Mr Stanley',
	booking_lastNameLead = 'Madisons'
where booking_id = 1189468
*/

	/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = 'Deangelos, MARIO: Volta 321, C.P. 4498-Godoy Cruz, Mendoza, Argentina. Costas, TERESITA NORMA: Volta 321, C.P.4498-Godoy Cruz, Mendoza, Argentina. Deangelos, Elsa SUSANA: J.P.Norton 321, C.P.4496-Lujan de Cuyo, Mendoza, Argentina.',
--	CustomerFullName = 'Madisons Mr Stanley',
	booking_firstNameLead = 'Elsa Susana',
	booking_lastNameLead = 'Deangelos'
where booking_id = 1586987
*/

	/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '1214 Highmountain Drive Houston TX 77063 USA   Lucida Thamesis 412 Venus Road Memphis TN 38117 USA   Roggina Thamesis 412 Venus Road Memphis TN 38117 USA   Lucy Hearndon 1337 Singleton Houston TX 77008 USA   Angela Henry 1020 Yukon Street Houston TX 77006',
--	CustomerFullName = 'Madisons Mr Stanley',
	booking_firstNameLead = 'Bruce',
	booking_lastNameLead = 'Hundley'
where booking_id = 1986196
*/


	/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '3210 berry street, villa corazon san agustin, san fernando pampanga, 2000.  PHILIPPINES // 213 sundial drive GreenFields country homes sindalan city of san fernando, pampanga 2000 PHILIPPINES //  12 san juan bautista betis guagua pampanga 2003 PHILIPPINES',
	booking_firstNameLead = 'Paul Wallacen',
	booking_lastNameLead = 'Horatio'
where booking_id = 2197923
*/

	/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = 'Rua Federicco Isoldi  789 ap 12, block 3 CEP 05441-040 Sao Paulo/SP - Brasil (Brasile) This address is of Gerverai Carlos F�lix De Alex and my daugther R�ela F�lix De Alex My father Rogers Da Flava Pires is: Rua Florian�polis 231-E CEP 89812-107 Chapeco/S',
	booking_firstNameLead = 'Gerverai F�lix',
	booking_lastNameLead = 'De Alex'
where booking_id = 2285356
*/

	/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = 'Argentino Hendleys Upton  4221 SW 34th Court, Coconut Creek FL 33073 - USA  Katherine Firth 213 Jacksons Road, Woonsocket RI 02895 - USA  Leanne Glen  9921 Coolmine Bank Road, North Smithfield RI 02896 - USA   Ronnie Busch 12420 Petite Court, Wyoming MN 5',
	booking_firstNameLead = 'Argentino',
	booking_lastNameLead = 'Hendleys Upton'
where booking_id = 2499942
*/

	/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = 'D�rte Robbsen Ringstr. 11 13921 Nordenham // Michaela Rank-Husse Altenbruch 27478 Cuxhaven // Ruddrin Gro�pietsch Postbrookstr. 12c 24368 Bremerhaven // Andrea Larcsott-Yenge Krummenacker 44 24368 Bremerhaven',
--	booking_firstNameLead = 'Argentino',
	booking_lastNameLead = 'Robbsen'
where booking_id = 1903816
*/

/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '1232 Sunshines Ranch Rd Campo,Ca 91906 // 4212 Cabo Bahia Chula Vista,Ca 91914 // PO Box 322119 San Ysidro, Ca 92143 //  213   Colben Ave  Chula Vista, Ca 91913 // 112 Paseo LaPuenta Chula Vista, Ca  91910',
	booking_firstNameLead = 'Roger',
	booking_lastNameLead = 'Sally'
where booking_id = 1903816
*/

/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '6/9 Trinity Street Dublin Ireland // 4212 College Green Dublin Ireland D2 // PO Box 12345 San Ysidro, FL 92143 //  213 City Wonders Ave Ober�, CA 91913 // 112 Dark Rome Provincia: Misiones, Pais :Argentina'
where 
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 200
	and LEN(booking_billingAddress) < 250
	and booking_billingAddress <> 'N/A -- N/A'
*/

/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = 'Avenida General Gozzi N� 16.6/9, Edificio 2, Piso 3�, Departamento 22, City Wonders, Partido de La Dark Roma, Provincia de Buenos Aires, Rep�blica Argentina'
where 
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 150
	and LEN(booking_billingAddress) <= 200
	and booking_billingAddress <> 'N/A -- N/A'
*/
/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = 'Amalfi #6/9 Privada Vista Gozzi, Dark Roma, Mexicali, Baja California, M�xico. C.P. 10141.'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 80
	and LEN(booking_billingAddress) <= 100
	and booking_billingAddress <> 'N/A -- N/A'
--	and booking_billingAddress LIKE 'Amalfi #6/9 Privada Vista Gozzi, Mexicali, Baja California, M�xico. C.P. 10141.'
*/

/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = 'St Simone"s Presbytery, City Wonders Rd, East Winning, Dark Roma DH7 6/9.'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 70
	and LEN(booking_billingAddress) <= 80
	and booking_billingAddress <> 'N/A -- N/A'
*/
/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '6/9 City Wonders Boulevard 80113 Dark Rome Colorado, United States'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 60
	and LEN(booking_billingAddress) <= 70
	and booking_billingAddress <> 'N/A -- N/A'
*/
/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '69 Trinity street, Dark Rome, Cairns  Queensland Australia'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 55
	and LEN(booking_billingAddress) <= 60
	and booking_billingAddress <> 'N/A -- N/A'
*/
/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '6/9 Trinity street, Dark Rome height"s, Victoria 2008'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 50
	and LEN(booking_billingAddress) <= 55
	and booking_billingAddress <> 'N/A -- N/A'
*/
/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '6/9 RUE DE LA LIBERATION 12345 DARK ROMA - FRANCE'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 47
	and LEN(booking_billingAddress) <= 50
	and booking_billingAddress <> 'N/A -- N/A'
*/
/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = 'Hintere Marktstra�e 69 84085 Dark Roma Germany'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 45
	and LEN(booking_billingAddress) <= 47
	and booking_billingAddress <> 'N/A -- N/A'
*/
/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '6/9 Trinity San Francisco California 94122'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 40
	and LEN(booking_billingAddress) <= 45
	and booking_billingAddress <> 'N/A -- N/A'
*/
/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '6/9 NE DarkRome, Portland, OR 97212'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 30
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'US',
		'United States'
	)
*/
/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '6/9 City Wonders  Auburn, AL  36830 USA'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 30
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'US',
		'United States'
	)
*/
/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '10/12 College Green -- Lake forest'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 30
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'US',
		'United States'
	)
*/
/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '6/9, AVENUE CITY WONDERS 25017 PARIS'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'FRANCE'
	)
*/

/*

UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = 'Dark Roma 10/12 -- Buenos aires'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'Argentina'
	)

UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '6/9 DARK ROME AVE BLAXLAND 1747'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'Australia',
		'AU'
	)

UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = 'Jagdschlossgasse 6/9, 1234 Dark Wien'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'Austria'
	)

UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = 'RUA CITY WONDERS  6/9 APT 2008'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'Brazil'
	)

UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '1501-833 Dark Rome street'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'CA',
		'Canada'
	)

	UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '6/9 College Green, Dark Rome DR11'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'GB',
		'United Kingdom'
	)
	
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = 'Gru�dorfstr. 6/9, 13579 Dark Rome'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'Germany'
	)

UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = 'Ret. Simone Dark Roma # 17 -- Df'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'Mexico'
	)

UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = 'Neeskesweide 6/9, Heerewaarden'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'Netherlands'
	)

UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = 'Dark Roma 10/12, 01-101 Warszawa'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'Poland'
	)

	
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '6/9 CITY WONDERS WAY MULBARTON'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'South Africa'
	)
	
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '123 City Wonders Road #6-9'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'Singapore'
	)

UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = 'ISLETA, City Wonders 2 -- ubrique'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'Spain'
	)

UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = 'Kohlenweg 6/9, 1234 Liebefeld'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry IN
	( 
		'Switzerland'
	)

*/
/*
UPDATE [dbo].[d_booking]
Set 
	booking_billingAddress = '10/12 City Wonders Lane, Dark Rome'
where
	booking_billingAddress IS NOT NULL
	and LEN(booking_billingAddress) > 10
	and LEN(booking_billingAddress) <= 40
	and booking_billingAddress <> 'N/A -- N/A'
	and booking_billingCountry not IN
	( 
		'Argentina',
		'Australia',
		'Austria',
		'AU',
		'Brazil',
		'CA',
		'Canada',
		'FRANCE',
		'Germany',
		'GB',
		'Netherlands',
		'Mexico',
		'Poland',
		'Singapore',
		'South Africa',
		'Spain',
		'Switzerland',
		'US',
		'United Kingdom',
		'United States'
	)
*/