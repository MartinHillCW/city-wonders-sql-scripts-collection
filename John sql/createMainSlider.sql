USE [darkrometest]
GO

/****** Object:  StoredProcedure [dbo].[darkproc_command_booking]    Script Date: 04/26/2013 16:15:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[darkproc_command_booking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[darkproc_command_booking]
GO

USE [darkrometest]
GO

/****** Object:  StoredProcedure [dbo].[darkproc_command_booking]    Script Date: 04/26/2013 16:15:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[darkproc_command_booking]
	-- Add the parameters for the stored procedure here
	@command_number INT,
	@billing_id INT = NULL,
    @price_id INT = NULL,
    @booking_dateTour NVARCHAR(10) = NULL, 
    @currency_id INT = NULL,
    @booking_finalPrice NUMERIC(18,2) = NULL,
    @booking_nAdults INT = NULL,
    @booking_nChild INT = NULL,
    @booking_nInfant INT = NULL,
    @booking_nSenior INT = NULL,
    @payment_id INT = NULL,
    @provenienza_id INT = NULL,
    @user_id_ins INT = NULL,
    @booking_flagPagato INT = NULL,
    @sito_id INT = NULL,
    @booking_cambio NUMERIC(18,4) = NULL,
    @booking_id_pacchetto INT = NULL,
    @booking_firstNameLead NVARCHAR(255) = NULL,
    @booking_lastNameLead NVARCHAR(255) = NULL,
    @booking_specialRequirements  NVARCHAR(255) = NULL,
    @booking_contactEmail  NVARCHAR(255) = NULL,
	@booking_contactNumber  NVARCHAR(255) = NULL,
	@booking_billingAddress  NVARCHAR(255) = NULL,
	@booking_billingCountry  NVARCHAR(255) = NULL,
	@fattura_id INT = NULL,
	@booking_id INT = NULL,
	@promoDiscount INT = NULL,
	@promoCode NVARCHAR(255) = NULL
	
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    -- numero di item e rispettivo valore nel carrello
    
    IF @command_number = 1
		BEGIN
    
			SET DATEFORMAT DMY;
			INSERT INTO d_booking 
			(billing_id,price_id,booking_dateTour,currency_id,booking_finalPrice,
							booking_nAdults,booking_nChild,booking_nInfant,booking_nSenior,
							payment_id,provenienza_id,
							user_id_ins,booking_flagPagato,sito_id,booking_cambio,booking_id_pacchetto,
							booking_firstNameLead,booking_lastNameLead,booking_specialRequirements,
							booking_contactEmail,booking_contactNumber,booking_billingAddress,
							booking_billingCountry,fattura_id
							) 
							VALUES (
							@billing_id ,
							@price_id,
							@booking_dateTour, 
							@currency_id,
							@booking_finalPrice,
							@booking_nAdults,
							@booking_nChild,
							@booking_nInfant,
							@booking_nSenior,
							@payment_id,
							@provenienza_id,
							@user_id_ins,
							@booking_flagPagato,
							@sito_id,
							@booking_cambio,
							@booking_id_pacchetto,
							@booking_firstNameLead,
							@booking_lastNameLead,
							@booking_specialRequirements,
							@booking_contactEmail,
							@booking_contactNumber,
							@booking_billingAddress,
							@booking_billingCountry,
							@fattura_id)
		END
		
		
		
		
	IF @command_number = 2
		BEGIN
		
		  UPDATE d_booking SET 
                    booking_firstNameLead = @booking_firstNameLead,
                    booking_lastNameLead = @booking_lastNameLead,
                    booking_specialRequirements = @booking_specialRequirements,
                    booking_contactEmail  = @booking_contactEmail,
                    booking_contactNumber  = @booking_contactNumber,
                    booking_billingAddress  = @booking_billingAddress,
                    booking_billingCountry  = @booking_billingCountry
                    WHERE booking_id = @booking_id
		END



	/* da eliminare  */
	IF @command_number = 3
		BEGIN
			UPDATE d_booking 
			SET 
				booking_flagPagato = 1, 
				fattura_id = (SELECT ISNULL(max(fattura_id),0) + 1 FROM d_booking) 
				WHERE billing_id = @billing_id
		END



	IF @command_number = 4
		BEGIN
			UPDATE d_booking 
			SET 
				booking_flagPagato = 1, 
				fattura_id = (SELECT ISNULL(max(fattura_id),0) + 1 FROM d_booking) ,
				booking_finalPrice =  (booking_finalPrice - booking_finalPrice * @promoDiscount /100)
				
				WHERE billing_id = @billing_id
				
				
				IF @promoCode IS NOT NULL
				BEGIN
					UPDATE d_promo_code 
					SET
						billing_id_utilizzatoDa = @billing_id, 
						promo_code_dataUtilizzo = GETDATE()
					WHERE
						promo_code_code = @promoCode
				END
				
		END

			
END

GO


