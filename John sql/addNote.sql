USE [darkrometest]
GO

/****** Object:  StoredProcedure [dbo].[sp_refendTracker_email]    Script Date: 04/19/2013 14:24:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RefundTracker_addNote]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RefundTracker_addNote]
GO

USE [darkrometest]
GO

/****** Object:  StoredProcedure [dbo].[sp_RefendTracker_addNote]    Script Date: 04/19/2013 14:24:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--sp_RefendTracker_addNote @note ='test',@user_id=522, @trans_id=365574
CREATE PROCEDURE [dbo].[sp_RefundTracker_addNote]( 
	@note		NVarChar(max),
	@user_id	INT,
	@trans_id	INT
)
AS
BEGIN
	SET DATEFORMAT DMY; SET LANGUAGE ENGLISH;
	
	DECLARE @name NVarChar(max);
	
	SELECT @name=user_name FROM d_user WHERE user_id = @user_id
	PRINT 'Refund Status changed to ' + @note+ ' by '+@name+' on ' + CONVERT(VARCHAR(8), GETDATE(), 103)
	INSERT INTO d_refundtracker_notes (trans_id,note) VALUES (@trans_id,'Refund Status changed to ' + @note+ ' by '+@name+' on ' + CONVERT(VARCHAR(8), GETDATE(), 103))
	
	
END

GO


