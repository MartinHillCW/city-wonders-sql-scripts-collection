
DECLARE @Billing_ID INT, @Amount DECIMAL(10,2), @Booking_id INT, @Trans_id INT, @BillingIdMaster INT
--	select * from D_TRANSACTIONS_TYPE
------------------------------------------------------------------------------
SELECT	@Billing_ID =   	 1188579   , 		
		@BillingIdMaster = @Billing_ID 
------------------------------------------------------------------------------

SELECT * FROM d_transactions WHERE billing_id_master	= @BillingIdMaster				--AND trans_amount = @Amount
SELECT * FROM d_transactions WHERE billing_id			= @Billing_ID					--AND trans_amount = @Amount

SELECT @Trans_id = Trans_id FROM d_transactions			WHERE billing_id = @Billing_ID	--AND trans_amount = @Amount
SELECT @Booking_id = booking_id FROM d_transactions		WHERE billing_id = @Billing_ID	--AND trans_amount = @Amount

-- BOOKING
SELECT booking_finalPrice,* FROM d_booking WHERE booking_id = @Booking_id

-- SHOW DATA
SELECT @Trans_id TransId, @Booking_id BookingId


BEGIN TRAN	

-- STEP 01 DELETE
	----	DELETE BOOKING RECORDS
	DELETE FROM d_Trans_RefundReasons where trans_id	IN (979294)
	DELETE FROM d_transactions where trans_id			IN (979294)
		
	UPDATE d_booking SET 
		 booking_finalprice = booking_finalprice+0.80
		--,booking_nAdults = 3
		--,booking_nChild = 0
		,booking_flagDeleted = 0
	WHERE booking_id IN(1390676) --1132300
	
	UPDATE d_transactions SET trans_amount = 216 WHERE trans_id = 963834			--TRANSAMOUNT:	
	UPDATE d_transactions SET trans_currency_rate = 1.101000 WHERE trans_id in (976264,940510)			--TRANS RATE	--1.10110000
	
	/*
		DECLARE @TID INT; DECLARE @BID INT; DECLARE @PRICE DECIMAL(16,2);
		
		SET @TID= 979294;SET @BID= 1428089;SET @PRICE=158.25
		
		DELETE FROM d_Trans_RefundReasons where trans_id	IN (@TID)
		DELETE FROM d_transactions where trans_id			IN (@TID)		
		UPDATE d_booking SET booking_finalprice = @PRICE	 WHERE booking_id = @BID	
	*/
	
	/*
		DECLARE @BID INT; 
		SET @BID= 1410367
		DELETE FROM d_booking_note where booking_id			IN (@BID)
		DELETE FROM d_booking_template where booking_id		IN (@BID)
		DELETE FROM D_BOOKING_GUIDE where booking_id		IN (@BID)
		DELETE FROM d_booking where booking_id				IN (@BID)
	*/
----	COMMIT TRAN

-- STPE 02	AMEND
		insert into d_billing (billing_date) SELECT (GETDATE())
	UPDATE d_transactions SET billing_id_master = 1168309, billing_id = 1168309 WHERE trans_id IN(949457)	--BILLINGID:	
	--UPDATE d_transactions SET booking_id_master = 1352022,booking_id = 1352022 WHERE trans_id IN (911442,911974)				--BILLINGID:	
	
	
	--UPDATE d_transactions SET trans_amount = 112.10 WHERE trans_id = 871614			--TRANSAMOUNT:	
	--UPDATE d_transactions SET trans_transcode_id = 2 WHERE trans_id = 959248			--TRANSAMOUNT:		
	--UPDATE d_transactions SET trans_currency_rate = 1.12662200 WHERE trans_id IN (909138)	--RATE :			
	--UPDATE d_transactions SET trans_transcode_id = 2 WHERE trans_id IN (922830)	--RATE :	
	--UPDATE d_transactions SET trans_status = 2 WHERE trans_id IN (810107)	--RATE :	
	--UPDATE d_transactions SET salescurrency_id = 3 WHERE trans_id IN (915050)	--RATE :	
	--UPDATE d_transactions SET billing_id = 1161334 WHERE trans_id IN (949455)	--RATE :	
	
	-- select * from d_payment

	--	UPDATE d_booking SET booking_finalprice = 0			WHERE booking_id IN(1385521)
	--	UPDATE d_booking SET booking_id_pacchetto = 1239944		WHERE booking_id IN(1311757)
	
	--	UPDATE d_booking SET payment_id = 3 WHERE booking_id IN (1371499)
	--	UPDATE d_booking SET billing_id = 1084234 WHERE booking_id IN(1304512)
	--	UPDATE d_booking SET provenienza_id = 1 WHERE booking_id IN(1208202)	
	--	UPDATE d_booking SET booking_flagPagato = 1 WHERE booking_id IN(1408278,1408279)
	--	UPDATE d_booking SET booking_flagDeleted = 1 WHERE booking_id IN(1206330)
	--	UPDATE d_booking SET booking_nAdults = 2, booking_nChild = 1 WHERE booking_id IN(1297146)
	
	--UPDATE d_transactions SET booking_id_master = 1024376 WHERE trans_id IN(659166)
	
	--UPDATE d_transactions SET booking_id = 1403009, booking_id_master = 1403009 WHERE trans_id IN(957087)

--	ROLLBACK TRAN
--	COMMIT TRAN
