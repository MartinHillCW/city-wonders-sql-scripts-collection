SET DATEFORMAT DMY;

DECLARE @FROMDATE DATETIME, @TODATE DATETIME, @PRICEID VARCHAR(100), @CITYID INT, @VATICAN BIT, @DAYS INT, @PRODUCTID VARCHAR(100), @DISTRIBUTOR VARCHAR(100), @PACKAGETOUR INT
DECLARE @MAXTICKETS INT, @GROUPID INT, @CHANNELDAYS INT

SET @FROMDATE		= '24/07/2016'
SET @DAYS			= 0
SET @TODATE			= DATEADD(DAY,@DAYS,@FROMDATE)
SET	@PRICEID		= null--'975,976,977'--'861'N
SET	@PRODUCTID		= null--'26,34,271'--'271,34,26'--'1'--'148,177'
SET @CITYID			= 3
SET @VATICAN		= NULL
SET @DISTRIBUTOR	= 2
SET @PACKAGETOUR	= null
SEt @GROUPID		= null

SELECT	@MAXTICKETS = CASE WHEN @PRODUCTID = '62' THEN 25 ELSE NULL END -- LAST SUPPER TICKETS
SELECT	@CHANNELDAYS = null

SELECT	 product_title			[Product]
		,P						[P]
		,price_desc				[Option]
		,[Tour date]			[Tour date]
		,[Days 2 Tour]			[DAYS]
		,d.product_id			[PID]
		,group_id				[GID]
		,d.price_id				[OID]
		--,utcHoldExpiration
		,[MAX]					[MT]						-- MAX TICKETS
		,[Voucher PAX]			[Pax V]						-- VOUCHER PAX
		,CAST(ISNULL([VVP],0) as int)		[Pax VV]		-- VOUCHER PAX - VIRTUAL VOUCHER CONFIG
		,[Booked]				[Booked]		
		,Available				[Available]
		,CAST([VVP]	as int) 		- [Booked]			[Available VV]
		,[D Tickets LEFT]
		--,sum(sc.totAdults) + sum(sc.totChild)			[reserved]
FROM (
	SELECT	 p.product_title
			,p.product_flagPacchetto 'P'
			,pp.price_desc
			,ISNULL(m.maxticket_giorno,v.booking_dateTour) [Tour date]
			,p.product_id
			,pp.group_id
			,pp.price_id	
			--,sc.utcHoldExpiration
			,DATEDIFF(DAY,GETDATE(),@FROMDATE)
				[Days 2 Tour]
				
			-- GENERAL TICKETS
			,ISNULL(@MAXTICKETS,CASE WHEN p.product_id = 62 THEN 25 ELSE m.maxticket_number END)
				[Max]	
			,ISNULL(d.voucher_paxAllocabili,d.voucher_pax)
				[Voucher PAX]
			,CASE 
				WHEN ptav.UnlimitedTickets IS NOT NULL THEN 999999
				WHEN ptav.UsePercentage = 1 THEN (ptav.PercentageValue * ISNULL(d.voucher_paxAllocabili,d.voucher_pax)) + ISNULL(d.voucher_paxAllocabili,d.voucher_pax)
				WHEN ptav.UseVirtualVoucher = 1 THEN d.voucher_pax_max + ISNULL(d.voucher_paxAllocabili,d.voucher_pax)
			 END  
				[VVP]
			,SUM(ISNULL(v.booking_nAdults_show+v.booking_nChild_show,ISNULL(v.booking_nAdults+v.booking_nChild,0)))		
				[Booked]			
				
			,Available = (ISNULL(ISNULL(@MAXTICKETS,m.maxticket_number),ISNULL(d.voucher_paxAllocabili,d.voucher_pax)) - 
					SUM(ISNULL(v.booking_nAdults_show+v.booking_nChild_show,ISNULL(v.booking_nAdults+v.booking_nChild,0))))
			
			-- SPLIT			
			,ISNULL(@MAXTICKETS,FLOOR(ISNULL(m.maxticket_number,ISNULL(d.voucher_paxAllocabili,d.voucher_pax)) * 
					CASE WHEN DATEDIFF(DAY,GETDATE(), DATEADD(DAY,ISNULL(-@CHANNELDAYS,-dgp.IgnorePercentageDays),@FROMDATE)) > 0 THEN dgp.Percentage ELSE 1.0 END))												
				[B2B ALLOWED]
			,SUM(ISNULL(v3.booking_nAdults_show+v3.booking_nChild_show,ISNULL(v3.booking_nAdults+v3.booking_nChild,0)))					
				[B2B SOLD]
			,ISNULL(@MAXTICKETS,CEILING(ISNULL(m.maxticket_number,ISNULL(d.voucher_paxAllocabili,d.voucher_pax)) * (CASE WHEN DATEDIFF(DAY,GETDATE(), DATEADD(DAY,ISNULL(-@CHANNELDAYS,-dgp.IgnorePercentageDays),@FROMDATE)) > 0 THEN dgp.Percentage ELSE 1.0 END) ))										
				[B2C ALLOWED]		
			,SUM(ISNULL(v4.booking_nAdults_show+v4.booking_nChild_show,ISNULL(v4.booking_nAdults+v4.booking_nChild,0)))					
				[B2C SOLD Tickets]
			
			-- DISTRIBUTOR						
					
			,CAST(CEILING(( CASE WHEN ISNULL(dtc.IgnorePercentageDays,99) = -1 THEN ISNULL(dtc.TicketsPercentage,1.0) ELSE 1.0 END * 
				ISNULL(@MAXTICKETS,FLOOR(ISNULL(m.maxticket_number,ISNULL(d.voucher_paxAllocabili,d.voucher_pax)) * CASE WHEN DATEDIFF(DAY,GETDATE(), DATEADD(DAY,ISNULL(-@CHANNELDAYS,-dgp.IgnorePercentageDays),@FROMDATE)) > 0 THEN dgp.Percentage ELSE 1.0 END)))) AS INT) - SUM(ISNULL(v1.booking_nAdults_show+v1.booking_nChild_show,ISNULL(v1.booking_nAdults+v1.booking_nChild,0)))		
				[D Tickets LEFT]		
			,SUM(ISNULL(v1.booking_nAdults_show+v1.booking_nChild_show,ISNULL(v1.booking_nAdults+v1.booking_nChild,0)))					
				[D Tickets SOLD]		
			,CAST(CEILING(( CASE WHEN ISNULL(dtc.IgnorePercentageDays,99) = -1 THEN ISNULL(dtc.TicketsPercentage,1.0) ELSE 1.0 END * 
					ISNULL(@MAXTICKETS,FLOOR(ISNULL(m.maxticket_number,ISNULL(d.voucher_paxAllocabili,d.voucher_pax)) * 
					CASE WHEN DATEDIFF(DAY,GETDATE(), DATEADD(DAY,ISNULL(-@CHANNELDAYS,-dgp.IgnorePercentageDays),@FROMDATE)) > 0 THEN dgp.Percentage ELSE 1.0 END)))) AS INT)		
				[D Tickets ALLOWED]		
			
			--CAPACITY RULES
			,CASE WHEN DATEDIFF(DAY,GETDATE(), DATEADD(DAY,ISNULL(-@CHANNELDAYS,-dgp.IgnorePercentageDays),@FROMDATE)) > 0 THEN dgp.Percentage ELSE 1.0 END				[B2B Percentage]		
			,ISNULL(@CHANNELDAYS,dgp.IgnorePercentageDays)							[Channel Days]		
			,CASE WHEN ISNULL(dtc.IgnorePercentageDays,99) = -1 THEN ISNULL(dtc.TicketsPercentage,1.0) ELSE 1.0 END [Capacity Rule] 
			,ISNULL(dtc.IgnorePercentageDays,99)									[Dist-Days]				
	FROM	v_booking v			
				FULL	JOIN	d_product_price_maxticket m			ON (m.price_id = v.price_id AND m.maxticket_giorno = v.booking_dateTour)
				LEFT	JOIN	d_product_price pp					ON pp.price_id = v.price_id
				INNER	JOIN	d_product p							ON p.product_id = pp.product_id AND (product_flagVatican = @VATICAN OR @VATICAN IS NULL) AND (p.city_id = @CITYID OR @CITYID IS NULL)
				LEFT	JOIN	d_booking v1						ON v1.booking_id = v.booking_id AND (v1.provenienza_id IN ((SELECT items FROM dbo.fn_Split(@DISTRIBUTOR,','))))				
				LEFT 	JOIN	d_booking v3						ON v.booking_id = v3.booking_id AND (v.provenienza_id IN (SELECT provenienza_id FROM d_provenienza WHERE provenienza_gruppo_id = 2))
				LEFT 	JOIN	d_booking v4						ON v.booking_id = v4.booking_id AND (v.provenienza_id IN (SELECT provenienza_id FROM d_provenienza WHERE provenienza_gruppo_id <> 2))			
				LEFT 	JOIN	d_distributor_TourConfiguration dtc ON dtc.provenienza_id = @DISTRIBUTOR AND dtc.price_id = m.price_id
				LEFT 	JOIN	DistributorGroupPercentage dgp		ON dgp.provenienza_gruppo_id = (SELECT provenienza_gruppo_id FROM d_provenienza prov WHERE prov.provenienza_id = @DISTRIBUTOR)			
				LEFT 	JOIN	ProductTicketAvailabilityConfig ptav ON ptav.ProductId = p.product_id
				LEFT 	JOIN	(	SELECT   SUM(voucher_pax) voucher_pax
											,voucher_giorno
											,SUM(voucher_paxAllocabili) voucher_paxAllocabili
											,fascia_id
											,ISNULL(MAX(voucher_paxAllocabili),MAX(voucher_pax)) voucher_pax_max
									FROM	d_assignement_voucher 
									WHERE	voucher_status = 'confirmed'
									GROUP BY 
											voucher_giorno, fascia_id
								) d  ON d.voucher_giorno = v.booking_dateTour AND d.fascia_id IN (Select fascia_id FROM d_fascia WHERE group_id = v.group_id)
	
				
	
	WHERE	((v.booking_dateTour BETWEEN @FROMDATE AND @TODATE))
	AND	    (v.price_id IN (SELECT items FROM dbo.fn_Split(@PRICEID,',')) OR @PRICEID IS NULL)
	AND	    (pp.group_id IN (SELECT items FROM dbo.fn_Split(@GROUPID,',')) OR @GROUPID IS NULL)
	AND	    (p.city_id IN (SELECT items FROM dbo.fn_Split(@CITYID,',')) OR @CITYID IS NULL)
	AND	    (v.product_id IN (SELECT items FROM dbo.fn_Split(@PRODUCTID,',')) OR @PRODUCTID IS NULL)
	AND		(p.product_flagPacchetto = @PACKAGETOUR OR @PACKAGETOUR IS NULL) --AND pp.price_id IN (select price_id from d_product_package pack WHERE pack.price_idMainOption = @PRICEID OR @PRICEID IS NULL))
	GROUP BY 
			 p.product_id	
			,p.product_flagPacchetto
			,pp.group_id	
			,pp.price_id		
			--,sc.utcHoldExpiration
			,p.product_title
			,pp.price_desc
			,m.maxticket_giorno
			,v.booking_dateTour
			,m.maxticket_number
			,d.voucher_pax
			,d.voucher_paxAllocabili
			,dgp.Percentage
			,dtc.TicketsPercentage
			,dtc.IgnorePercentageDays
			,dgp.IgnorePercentageDays
			,ptav.PercentageValue
			,ptav.UnlimitedTickets
			,ptav.UsePercentage
			,ptav.UseVirtualVoucher
			,d.voucher_pax_max

			
			--,vr.viator_rule_code
			--,vr.viator_rule_grade		
	--order by 4,1
) d
	--	LEFT JOIN	d_booking_shoppingcart sc on (sc.price_id = d.price_id and sc.tourDate = d.[Tour date]) AND CONVERT(datetimeoffset, sc.utcHoldExpiration, 127) > SysUtcDateTime()


group by
			 product_title						
			,P									
			,price_desc							
			,[Tour date]							
			,[Days 2 Tour]						
			,d.product_id						
			,group_id							
			,d.price_id							
			,[MAX]											
			,[Voucher PAX]									
			,[Booked]								
			,Available	
			,[VVP]						
			,[D Tickets LEFT]

order by 4,1

/*
-- VIRTUAL VOUCHERS

SELECT [id]
      ,[ProductId]
	  ,p.product_title
	  ,pp.price_desc
	  ,pp.price_id
	  ,pp.group_id
      ,[UnlimitedTickets]
      ,[UseVirtualVoucher]
      ,[VirtualVoucherNumber]
      ,[UsePercentage]
      ,[PercentageValue]
  FROM [citywonders].[dbo].[ProductTicketAvailabilityConfig]
	INNER JOIN d_product_price pp on pp.product_id = ProductId
	INNER JOIN d_product p on p.product_id = pp.product_id
where	1=1
and		ProductId IN(1)
--and		group_id = 2
and		pp.price_validoA >= GETDATE()
order by p.product_id asc


UPDATE ProductTicketAvailabilityConfig SET VirtualVoucherNumber = 1 WHERE ProductId IN (77,78,80,81)
UPDATE ProductTicketAvailabilityConfig SET UsePercentage = 1 WHERE ProductId IN (209)

*/

/*

SET DATEFORMAT DMY;
DECLARE @GROUPID INT, @DATE DATETIME
SET @GROUPID = 337; SET @DATE = '08/04/2016'
SELECT   SUM(voucher_pax) voucher_pax
		,voucher_giorno
		,SUM(voucher_paxAllocabili) voucher_paxAllocabili
		,fascia_id 
		,voucher_status
FROM	d_assignement_voucher av
WHERE	av. fascia_id IN (Select fascia_id FROM d_fascia WHERE group_id = @GROUPID)
AND		av.voucher_giorno = @DATE
GROUP BY voucher_giorno, fascia_id,voucher_status

SELECT   voucher_pax
		,voucher_giorno
		,voucher_paxAllocabili
		,fascia_id 
		,voucher_status
FROM	d_assignement_voucher av
WHERE	av. fascia_id IN (Select fascia_id FROM d_fascia WHERE group_id = @GROUPID)
AND		av.voucher_giorno = @DATE


*/

/*

	select * from d_product_package where price_idMainOption = 976

*/

/*
	SELECT * FROM DistributorGroupPercentage

	ALTER TABLE DistributorGroupPercentage
		ADD [IgnorePercentageDays] INT NOT NULL DEFAULT 30
		
	UPDATE DistributorGroupPercentage SET IgnorePercentageDays = 30
	
	UPDATE DistributorGroupPercentage SET Percentage = 0.30 WHERE DistributorGroupPercentageId = 1	
	UPDATE DistributorGroupPercentage SET Percentage = 0.70 WHERE DistributorGroupPercentageId = 2
	
	UPDATE DistributorGroupPercentage SET Percentage = 1.00 
	
*/