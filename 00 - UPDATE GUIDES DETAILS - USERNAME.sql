
SELECT * FROM d_user WHERE user_firstName like '%francesca%'

DECLARE @USERNAME VARCHAR(255), 
		@USERID INT,
		@USERPASSWORD VARCHAR(100),
		@USERFIRSTNAME VARCHAR(100)

SELECT	@USERNAME		= 'francesca.scolozzi', 
		@USERPASSWORD	= 'francesca123',
		@USERFIRSTNAME	= 'Francesca',
		@USERID			= 1315

BEGIN TRAN
	UPDATE	d_user
	SET		user_name		= @USERNAME,
			user_pwd		= @USERPASSWORD,
			user_firstName	= @USERFIRSTNAME
	WHERE	user_id			= @USERID
	
	SELECT * FROM d_user WHERE USER_ID = @USERID
	-- commit tran
	-- rollback tran