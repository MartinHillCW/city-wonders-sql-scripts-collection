
DECLARE @Billing_ID INT, @Amount DECIMAL(10,2), @Booking_id INT, @Trans_id INT, @BillingIdMaster INT
------------------------------------------------------------------------------
SELECT	@Billing_ID =  	 		852007  	 , 		
		@BillingIdMaster = @Billing_ID 
------------------------------------------------------------------------------

SELECT * FROM d_transactions WHERE billing_id_master	= @BillingIdMaster					--AND trans_amount = @Amount
SELECT * FROM d_transactions WHERE billing_id			= @Billing_ID						--AND trans_amount = @Amount

SELECT @Trans_id = Trans_id FROM d_transactions			WHERE billing_id = @Billing_ID		--AND trans_amount = @Amount
SELECT @Booking_id = booking_id FROM d_transactions		WHERE billing_id = @Billing_ID		--AND trans_amount = @Amount

-- BOOKING
SELECT booking_finalPrice,* FROM d_booking WHERE booking_id = @Booking_id

-- SHOW DATA
SELECT @Trans_id TransId, @Booking_id BookingId


/*
	EXEC sp_FIN_GetTransactionList @BILLINGIDMASTER = 788258
	EXEC sp_FIN_GetTransactionList @BILLINGIDMASTER = 812965
	select * from d_Currency
	select * from d_CurrencyConversionRates

*/

------------------------------------------------------------------------------------


BEGIN TRAN	

	DELETE FROM d_Trans_RefundReasons where trans_id	IN (710611)
	DELETE FROM d_transactions where trans_id			IN (710611)
	UPDATE d_booking SET booking_finalprice = 0 WHERE booking_id = 1022086	
	--	COMMIT TRAN
	
	--UPDATE d_transactions SET 
	--							billing_id = 864942,
	--							billing_id_master = 864942,
	--							trans_amount = 134.00
	--WHERE	trans_id = 672478	
	
	--UPDATE d_transactions SET trans_amount = -43	WHERE trans_id = 673538
	--UPDATE d_transactions SET trans_currency_rate = 1	WHERE trans_id = 636731	
	
	--UPDATE d_booking SET booking_finalprice = 0	WHERE booking_id = 1046759	
	--UPDATE d_booking SET billing_id = 864942	WHERE booking_id = 1042373	
	
	--UPDATE d_transactions SET salescurrency_id = 3		WHERE trans_id = 620060	
	
	--select * from d_booking_note where booking_id			IN (794803)
	
	--DELETE FROM d_booking_note where booking_id			IN (1056667,1056668,1056669)	
	--DELETE FROM d_booking_template where booking_id		IN (1056667,1056668,1056669)		
	--DELETE FROM D_BOOKING_GUIDE where booking_id		IN (1056667,1056668,1056669)	
	--DELETE FROM d_booking where booking_id				IN (1056667,1056668,1056669)	
	
	UPDATE d_booking SET booking_finalprice = 91.50 WHERE booking_id = 1076094	
	--UPDATE d_booking SET billing_id = 801350 WHERE booking_id = 1013326
	--UPDATE d_booking SET booking_flagDeleted = 0 WHERE booking_id IN(988367)
	--UPDATE d_booking SET booking_flagPagato = 1 WHERE booking_id IN (1032036)
	--UPDATE d_booking SET user_id_segnalato = 324 WHERE booking_id IN (967381)
	--UPDATE d_booking SET booking_id_pacchetto = 1029551 WHERE booking_id IN (1045965)
	
	--	ROLLBACK TRAN
	--	COMMIT TRAN


