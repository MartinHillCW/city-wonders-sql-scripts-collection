/*
SELECT (SELECT provenienza_id FROM d_provenienza WHERE provenienza_desc LIKE '%Dry Run%') OriginIdToDuplicate,
				provenienza_id, provenienza_desc
			FROM d_provenienza
			WHERE provenienza_desc LIKE '%Guest%'
			   OR provenienza_desc LIKE '%Guide Training%'
			   OR provenienza_desc LIKE '%Staff Training%'
			   OR provenienza_desc LIKE '%Tour Evaluation%'
			   OR provenienza_desc LIKE '%VIP B2B%'
			   OR provenienza_desc LIKE '%VIP B2C%'
*/
BEGIN TRAN
	DECLARE @OriginIdToDuplicate INT
	DECLARE @ProvenienzaId INT
	DECLARE @ProvenienzaName NVARCHAR(MAX)
	
	-- cursor for Origins 
	DECLARE db_cursor CURSOR FOR
		SELECT (SELECT provenienza_id FROM d_provenienza WHERE provenienza_desc LIKE '%_Direct Client B2B Email%') OriginIdToDuplicate,
				provenienza_id, provenienza_desc
			FROM d_provenienza
			WHERE provenienza_desc LIKE '%_Direct Client B2B Phone%'
			   OR provenienza_desc LIKE '%_Direct Client B2C Email%'
			   OR provenienza_desc LIKE '%_Direct Client B2C Phone%'
			   OR provenienza_desc LIKE '%_Direct Client Live Chat%'
			   OR provenienza_desc LIKE '%Guides Referral%'

	OPEN db_cursor  
	FETCH NEXT FROM db_cursor INTO @OriginIdToDuplicate, @ProvenienzaId, @ProvenienzaName

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SELECT @OriginIdToDuplicate OriginIdToDuplicate, @ProvenienzaId ProvenienzaId, @ProvenienzaName ProvenienzaName
		
		-- count current prices
		SELECT *
			FROM d_provenienza_price
			WHERE provenienza_id = @ProvenienzaId

		-- delete current prices
		DELETE FROM d_provenienza_price WHERE provenienza_id = @ProvenienzaId
		
		-- copy new prices
		INSERT INTO d_provenienza_price(provenienza_id, price_id, provenienza_price_validoDa, provenienza_price_validoA, provenienza_price_base,
										provenienza_price_numeroPersoneBase, provenienza_price_adult, provenienza_price_adultEtaDa, provenienza_price_adultEtaA,
										provenienza_price_child, provenienza_price_childEtaDa, provenienza_price_childEtaA, provenienza_price_senior,
										provenienza_price_seniorEtaDa, provenienza_price_seniorEtaA, provenienza_price_infant, provenienza_price_infantEtaDa,
										provenienza_price_infantEtaA, provenienza_price_freeSaleDays, provenienza_price_onRequestHours,
										provenienza_price_flagPrivate, provenienza_price_note, AdultConversionRate, ChildConversionRate,
										DateCreated, CreatedBy, DateAltered, AlteredBy)
			SELECT @ProvenienzaId, price_id, provenienza_price_validoDa, provenienza_price_validoA, provenienza_price_base,
										provenienza_price_numeroPersoneBase, provenienza_price_adult, provenienza_price_adultEtaDa, provenienza_price_adultEtaA,
										provenienza_price_child, provenienza_price_childEtaDa, provenienza_price_childEtaA, provenienza_price_senior,
										provenienza_price_seniorEtaDa, provenienza_price_seniorEtaA, provenienza_price_infant, provenienza_price_infantEtaDa,
										provenienza_price_infantEtaA, provenienza_price_freeSaleDays, provenienza_price_onRequestHours,
										provenienza_price_flagPrivate, provenienza_price_note, AdultConversionRate, ChildConversionRate,
										DateCreated, CreatedBy, DateAltered, AlteredBy
				FROM d_provenienza_price
				WHERE provenienza_id = @OriginIdToDuplicate

		FETCH NEXT FROM db_cursor INTO @OriginIdToDuplicate, @ProvenienzaId, @ProvenienzaName  
	END  

	CLOSE db_cursor
	DEALLOCATE db_cursor 



-- ROLLBACK TRAN
-- COMMIT TRAN


SELECT *
	FROM d_provenienza_price
	WHERE provenienza_id = 88

SELECT *
	FROM d_provenienza_price
	WHERE provenienza_id = 29

--(438 row(s) affected)

--(419 row(s) affected)

