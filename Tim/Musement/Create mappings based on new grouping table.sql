
SELECT *
	FROM d_BookingApiMappingsGrouping


SELECT --p.product_title, 
		--pp.price_desc,
		dp.provenienza_id,
		'PR' + replicate('0', 4 - len(mg.MainProductId)) + CAST(mg.MainProductId AS NVARCHAR(MAX)) AS ProductKey,
		UPPER(LEFT(l.ISO_639_1,2)) + REPLACE(CONVERT(VARCHAR(5),CONVERT(DATETIME,pp.price_startTime),108) ,':','') OptionKey,
		UPPER(l.ISO_639_1),
		p.city_id,
		pp.price_validoDa, pp.price_validoA, pp.price_validoDa,	pp.price_validoA,
		pp.price_id, GETDATE(),	436,
		mg.*
	FROM d_provenienza_price dp
	JOIN d_product_price pp ON pp.price_id = dp.price_id
	JOIN d_product p ON p.product_Id = pp.product_id
	JOIN d_language l ON l.lan_id = p.LAN_id
	JOIN d_BookingApiMappingsGrouping mg ON mg.GroupedProductId = p.product_id
	WHERE dp.provenienza_id = 107
	  AND mg.MainProductId IN (319, 40, 107, 126, 28)
	  --AND mg.GroupedProductId <> mg.MainProductId
	  AND GETDATE() < provenienza_price_validoA
	ORDER BY ProductKey, OptionKey, pp.price_validoDa

SELECT *
	FROM d_DistributorOptionIdentificationMapping
	WHERE DistributorId = 107
	  AND TourDateTo > GETDATE()
	  AND OptionID IN (SELECT price_id
						FROM d_product_price
						WHERE product_id IN (319, 40, 107, 126, 28, 379, 224, 74, 92, 82, 83, 353, 278, 273))
/*
DELETE
	FROM d_DistributorOptionIdentificationMapping
	WHERE DistributorId = 107
	  AND TourDateTo > GETDATE()
	  AND OptionID IN (SELECT price_id
						FROM d_product_price
						WHERE product_id IN (319, 40, 107, 126, 28, 379, 224, 74, 92, 82, 83, 353, 278, 273))


INSERT INTO d_DistributorOptionIdentificationMapping(DistributorId, ProductKey, OptionKey, LanguageCode, CityId, TourDateFrom, TourDateTo, BookingDateFrom, BookingDateTo, OptionId, DateCreated, CreatedBy)

	SELECT	dp.provenienza_id,
			'PR' + replicate('0', 4 - len(mg.MainProductId)) + CAST(mg.MainProductId AS NVARCHAR(MAX)) AS ProductKey,
			UPPER(LEFT(l.ISO_639_1,2)) + REPLACE(CONVERT(VARCHAR(5),CONVERT(DATETIME,pp.price_startTime),108) ,':','') OptionKey,
			UPPER(l.ISO_639_1),
			p.city_id,
			pp.price_validoDa,
			pp.price_validoA,
			pp.price_validoDa,
			pp.price_validoA,
			pp.price_id,
			GETDATE(),
			436
		FROM d_provenienza_price dp
		JOIN d_product_price pp ON pp.price_id = dp.price_id
		JOIN d_product p ON p.product_Id = pp.product_id
		JOIN d_language l ON l.lan_id = p.LAN_id
		JOIN d_BookingApiMappingsGrouping mg ON mg.GroupedProductId = p.product_id
		WHERE dp.provenienza_id = 107
		  AND mg.MainProductId IN (319, 40, 107, 126, 28)
		  AND GETDATE() < provenienza_price_validoA
		ORDER BY ProductKey, OptionKey, pp.price_validoDa
*/


SELECT *
	FROM d_DistributorOptionIdentificationMapping
	WHERE ProductKey = 'PR0119'