
SELECT	--sr.sru_id,
		p.product_title, 
		pp.price_desc,
		'PR' + replicate('0', 4 - len(p.product_id)) + CAST(p.product_id AS NVARCHAR(MAX)) AS ProductKey,
		UPPER(LEFT(l.ISO_639_1,2)) + REPLACE(CONVERT(VARCHAR(5),CONVERT(DATETIME,pp.price_startTime),108) ,':','') OptionKey,
		p.product_id,
		pp.price_id,
		pp.price_validoDa,
		pp.price_validoA

	FROM d_provenienza_price dp
	JOIN d_product_price pp ON pp.price_id = dp.price_id
	JOIN d_product p ON p.product_id = pp.product_id
	JOIN d_language l ON l.lan_id = p.LAN_id
	WHERE provenienza_id = 133
	  AND provenienza_price_validoA > GETDATE()
	  AND p.product_id IN (443, 415, 431, 84, 26, 63)
	ORDER BY p.product_id, pp.price_startTime, pp.price_validoDa

	

/*

-- Headout
INSERT INTO d_DistributorOptionIdentificationMapping(ProductKey, OptionKey, LanguageCode, CityId, OptionId, DistributorId, 
													TourDateFrom, TourDateTo, BookingDateFrom, BookingDateTo, DateCreated, CreatedBy)
SELECT	'PR' + replicate('0', 4 - len(p.product_id)) + CAST(p.product_id AS NVARCHAR(MAX)) AS ProductKey,
		UPPER(LEFT(l.ISO_639_1,2)) + REPLACE(CONVERT(VARCHAR(5),CONVERT(DATETIME,pp.price_startTime),108) ,':','') OptionKey,  
		l.ISO_639_1, p.city_id, pp.price_id, (SELECT provenienza_id FROM d_provenienza WHERE provenienza_desc = 'Headout'), 
		pp.price_validoDa, pp.price_validoA, pp.price_validoDa, pp.price_validoA, GETDATE(), 436
	FROM d_provenienza_price dp
	JOIN d_product_price pp ON pp.price_id = dp.price_id
	JOIN d_product p ON p.product_id = pp.product_id
	JOIN d_language l ON l.lan_id = p.LAN_id
	WHERE provenienza_id = 133
	  AND provenienza_price_validoA > GETDATE()
	  AND p.product_id IN (443, 415, 431, 84, 26, 63)
	ORDER BY p.product_id, pp.price_startTime, pp.price_validoDa

*/

SELECT *
	FROM d_DistributorOptionIdentificationMapping om
	JOIN d_product_price pp ON pp.price_id = om.OptionId
	WHERE DistributorId = 133
	  AND pp.product_id IN (443, 415, 431, 84, 26, 63)
	

