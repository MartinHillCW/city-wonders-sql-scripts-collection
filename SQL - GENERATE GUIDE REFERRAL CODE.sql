
DECLARE @Date DATETIME
SET @Date = GETDATE()
DECLARE @LastDateInMonth DATETIME
SET @LastDateInMonth = DATEADD(dd, 35, @Date)

DECLARE @DiscountPercentage INT
SET @DiscountPercentage = 5

-- populate cursor with concatenated user first last name and user id
DECLARE @Username VARCHAR(20)
DECLARE @UserId INT
DECLARE db_cursor CURSOR FOR
	SELECT	LOWER(SUBSTRING(user_firstName, 1, 3)) + LOWER(SUBSTRING(user_lastName, 1, 3)) name,
			USER_ID
		FROM d_user u
		WHERE u.user_flagGuide = 1 /* Guide */
		  AND u.user_flagInServizio = 1
		  AND u.user_id = 1518

OPEN db_cursor
FETCH NEXT FROM db_cursor INTO @Username, @UserId

WHILE @@FETCH_STATUS = 0  
BEGIN
	-- build promo code
	DECLARE @PromoCode NVARCHAR(20)
	SET @PromoCode = @Username + 
						CAST(MONTH(@Date) as NVARCHAR(2)) + 
						SUBSTRING(CAST(YEAR(@Date) as NVARCHAR(4)), 3, 2) + 
						CAST(@UserId as NVARCHAR(10))

	-- insert promo code
	INSERT INTO d_promo (promo_type_id, promo_desc, promo_percSconto,
							promo_dataAttivitaDa, promo_dataAttivitaA, promo_notes, UserId)
	VALUES (1, @PromoCode, @DiscountPercentage, @Date, @LastDateInMonth, 'guide referral code auto generated from sql server job', @UserId)
	
	-- fetch next record
	FETCH NEXT FROM db_cursor INTO @Username, @UserId
END

-- delete cursor
CLOSE db_cursor  
DEALLOCATE db_cursor 
