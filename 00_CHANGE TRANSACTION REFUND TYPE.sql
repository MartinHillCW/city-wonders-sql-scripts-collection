

BEGIN TRAN
	DECLARE @TRANSID INT, @REFUNDREASONSID INT
	
	SET @TRANSID = 1054146
	
	SET @REFUNDREASONSID = (SELECT RefundReasons_id FROM d_Trans_RefundReasons WHERE Trans_id = @TRANSID)
	
	SELECT * FROM d_TRANSACTIONS		WHERE trans_id = @TRANSID
	SELECT * FROM d_BibleData			WHERE transid = @TRANSID
	SELECT * FROM d_Trans_RefundReasons WHERE RefundReasons_id = @REFUNDREASONSID

	UPDATE d_TRANSACTIONS SET trans_RefundReason = 'Chargeback' WHERE trans_id = @TRANSID
	
	UPDATE d_BibleData 
	SET		 RefundReason_Description = 'Chargeback'
			,RefundReasonExtended_Description = NULL 
	WHERE	transid = @TRANSID
	
	UPDATE d_Trans_RefundReasons SET RefundReasons_RefundReason = 9, RefundReasons_RefundReasonExtended = '' WHERE RefundReasons_id = @REFUNDREASONSID

	SELECT * FROM d_TRANSACTIONS		WHERE trans_id = @TRANSID
	SELECT * FROM d_BibleData			WHERE transid = @TRANSID
	SELECT * FROM d_Trans_RefundReasons WHERE Trans_id = @TRANSID
	
	-- COMMIT TRAN
	-- ROLLBACK TRAN
--	3-9