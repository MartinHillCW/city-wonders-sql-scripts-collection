
SELECT *
	FROM (
			SELECT	--sr.sru_id,
					'PR' + replicate('0', 4 - len(p.product_id)) + CAST(p.product_id AS NVARCHAR(MAX)) AS ProductKey,
					UPPER(LEFT(l.ISO_639_1,2)) + REPLACE(CONVERT(VARCHAR(5),CONVERT(DATETIME,pp.price_startTime),108) ,':','') OptionKey,
					UPPER(LEFT(l.LAN_name,2)) + REPLACE(CONVERT(VARCHAR(5),CONVERT(DATETIME,pp.price_startTime),108) ,':','') OptionKey1,
					CASE WHEN pp.price_flagPrivate = 1 THEN 'Private' ELSE 'Standard' END ProductType,
					pp.price_flagPrivate,
					p.product_title, 
					pp.price_desc,
					sr.product_id,
					sr.price_id,
					sr.SRU_tourDateFrom,
					sr.SRU_tourDateTo
					--sr.DescriptiveKey1,
					--sr.DescriptiveKey2
					--sr.*
				FROM d_supplierrule sr
				JOIN d_product_price pp ON pp.price_id = sr.price_id
				JOIN d_product p ON p.product_id = sr.product_id
				JOIN d_language l ON l.lan_id = p.LAN_id
				WHERE sr.provenienza_id = 9
				  AND ISNUMERIC(SRU_key1) = 1
				  AND GETDATE() < SRU_tourDateTo
				GROUP BY p.product_title, pp.price_desc, p.product_id, l.LAN_name, pp.price_startTime,
					sr.product_id, sr.SRU_tourDateFrom, sr.SRU_tourDateTo, sr.price_id, pp.price_flagPrivate, l.ISO_639_1
		) d
		--WHERE d.ProductKey = 'PR0034'
		--  AND d.OptionKey = 'EN1000'
		WHERE product_id IN (26, 27, 155
		-- 93, 326,
		--421, /* tickets */
		--119
		)
		ORDER BY d.product_title, d.price_desc

/*
-- find duplicates
SELECT ProductKey, OptionKey, d.SRU_tourDateFrom, d.SRU_tourDateTo, COUNT(OptionKey)
	FROM (
			SELECT	--sr.sru_id,
					p.product_title, 
					pp.price_desc,
					'PR' + replicate('0', 4 - len(p.product_id)) + CAST(p.product_id AS NVARCHAR(MAX)) AS ProductKey,
					UPPER(LEFT(l.LAN_name,2)) + REPLACE(CONVERT(VARCHAR(5),CONVERT(DATETIME,pp.price_startTime),108) ,':','') OptionKey,
					l.lan_name,
					sr.product_id,
					sr.price_id,
					sr.SRU_tourDateFrom,
					sr.SRU_tourDateTo
				FROM d_supplierrule sr
				JOIN d_product_price pp ON pp.price_id = sr.price_id
				JOIN d_product p ON p.product_id = sr.product_id
				JOIN d_language l ON l.lan_id = p.LAN_id
				WHERE sr.provenienza_id = 9
				  AND ISNUMERIC(SRU_key1) = 1
				  AND GETDATE() < SRU_tourDateTo
				GROUP BY p.product_title, pp.price_desc,
					'PR' + replicate('0', 4 - len(p.product_id)) + CAST(p.product_id AS NVARCHAR(MAX)),
					UPPER(LEFT(l.LAN_name,2)) + REPLACE(CONVERT(VARCHAR(5),CONVERT(DATETIME,pp.price_startTime),108) ,':',''),
					l.lan_name, sr.product_id, sr.price_id, sr.SRU_tourDateFrom, sr.SRU_tourDateTo
				--ORDER BY p.product_title, pp.price_desc
	) d
	GROUP BY ProductKey, OptionKey, d.SRU_tourDateFrom, d.SRU_tourDateTo
	ORDER BY COUNT(OptionKey) DESC
*/

