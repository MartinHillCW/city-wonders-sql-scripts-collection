/*

Date		Autor			Change
2019-04-25	Martin Hill		Amended to have multiple To's


SELECT *
	FROM d_provenienza
	WHERE provenienza_desc LIKE '%rail%'

SELECT *
	FROM d_provenienza_price
	WHERE provenienza_id = 88

--	select * FROM d_provenienza_price WHERE provenienza_id IN (97)

From 88	_Direct Client B2B Email
89	_Direct Client B2B Phone
90	_Direct Client B2C Email
91	_Direct Client B2C Phone
128	_Direct Client Live Chat
29	Guides Referral


From 93	Dry Run
54	Guest
81	VIP B2B
94	Guide Training
95	VIP B2C
97	Tour Evaluation
99	Staff Training

*/

BEGIN TRAN
	
	DECLARE @ProvenienzaId_COPYFROM INT 
	
	SET @ProvenienzaId_COPYFROM = 88
--	SET @ProvenienzaId_COPYFROM = 93

	Create Table #To ( provenienza_id int )
	Insert #To Values 
 	(89), (90), (91), (128), (29)			-- for 88
--	(54), (81), (94), (95), (97), (99)		-- for 93
		
	-- DELETE EXISTING ORIGIN PRICES
	DELETE P
	FROM d_provenienza_price P 
				Inner Join #To On P.provenienza_id = #To.provenienza_id

	-- insert dry run
	INSERT INTO d_provenienza_price (provenienza_id,price_id,provenienza_price_validoDa,provenienza_price_validoA,provenienza_price_base,provenienza_price_numeroPersoneBase
									,provenienza_price_adult,provenienza_price_adultEtaDa,provenienza_price_adultEtaA,provenienza_price_child,provenienza_price_childEtaDa
									,provenienza_price_childEtaA,provenienza_price_senior,provenienza_price_seniorEtaDa,provenienza_price_seniorEtaA
									,provenienza_price_infant,provenienza_price_infantEtaDa,provenienza_price_infantEtaA,provenienza_price_freeSaleDays
									,provenienza_price_onRequestHours,provenienza_price_flagPrivate,provenienza_price_note)
	SELECT		#To.provenienza_id
				,price_id,provenienza_price_validoDa,provenienza_price_validoA,provenienza_price_base,provenienza_price_numeroPersoneBase
				,provenienza_price_adult,provenienza_price_adultEtaDa,provenienza_price_adultEtaA,provenienza_price_child,provenienza_price_childEtaDa
				,provenienza_price_childEtaA,provenienza_price_senior,provenienza_price_seniorEtaDa,provenienza_price_seniorEtaA,provenienza_price_infant
				,provenienza_price_infantEtaDa,provenienza_price_infantEtaA,provenienza_price_freeSaleDays,provenienza_price_onRequestHours
				,provenienza_price_flagPrivate,provenienza_price_note
		FROM d_provenienza_price P
				, #To 
		Where P.provenienza_id = @ProvenienzaId_COPYFROM
	
	SELECT P.* 
	FROM d_provenienza_price P
			Inner Join #To On P.provenienza_id = #To.provenienza_id

Drop Table #To


COMMIT TRAN
--	ROLLBACK TRAN
