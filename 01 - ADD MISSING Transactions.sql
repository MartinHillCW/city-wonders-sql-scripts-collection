USE DARKROME;	

DECLARE @BookingID INT

SELECT @BookingID = 711602
SELECT * FROM d_transactions WHERE booking_id IN (@BookingID)
SELECT * FROM d_booking WHERE booking_id = @BookingID


BEGIN TRAN		
		INSERT INTO d_transactions (booking_id, billing_id, trans_amount, trans_transcode_id, trans_transdate, trans_comment, 
			trans_user_id,trans_currency_rate, billing_id_master, trans_date_last_change, fattura_id, trans_status,booking_id_master
		)						
		SELECT 
			booking_id,							--booking_id, 
			billing_id,							--billing_id, 
			booking_finalprice ,				--trans_amount, 
			1,									--trans_transcode_id, 
			booking_date,						--trans_transdate, 
			'NEW',								--trans_comment, 
			user_id_ins,						--trans_user_id, 
			ISNULL(booking_cambio, 1.0),		--trans_currency_rate, 
			billing_id,							--billing_id_master, 
			getdate(),							--trans_date_last_change,
			NULL,								--fattura_id, 
			1,									--trans_status,
			booking_id							--booking_id_master
		FROM d_booking 
		WHERE booking_id = @BookingId
		
		SELECT	*
		FROM	d_transactions 
		WHERE	booking_id = @BookingID
----	--	ROLLBACK TRAN
--COMMIT TRAN